
/////////////////////////////////////////////////
//
// Multiplexer Program for ID21 SXM Branch
//
/////////////////////////////////////////////////

// INPUT

wire musst_outa;
wire musst_dcm_outa;
wire p201_gate;
wire vscanner_1;
wire vscanner_2;

// OUTPUT

reg musst_trig;
reg p201_trig;
reg p201_dcm_trig;
reg xia_trig;
reg ccd_trig;
reg mca_trig;
reg shutter_out;

// SELECTOR

wire [1:0]sel_trig_out;
wire [1:0]sel_trig_in;
wire [1:0]sel_shutter;

// Assignment

assign musst_outa  = I1;  // Musst OUTA [ZAP Point Pulse]
assign p201_gate   = I2;  // P201 GATEOUT [SPEC count]
assign vscanner_1  = I3;
assign vscanner_2  = I4;
assign musst_dcm_outa  = I5;  // Musst DCM OUTA [ZAP Point Pulse]


assign O1 = musst_trig;
assign O2 = p201_trig;    // P201 EXTTRIG [ZAP Point Pulse or 0]
assign O3 = xia_trig;     // XIA trigger  [ZAP Point Pulse or P201 GATEOUT]
assign O4 = ccd_trig;     // CCD trigger  [ZAP Point Pulse or P201 GATEOUT]
assign O5 = mca_trig;     // MCA trigger
assign O6 = shutter_out;  // shutter command (no more inverted)
assign O7 = p201_dcm_trig;    // P201 DCM EXTTRIG [ZAP Point Pulse or 0]

// Register Assignement

assign sel_trig_out[1:0] = {IM2, IM1};
assign sel_trig_in[1:0]  = {IM4, IM3};
assign sel_shutter[1:0]  = {IM6, IM5};


// Trigger Out Mode
// 00 = SPEC Count Gate
// 01 = ZAP Point Pulse SXM
// 10 = ZAP DCM
// 11 = ?

always @(sel_trig_out or musst_outa or musst_dcm_outa or p201_gate)
begin
  case (sel_trig_out)
    2'b00 :
      begin
        p201_trig     = 0;
        p201_dcm_trig = 0;
        xia_trig      = p201_gate;
        ccd_trig      = p201_gate;
        mca_trig      = p201_gate;
      end
    2'b01 :
      begin
        p201_trig     = musst_outa;
        p201_dcm_trig = musst_outa;
        xia_trig      = musst_outa;
        ccd_trig      = musst_outa;
        mca_trig      = musst_outa;
      end
    2'b10 :
      begin
        p201_trig     = musst_dcm_outa;
        p201_dcm_trig = musst_dcm_outa;
        xia_trig      = musst_dcm_outa;
        ccd_trig      = musst_dcm_outa;
        mca_trig      = musst_dcm_outa;
      end
  endcase
end

// Trigger In Mode
// 0 = no trigger
// 1 = vscanner 1
// 2 = vscanner 2

always @(sel_trig_in or vscanner_1 or vscanner_2)
begin
  case (sel_trig_in)
    2'b00 : musst_trig = 0;
    2'b01 : musst_trig = vscanner_1;
    2'b10 : musst_trig = vscanner_2;
    2'b11 : musst_trig = 0;
  endcase
end

always @(sel_shutter or p201_gate)
begin
  case (sel_shutter)
    2'b00 : shutter_out = 0;
    2'b01 : shutter_out = 1;
    2'b10 : shutter_out = p201_gate;
    2'b00 : shutter_out = 0;
  endcase
end