wire inp_1;

reg [14:0] CNT_120;
reg inp_aux;
reg cnt_start;

assign O1 = CNT_120[12];
assign O2 = CNT_120[12];
assign O3 = CNT_120[12];
assign O4 = CNT_120[12];
assign O5 = CNT_120[12];
assign O6 = CNT_120[12];
assign O7 = CNT_120[12];
assign O8 = CNT_120[12];

assign inp_1 = I1;

always @(posedge CLK16)
begin
  inp_aux <= inp_1;
end

always @(posedge CLK16)
  begin
    if (inp_1 && ~inp_aux) 
       cnt_start <= 1;
    else
       cnt_start <= 0;
  end

always @(posedge CLK16)
  begin
    if (cnt_start)
      CNT_120 <= 0'h1000;
    else if (CNT_120[13])
      CNT_120 <= CNT_120;
    else
      CNT_120 <= CNT_120 + 1;
  end



