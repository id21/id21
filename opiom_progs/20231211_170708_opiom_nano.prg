/////////////////////////////////////////////////
//
// Multiplexer Program for ID21 Nano End-Station
// 2023-12
//
/////////////////////////////////////////////////

////// INPUTS

wire musst_btrig;
wire p201_gate;


////// OUTPUTS

reg gate_p201;
reg gate_fx1;
reg gate_fx2;
reg gate_ccd;


////// SELECTORS

wire sel_trig;

//// BIDOUILLE

reg opiom_trig;

assign O8 = opiom_trig;

////// ASSIGNMENTS

assign p201_gate   = I1;  // COUNT gate : p201-9
assign musst_btrig = I2;  // ZAP gate

assign O1 = gate_p201;  // p201-9
assign O2 = gate_fx1;   // 
assign O3 = gate_fx2;   // 
assign O4 = gate_ccd;   // 


////// REGISTER ASSIGNEMENT

assign sel_trig = IM1;

// Trigger Out Mode
// 00 = Count Gate (p201)
// 01 = ZAP Gate


always @(sel_trig or musst_btrig or p201_gate or opiom_trig)
begin
  case (sel_trig)
    1'b0 :
      begin
        gate_p201 = p201_gate;
        gate_fx1  = p201_gate;
        gate_fx2  = p201_gate;
        gate_ccd  = p201_gate;
      end
    1'b1 :
      begin
        gate_p201 = musst_btrig;
        gate_fx1  = musst_btrig;
        gate_fx2  = musst_btrig;
        gate_ccd  = musst_btrig;
      end
  endcase
end

always @(negedge CLK16)
  begin
    opiom_trig = IM8;
  end
