============
ID21 project
============

[![build status](https://gitlab.esrf.fr/ID21/id21/badges/master/build.svg)](http://ID21.gitlab-pages.esrf.fr/ID21)
[![coverage report](https://gitlab.esrf.fr/ID21/id21/badges/master/coverage.svg)](http://ID21.gitlab-pages.esrf.fr/id21/htmlcov)

ID21 software & configuration

Latest documentation from master can be found [here](http://ID21.gitlab-pages.esrf.fr/id21)
