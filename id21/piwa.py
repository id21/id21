"""
SESSION_SXM [10]: calc_kb.constants
        Out [10]: {'off_h': -0.656, 'off_v': -1.2677, 'dist_h': 98.21, 'dist_v': 99.62, 'simul_h1': 7.9745, 'simul_v1': 7.5326}


SESSION_SXM [17]: calc_kb._expressions
        Out [17]: {'kbhfe': '(0.1 * h1) + off_h', 'kbhfa': 'arctan(((0.1 * h1) + off_h ) / dist_h ) * 1000', 'kbvfe': '(0.1 * v1) + off_v', 'kbvfa': 'arctan(((0.1 * v1) + off_v ) / dist_v ) * 1000'}


SESSION_SXM [19]:
SESSION_SXM [19]: calc_kb.outputs
        Out [19]: Namespace containing:
                  .kbhfe
                  .kbhfa
                  .kbvfe
                  .kbvfa


SESSION_SXM [20]: calc_kb.inputs
        Out [20]: Namespace containing:
                  .kbhfw
                  .kbvfw



SESSION_SXM [6]: pprint.pprint(piwa.config)
{'class': 'Piwa',
 'horizontal': {'angle': 'kbhfa',
                'encoder': 'kbhfe',
                'motor': <bliss.common.axis.Axis object at 0x7f47a164e710>},
 'name': 'piwa',
 'plugin': 'bliss',
 'vertical': {'angle': 'kbvfa',
              'encoder': 'kbvfe',
              'vertical': <bliss.common.axis.Axis object at 0x7f479fdfb090>}}


"""

import numpy


from bliss import global_map
from bliss import setup_globals

from bliss.config.beacon_object import BeaconObject

# better use a piwa (beacon) object ?

import pprint


def yesno(msg):
    _message = msg + "(y/n) "
    _ans = input(_message)
    try:
        return util.strtobool(_ans)
    except:
        return False


class Piwa:
    """
    Pico Wago kb bender adjustment.
    """

    def __init__(self, name, config):
        self.beacon_obj = BeaconObject(config, share_hardware=False)
        global_map.register(self, tag=name)

        self.calc_kb = self.beacon_obj.config.get("calc_counter")
        self.kbhfw = self.beacon_obj.config.get("horizontal").get("wago")
        self.kbhf = self.beacon_obj.config.get("horizontal").get("motor")

        self.kbvfw = self.beacon_obj.config.get("vertical").get("wago")
        self.kbvf = self.beacon_obj.config.get("vertical").get("motor")

    def __info__(self):
        """info string"""
        str_info = "Pico Wago KB adjustment\n"
        # print("----------------------------------------------------------")
        # pprint.pprint(self.config)
        # print("----------------------------------------------------------")

        res = self.calculate()

        kbhfe_val = res["kbhfe_val"]
        kbhfa_val = res["kbhfa_val"]
        kbvfe_val = res["kbvfe_val"]
        kbvfa_val = res["kbvfa_val"]

        str_info += f" HF:\n"
        str_info += f"    WAGO(V)   kbhfw = {self.kbhfw.value:2.5f} \n"
        str_info += f"    ANG(mrad) kbhfa = {kbhfa_val:1.5f}\n"
        str_info += f"    ENC(mm)   kbhfe = {kbhfe_val:1.5f}\n"
        str_info += f"    MOT       kbhf  = {self.kbhf.position:1.5f}\n"
        str_info += f" VF:\n"
        str_info += f"    WAGO(V)   kbvfw = {self.kbvfw.value:2.5f} \n"
        str_info += f"    ANG(mrad) kbvfa = {kbvfa_val:1.5f}\n"
        str_info += f"    ENC(mm)   kbvfe = {kbvfe_val:1.5f}\n"
        str_info += f"    MOT       kbvf  = {self.kbvf.position:1.5f}\n"

        return str_info

    def calculate(self):
        """Compute """
        self.out_calc = self.calc_kb.calc_function(
            {"h1": self.kbhfw.value, "v1": self.kbvfw.value}
        )

        results = dict()

        results["kbhfe_val"] = self.out_calc["kbhfe"].flat[0]
        results["kbhfa_val"] = self.out_calc["kbhfa"].flat[0]
        results["kbvfe_val"] = self.out_calc["kbvfe"].flat[0]
        results["kbvfa_val"] = self.out_calc["kbvfa"].flat[0]

        return results

    def piwa_move(motor, target_pos):

        if motor == kbhf:
            mirror = "horizontal"
            distance = calc_kb.constants.dist_h
        elif motor == kbvf:
            mirror = "vertical"
            distance = calc_kb.constants.dist_v
        else:
            raise RuntimeError(f"PIWA error, wrong motor: {motor.name} ")

        print(f"PIWA: target={target}")
        print(f"PIWA: distance={distance}")
        print(f"PIWA: motor.position={motor.position}")
        print(f"PIWA: relative_move={relative_move}")

        relative_move = (numpy.tan(target) * distance) - motor.position
        print(f"PIWA: relative_move={relative_move}")

        if abs(relative_move) > 0.1:
            raise RuntimeError(f"hummm relative_move seems too big: {relative_move}")

        iter_count_max = 5
        iter_count = iter_count_max
        accurate = False

        while (iter_count > 0) or (not accurate):
            """ stop if too many iterations or accurate enough.
            """
            new_pos = motor.position + relative_move
            # print(f"PIWA: moving {motor.name} by {relative_move} (to {new_pos})")
            if yesno(
                f"do you want to move  {motor.name} by {relative_move}  (to {new_pos})"
            ):
                motor.move(relative_move)
            else:
                raise RuntimeError("stopping")

            # read new positions
            encpos = self.conf[motor]["encoder"].position
            realpos = self.conf[motor]["encoder"].position

            # new relative movement to do.
            relative_move = numpy.tan(target_pos - realpos) * distance

            print(
                f"PIWA: {motor.name} position={motor.position} encoder={encpos} correction={relative_move}"
            )

            iter_count -= 1

            if abs(relative_move) < piwa_conf["delta"]:
                accurate = True

        # Done: either accurate enough, or too much iterations.

        # Update motor position
        if yesno(f"do you want to set {motor.name} position to {encpos}"):
            motor.dial = encpos
            motor.position = encpos
        else:
            raise RuntimeError("stopping")

        if accurate:
            print(f"PIWA: OK finished in {iter_count_max - iter_count} steps.")
        else:
            print(f"PIWA: too many iteration :( ")

        print(f"PIWA: error is: ")
