
import bliss
import gevent
import tango
import time
from bliss.common.axis import AxisState

from bliss.config import static
cfg = static.get_config()


"""
  contscan.EnergyCont Energy 2.14 2.3413332000000002 940 0.1

     Energy      time      p201  musstdcm  calc_diodes
WARNING 2023-03-01 14:31:06,768 global.controllers.EnergyTrackerCalcMotor_674d6a95296cb45b316192f46ec73f6b.u42b: Controller velocity (0.008) is d
ifferent from set velocity (0.00923058980573706)
!!! === DevFailed: DevFailed[41      0941      0941
DevError[
    desc = It is currently not allowed to write attribute Acceleration. The device state is MOVING
  origin = Device_3Impl::write_attributes
  reason = API_AttrNotAllowed
severity = ERR]

DevError[
    desc = Failed to write_attribute on device id-carr/tl2/gap, attribute Acceleration
  origin = DeviceProxy::write_attribute()
  reason = API_AttributeFailed

"""


uuu  = cfg.get("u42b")

pos_ap = tango.AttributeProxy("//acs.esrf.fr:10000/id/master/id21/U42b_GAP_Position")
vel_ap = tango.AttributeProxy("//acs.esrf.fr:10000/id/master/id21/U42b_GAP_Velocity")
state_ap = tango.AttributeProxy("//acs.esrf.fr:10000/id/master/id21/state")

und_dp = tango.DeviceProxy("tango://acs:10000/id-und/ppu42/17")
carr_udp = tango.DeviceProxy("tango://acs:10000/id-carr/TL2/Carriage")


if float(pos_ap.read().value) < 18:
    uuu.move(19, wait=False)
else:
    uuu.move(17, wait=False)

_t0=time.time()

while time.time()-_t0 < 2:

    u42bRstate = uuu.state.READY
    u42bMstate = uuu.state.MOVING

    if u42bRstate and u42bMstate:
        print("AHHHHHH faut pas croiser les flux !!!!")


    # !!!!!  BAD BAD BAD
    # if AxisState.READY in uuu.state:   (always True : it tests the existence of the READY property)
    #     print("READY")


    tg_state = state_ap.read().value
    tg_id_state = und_dp.state()
    tg_carr_state = carr_udp.state()

    print(f"{uuu.name} pos={uuu.position:2.6f}(tg:{pos_ap.read().value:2.6f}) "
          f"speed={uuu.velocity}(tg:{vel_ap.read().value}) "
          f"R={u42bRstate} M={u42bMstate}  tg state={tg_state} tg_und state={tg_id_state}   carr state={tg_carr_state}" )


    # time.sleep(0.001)


# u42b pos=17.002125(tg:17.002125) speed=3.0(tg:3.0) u42bstate = MOVING (Axis is MOVING)  tg state=MOVING tg_und state=MOVING   carr state=MOVING
# u42b pos=17.002125(tg:17.002125) speed=3.0(tg:3.0) u42bstate = MOVING (Axis is MOVING)  tg state=MOVING tg_und state=MOVING   carr state=MOVING
# u42b pos=17.002125(tg:17.002125) speed=3.0(tg:3.0) u42bstate = MOVING (Axis is MOVING)  tg state=MOVING tg_und state=ON   carr state=ON
# u42b pos=17.002125(tg:17.002125) speed=3.0(tg:3.0) u42bstate = MOVING (Axis is MOVING)  tg state=ON tg_und state=ON   carr state=ON
# u42b pos=17.002125(tg:17.002125) speed=3.0(tg:3.0) u42bstate = READY (Axis is READY)  tg state=ON tg_und state=ON   carr state=ON
# u42b pos=17.002125(tg:17.002125) speed=3.0(tg:3.0) u42bstate = READY (Axis is READY)  tg state=ON tg_und state=ON   carr state=ON
