
"""
Tripod Frame security motion hook. (icepap airlock 1 2 3 )
"""

# NB: hook must be placed on legs. if placed on calc motor, it will
# not be called on leg move.

# Q: is it legit to make a hook on such a specific controller ?  Or is
#    it better to make specific tests inside the tripod controller ?
#
#    ->  hummm specific controller code does not "see" legs movements (not
#    triggered by tripod) ???


# TODO : __info__ accessible from frame tripod calc ?

import pprint

from bliss.common.logtools import log_error, log_debug
from bliss.common.hook import MotionHook

from bliss.config import static
cfg = static.get_config()

class TripodFrameHookLegs(MotionHook):

    """Tripod Frame Legs motion hook       FOR LEGS !!!!  -> in  dmc30019_tripod.yml """

    def __init__(self, name, config):
        self.config = config
        self.name = name
        self.tripod_axes = list()
        self._tripod_name = config.get("tripod", None)

        super().__init__()

    def init(self):
        """
        init() is called once at first use of the hook. (not true in case of stacked hook  ????)
        * check and store names of axes involved in motion hook (l1 l2 l3 for the tripod hook).
        """
        self.tripod_axes = []
        for axis in self.axes.values():
            self.tripod_axes.append(axis)

        _axes_str = " ".join([axis.name for axis in self.tripod_axes])
        print(f">>>>>>>>> Initialize TripodFrameHook frame LEGS ( axes: {_axes_str})")
        log_debug(self, f"Initialize TripodFrameHook LEGS ( axes: {_axes_str})")

        # Get tripod object only at init to avoid infinite recursion.
        self._tripod = cfg.get(self._tripod_name)
        print(f">>>>>>>>> Initialize TripodFrameHook LEGS ( tripod: {self._tripod.name})")


    def pre_move(self, motion_list):
        """
        <motion_list>: list of Motion objects (only moving axes)
        !!! motion.target_pos is in controller units !!!
        """

        # print("--------------------pre move  LEGS ------------")
        # print("motion_list=", motion_list)

        # Determine final positions for all motors:
        # - if motor in this motion, get its target position
        # - otherwise, get its current position


        # Q: how to test if alls motors are ON before to move ???


        # Read current positions for all Hooked axes.
        target_pos = dict([(axis.name, axis.position) for axis in self.tripod_axes])

        # print("---- current pos ---")
        # pprint.pprint(target_pos)

        # print("---- motion pos ---")
        # Update positions for axes in motion.
        for motion in motion_list:
            target_pos[motion.axis.name] = motion.axis.sign * motion.target_pos / motion.axis.steps_per_unit

            # print(f"CTRL  motion.target_pos for {motion.axis.name} = {motion.target_pos}")   # CTRL units (backlash included)
            # print(f"DIAL  target_pos[{motion.axis.name}] = motion.target_pos / motion.axis.steps_per_unit")  # DIAL (backlash included)
            # print(f"FINAL motion.user_target_pos for {motion.axis.name} = {motion.user_target_pos}") # FINAL user position (BL excluded)

        print("---- target user pos (including backlash) ---")
        _msg = "\n".join([f"{axis.name} --> {target_pos[axis.name]:>10.5f}" for axis in self.tripod_axes])
        print(_msg)


        ############  SAFETY CALCULATIONS  #####################
        # Ensure a difference in height lower than self.leg_diff_limit (typically 0.2 ?)
        ########################################################

        pos_leg1 = target_pos['nair1']   # hummmmmm hard-coded :(
        pos_leg2 = target_pos['nair2']
        pos_leg3 = target_pos['nair3']

        # Can use:
        # self._tripod.v2l_matrix
        # self._tripod.l2v_matrix

        self.diff_limit_12 = 0.5
        self.diff_limit_13 = 0.5
        self.diff_limit_23 = 0.3


        _diff = abs(pos_leg1 - pos_leg2)
        if _diff > self.diff_limit_12:
            _msg = f"humm length skew too high abs(leg1[{pos_leg1:>10.5f}]-leg2[{pos_leg2:>10.5f}]={_diff:>10.5f} > {self.diff_limit_12}"
            raise RuntimeError(_msg)

        _diff = abs(pos_leg1 - pos_leg3)
        if _diff > self.diff_limit_13:
            _msg = f"humm length skew too high abs(leg1[{pos_leg1:>10.5f}]/leg3[{pos_leg3:>10.5f}])={_diff:>10.5f} > {self.diff_limit_13}"
            raise RuntimeError(_msg)

        _diff = abs(pos_leg2 - pos_leg3)
        if _diff > self.diff_limit_23:
            _msg = f"humm length skew too high abs(leg2[{pos_leg2:>10.5f}]/leg3[{pos_leg3:>10.5f}])={_diff:>10.5f} > {self.diff_limit_23}"
            raise RuntimeError(_msg)


        ########################################################

    def post_move(self, motion_list):
        # print("--------------------post  move LEGS  ------------")
        pass


class TripodFrameHook(MotionHook):

    """Tripod Frame motion hook    FOR FRAME  TRIPOD  -> calc_frame_tripod.yml """

    def __init__(self, name, config):
        self.config = config
        self.name = name
        self.tripod_axes = list()
        self._tripod_name = config.get("tripod", None)
        super().__init__()

    def init(self):
        """
        init() is called once at first use of the hook.
        * check and store names of axes involved in motion hook (l1 l2 l3 for the tripod frame hook).
        """
        self.tripod_axes = []
        for axis in self.axes.values():
            self.tripod_axes.append(axis)
        _axes_str = " ".join([axis.name for axis in self.tripod_axes])
        print(f">>>>>>>>> Initialize TripodFrameHook ( axes: {_axes_str})")
        log_debug(self, f"Initialize TripodFrameHook ( axes: {_axes_str})")

        # Get tripod object only at init to avoid infinite recursion.
        self._tripod = cfg.get(self._tripod_name)
        print(f">>>>>>>>> Initialize TripodFrameHook ( tripod: {self._tripod.name})")


    def pre_move(self, motion_list):
        """
        <motion_list>: list of Motion objects (only moving axes)
        !!! motion.target_pos is in controller units !!!
        """
        print("--------------------pre move------------")
        # print("motion_list=", motion_list)

        # Determine final positions for all motors:
        # - if motor in this motion, get its target position
        # - otherwise, get its current position

        # Read current positions for all Hooked axes.
        target_pos = dict([(axis.name, axis.position) for axis in self.tripod_axes])

        # Update positions for axes in motion.
        for motion in motion_list:
            target_pos[motion.axis.name] = motion.target_pos / motion.axis.steps_per_unit

        _msg = "\n".join([f"{axis.name} --> {target_pos[axis.name]:>10.5f}" for axis in self.tripod_axes])
        print(_msg)
        print("----------------------------------------")


        ############  SAFETY CALCULATIONS  #####################

        pos_sz = target_pos['nframez']
        pos_srx = target_pos['nframerx']
        pos_sry = target_pos['nframery']

        # if problem:
        #     raise RuntimeError(msg)

        # Can use:
        # self._tripod.v2l_matrix
        # self._tripod.l2v_matrix

        ########################################################


    def post_move(self, motion_list):
        # print("--------------------post  move------------")
        pass
