# -*- coding: utf-8 -*-

"""
TPG gauge monitoring device server
Send a mail in case of alarm value.
"""

import signal
import time
import smtplib
from email.message import EmailMessage
import traceback

import numpy as np

from PyTango import DeviceProxy
from PyTango.server import server_run
from PyTango.server import Device, DeviceMeta
from PyTango.server import attribute, command, device_property, attribute, command, run

def timestamp():
    """
    Timestamp string with blank space (for CSV saving: excel safe string)
    """
    return time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())

def timestamp_file():
    """
    Timestamp string without blank space.
    """
    return time.strftime("%Y-%m-%d_%Hh%Mm%Ss", time.localtime())


class TPGmoni(Device):
    __metaclass__ = DeviceMeta

    tango_server = device_property(dtype=str, doc="Tango uri. ex:ID42/dcm-balzpir/01")
    threshold = device_property(dtype=float, doc="pressure threshold ex:9e-4")

    last_email_send_timestamp = 0

    # delay between 2 mails in seconds
    delay_to_resend = 60 * 60

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.tgp = DeviceProxy(self.tango_server)

    def send_email(self):
        """
        Send an email giving the status (can be ALERT or OK depending on device status)
        """
        msg = EmailMessage()
        msg['Subject'] = f'DCM GAUGE MONITORING'
        msg['From'] = "cyril"

        msg['To'] = "cyril.guilloud@esrf.fr, murielle.salome@esrf.fr, marine.cotte@esrf.fr, gaetan.goulet@esrf.fr, hiram.castillo_michel@esrf.fr"
        # msg['To'] = "cyril.guilloud@esrf.fr"

        msg.set_content(self.get_status())
        s = smtplib.SMTP("smtp.esrf.fr")
        s.send_message(msg)
        print("email sent")
        self.last_email_send_timestamp = time.time()
        s.quit()

    def check_pressure(self):
        try:
            _pressure = self.tgp.pressure
        except Exception as err:
            _pressure = np.nan
            traceback.print_exc()

        # DEBUG
        # print(f"give press? (real is: {_pressure})")
        # _pressure = float(input())

        if _pressure > self.threshold or _pressure < 1e-6 or np.isnan(_pressure):
            _status = f"{timestamp()} ALERT TPG300 DCM presure={_pressure:2.2e} > {self.threshold:2.2e} (or < 1e-6)  ---   ({self.get_name()} TPGmoni.py _pressure={_pressure})"
            print(_status)
            self.set_status(_status)
            if ( time.time() - self.last_email_send_timestamp ) > self.delay_to_resend:
                self.send_email()
                # adaptative delay ? (not for now) self.delay_to_resend = self.delay_to_resend * 2
        else:
            _status = f"{timestamp()} OK TPG300 DCM : 1e-6 < presure={_pressure:2.2e} < {self.threshold:2.2e} ---   ({self.get_name()} _pressure={_pressure})"
            print(_status)
            self.set_status(_status)

            # if pressure is ok and an email has been sent recently (less than 10s), sent another one to say it's ok.
            if ( time.time() - self.last_email_send_timestamp ) < 30:
                print("pressure back to normal ")
                self.send_email()

                # Let's consider that previous mail was old.
                self.last_email_send_timestamp -= 50

    def dev_status(self):
        """
        Called to read status
        Polling set to 1000 ms make it periodically read.
        """
        self.check_pressure()

        return self.get_status()



    @command(dtype_in=str, dtype_out=str)
    def strftime(self, format):
        return time.strftime(format)


if __name__ == "__main__":
    server_run((TPGmoni,))
