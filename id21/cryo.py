
import numpy
import time

from bliss import global_map
from bliss import setup_globals

from bliss.config.beacon_object import BeaconObject

import pprint


"""
SESSION_CRYO [7]: wcid21d.modules_config.logical_keys
         Out [7]: {'ln2status': 0, 'led1': 1, 'led2': 2, 'ln2tc': 3, 'ln2refill': 4, 'ln2int': 5}

SESSION_CRYO [8]: wcid21d.modules_config.logical_keys.keys()
         Out [8]: dict_keys(['ln2status', 'led1', 'led2', 'ln2tc', 'ln2refill', 'ln2int'])


SESSION_CRYO [11]: wcid21d.modules_config.read_table['ln2status']
         Out [11]: {0: {'module_reference': '750-408', 'info': ModConf(digi_in=4, digi_out=0, ana_in=0, ana_out=0, n_channels=4, n_channels_ext=4, reading_type='digital', description='4 Channel Digital Input', reading_info={'reading_type': 'digital', 'bits': 1, 'type': 'DIGI_IN'}, writing_info={}), 'DIGI_IN': {'mem_position': [0]}}}

SESSION_CRYO [25]: wcid21d.get("ln2tc")
         Out [25]: 23.1

"""

def yesno(msg):
    _message = msg + "(y/n) "
    _ans = input(_message)
    try:
        return util.strtobool(_ans)
    except:
        return False

class Cryo:
    """
    Cryo cooler filling
    """

    def __init__(self, name, config):
        self.beacon_obj = BeaconObject(config, share_hardware=False)
        global_map.register(self, tag=name)

        self.name = name
        self.wago = config.get("wago")
        # self.cryo_IN_position = -44.5
        # self.cryo_OUT_position = 15

        print(f"     using wago: {self.wago.name} for {self.name}")

    def __info__(self):
        info_str = " CRYO COOLER REFILLING\n"
        info_str += f" {self.get_ln2status()}\n"
        info_str += f" wago: {self.wago.name}\n"
        info_str += f" temperature: {self.get_ln2tc()}\n"
        return info_str

    def state(self):
        print("----------------- CRYO --------------------")
        print(self.__info__())
        print(f"------------- WAGO INTERLOCK -------------")
        self.wago.interlock_show()
        print("------------------------------------------")

    def ln2_refill(self):
        print("refill")
        self.wago.set("ln2refill", 1)
        time.sleep(0.5)
        self.wago.set("ln2refill", 0)

    def get_ln2status(self):
        if self.wago.get("ln2status") == 1:
            return "LN2 valve open"
        else:
            return "LN2 valve closed"

    def get_ln2tc(self):
        return self.wago.get("ln2tc")


    def loop(self):
        for i in range(10):
            print("======== LN2 INJECTION =======")
            print(f"started {time.asctime()}")
            self.wago.interlock_show()
            self.wago.interlock_reset()
            print(self.get_ln2status())
            self.ln2_refill()
            print(self.get_ln2status())

            # timescan(count_time, *counter_args, npoints=0, save=True, save_images=None, sleep_time=None, run=True, scan_info=None)
            timescan(1, self.wago.counters.ln2tc, npoints=120, sleep_time=60)

            print(f"started {time.asctime()}")
            print(self.get_ln2status())

#    def cryo_in(self):
#        print ("cryo in")
#        if yesno("Is transfer system back in SAS"):
#            cryoz.move(self.cryo_IN_position)
#
#    def cryo_out(self):
#        print ("cryo out")
#        if yesno("Is transfer system back in SAS"):
#            if yesno("Is sample holder detached from cryo stage"):
#                cryoz.move(self.cryo_OUT_position)




