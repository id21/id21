import numpy

from bliss.controllers.counter import CalcCounterController

class LaserCalcCounter(CalcCounterController):
    """
    Calculation of LASER positions with 2 4Q dides and 4chan electrometers.
    """
    def __init__(self, name, config):
        
        super().__init__(name, config)
        
        self.l0 = 2300.0
        self.l1 = 100.0
        self.l2 = 1054.0
        self.l1pl2 = self.l1 + self.l2
        self.l1ml2 = self.l1 - self.l2
        self.l2ml1 = self.l2 - self.l1
        self.l0pl1 = self.l0 + self.l1
        self.l2ml1m2l0 = self.l2 - self.l1 - 2*self.l0
            
    def calc_function(self, input_dict):
                
        laser_values = {}
        
        laser_values["zd1"] = 0.3278 * input_dict['em1z']
        laser_values["yd1"] = 0.3155 * input_dict['em1y']
        laser_values["zd2"] = 0.2494 * input_dict['em2z']
        laser_values["yd2"] = 0.2988 * input_dict['em2y']
                     
        laser_values["tz"] = 0.5 * (1 - self.l2ml1m2l0 / self.l1pl2) * laser_values["zd1"] - 0.5 * (1 + self.l2ml1m2l0 / self.l1pl2) * laser_values["zd2"]
        laser_values["ty"] = (1 + self.l0pl1 / self.l2ml1) * laser_values["yd1"] - (self.l0pl1 / self.l2ml1) * laser_values["yd2"]
        laser_values["ry"] = numpy.arctan((laser_values["zd1"] + laser_values["zd2"]) / self.l1pl2) * 1e6
        laser_values["rx"] = numpy.arctan((laser_values["yd2"] - laser_values["yd1"]) / self.l2ml1) * 1e6

        return laser_values

