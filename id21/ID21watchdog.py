

from bliss.scanning.scan import ScanState, WatchdogCallback





class ID21ScanWatchdog(WatchdogCallback):
    """
    Monitoring class to detect and treat detectors errors during scans.
    """

    def __init__(self, points, motor, builder):

        super().__init__(watchdog_timeout=30.0)


    def on_scan_new(self, scan, scan_info):
        """
        Called at each new scan ?
        """
        print(f"WWWWWWWWWWWW on_scan_new(s={scan}, s_i={scan_info})")
        pass

    def on_state(self, state):
        """
        ???
        """
        print(f"WWWWWWWWWWWW on_state(s={state})")
        return True


    def on_scan_data(self, data_events, nodes, info):
        """
        Called each time data are received ?
        """
        print(f"WWWWWWWWWWWW on_state(de={data_events}, n={nodes}, i={info})")

        if len(data_events) < 1:
            print(" nothing received ? why osd called ???")
            return

        self._timeout_on = True

        for cont_name in self.controllers:
            self.controllers[cont_name]["updated"] = False

        for channel, events in data_events.items():

            data_node = nodes.get(channel)

            if not isinstance(data_node, DataNodeContainer):

                if data_node.name.lower() == self.musst_time_channel:
                    cont_name = ""
                else:
                    cont_name = data_node.name
                    if cont_name not in self.controllers:
                        cont_name = data_node.parent.name
                        if cont_name not in self.controllers:
                            cont_name = data_node.parent.parent.name

                if cont_name in self.controllers:
                    cont_obj = self.controllers[cont_name]
                    if not cont_obj["updated"]:
                        cont_obj["updated"] = True
                        cont_obj["points"] = len(data_node)
