import numpy as np
from bliss.common.counter import Counter, CalcCounter
from bliss.controllers.counter import CalcCounterController


class Fx2MoscaCorrCalcCounterController(CalcCounterController):
    
    def __init__(self, name, config):
        super().__init__(name, config)
            
    def build_counters(self, config):
        
        fx2mosca = config["device"]
        self._roi_name = config["roi_name"]
        
        roi2detnum = {}
        for rname, rvals in fx2mosca.rois.items():
            if rname.startswith(self._roi_name):
                if not isinstance(rvals[2],str):
                    roi2detnum[rname] = rvals[2]
                
        self._detnums = list(range(fx2mosca._number_channels))
        
        for cname in [self._roi_name, 'icr', 'ocr']:
            tag = f"{cname}"
            cnt = fx2mosca.counters[tag]
            if isinstance(cnt, Counter):
                self._tags[cnt.name] = tag
                self._input_counters.append(cnt)
            else:
                raise RuntimeError(
                    f"CalcCounterController inputs must be a counter but received: {cnt}"
                )

        for detnum in self._detnums:
            cnt = CalcCounter(f"{self._roi_name}_corr_{detnum:02d}", self, dim=0)
            self._tags[cnt.name] = cnt.name
            self._output_counters.append(cnt)

    def calc_function(self, input_dict):

        return { f"{self._roi_name}_corr_{detnum:02d}": input_dict[f"{self._roi_name}_{detnum:02d}"] * (input_dict[f"icr_{detnum:02d}"] / input_dict[f"ocr_{detnum:02d}"]) 
        for detnum in self._detnums }

        # roi/(1-deadtime/100)/iodet

class FluoCorrSumCalcCounterController(CalcCounterController):
    
    def __init__(self, name, config):
        super().__init__(name, config)

    def build_counters(self, config):
        
        target = config["target"]
        self._roi_name = target._roi_name
        self._detnums = target._detnums
        
        for detnum in self._detnums:
            tag = f"{self._roi_name}_corr_{detnum:02d}"
            cnt = target.outputs[tag]
            if isinstance(cnt, Counter):
                self._tags[cnt.name] = tag
                self._input_counters.append(cnt)
            else:
                raise RuntimeError(
                    f"CalcCounterController inputs must be a counter but received: {cnt}"
                )

        
        cnt = CalcCounter(f"{self._roi_name}_corr_sum", self, dim=0)
        self._tags[cnt.name] = cnt.name
        self._output_counters.append(cnt)
            
    def calc_function(self, input_dict):
        roi_corr_sum = 0
        for cnt in self.inputs:
            roi_corr_sum += input_dict[cnt.name]
        return {f"{self._roi_name}_corr_sum": roi_corr_sum}
