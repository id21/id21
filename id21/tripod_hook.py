
"""
Tripod Nano security motion hook.
"""

# NB: hook must be placed on legs. if placed on calc motor, it will
# not be called on leg move.

# Q: is it legit to make a hook on such a specific controller ?  Or is
#    it better to make specific tests inside the tripod controller ?
#    hummm specific controller code does not "see" legs movements (not
#    triggered by tripod) ???


import pprint

from bliss.common.logtools import log_error, log_debug
from bliss.common.hook import MotionHook

from bliss.config import static
cfg = static.get_config()

class TripodHookLegs(MotionHook):

    """Tripod Nano Legs motion hook       FOR LEGS !!!!  -> in dmc30019_tripod.yml """

    def __init__(self, name, config):
        self.config = config
        self.name = name
        self.tripod_axes = list()
        self._tripod_name = config.get("tripod", None)
        super().__init__()

    def init(self):
        """
        init() is called once at first use of the hook. (not true in case of stacked hook  ????)
        * check and store names of axes involved in motion hook (l1 l2 l3 for the tripod hook).
        """
        self.tripod_axes = []
        for axis in self.axes.values():
            self.tripod_axes.append(axis)

        _axes_str = " ".join([axis.name for axis in self.tripod_axes])
        print(f">>>>>>>>> Initialize TripodHook LEGS ( axes: {_axes_str})")
        log_debug(self, f"Initialize TripodHook LEGS ( axes: {_axes_str})")

        # Get tripod object only at init to avoid infinite recursion.
        self._tripod = cfg.get(self._tripod_name)
        print(f">>>>>>>>> Initialize TripodHook LEGS ( tripod: {self._tripod.name})")


    def pre_move(self, motion_list):
        """
        <motion_list>: list of Motion objects (only moving axes)
        !!! motion.target_pos is in controller units !!!
        """
        print("--------------------pre move  LEGS ------------")
        # print("motion_list=", motion_list)

        # Determine final positions for all motors:
        # - if motor in this motion, get its target position
        # - otherwise, get its current position

        # Read current positions for all Hooked axes.
        target_pos = dict([(axis.name, axis.position) for axis in self.tripod_axes])

        # Update positions for axes in motion.
        for motion in motion_list:
            target_pos[motion.axis.name] = motion.target_pos / motion.axis.steps_per_unit

        _msg = "\n".join([f"{axis.name} --> {target_pos[axis.name]}" for axis in self.tripod_axes])
        print(_msg)
        print("----------------- LEGS -----------------------")


        ############  SAFETY CALCULATIONS  #####################

        pos_leg1 = target_pos['nsz1']
        pos_leg2 = target_pos['nsz2']
        pos_leg3 = target_pos['nsz3']

        # Can use:
        # self._tripod.v2l_matrix
        # self._tripod.l2v_matrix

        _diff = abs(pos_leg1 - pos_leg2)
        if _diff > 0.2:
            _msg = f"humm length skew too high leg1/leg2 :{_diff}"
            raise RuntimeError(_msg)

        _diff = abs(pos_leg1 - pos_leg3)
        if _diff > 0.2:
            _msg = f"humm length skew too high leg1/leg3 :{_diff}"
            raise RuntimeError(_msg)

        _diff = abs(pos_leg2 - pos_leg3)
        if _diff > 0.2:
            _msg = f"humm length skew too high leg2/leg3 :{_diff}"
            raise RuntimeError(_msg)

        # if pos_l1 > 1.4:

        ########################################################

    def post_move(self, motion_list):
        print("--------------------post  move LEGS  ------------")



class TripodHook(MotionHook):

    """Tripod Nano motion hook    FOR TRIPOD  !!!!  -> calc_tripod.yml """

    def __init__(self, name, config):
        self.config = config
        self.name = name
        self.tripod_axes = list()
        self._tripod_name = config.get("tripod", None)
        super().__init__()

    def init(self):
        """
        init() is called once at first use of the hook.
        * check and store names of axes involved in motion hook (l1 l2 l3 for the tripod hook).
        """
        self.tripod_axes = []
        for axis in self.axes.values():
            self.tripod_axes.append(axis)
        _axes_str = " ".join([axis.name for axis in self.tripod_axes])
        print(f">>>>>>>>> Initialize TripodHook ( axes: {_axes_str})")
        log_debug(self, f"Initialize TripodHook ( axes: {_axes_str})")

        # Get tripod object only at init to avoid infinite recursion.
        self._tripod = cfg.get(self._tripod_name)
        print(f">>>>>>>>> Initialize TripodHook ( tripod: {self._tripod.name})")


    def pre_move(self, motion_list):
        """
        <motion_list>: list of Motion objects (only moving axes)
        !!! motion.target_pos is in controller units !!!
        """
        print("--------------------pre move------------")
        # print("motion_list=", motion_list)

        # Determine final positions for all motors:
        # - if motor in this motion, get its target position
        # - otherwise, get its current position

        # Read current positions for all Hooked axes.
        target_pos = dict([(axis.name, axis.position) for axis in self.tripod_axes])

        # Update positions for axes in motion.
        for motion in motion_list:
            target_pos[motion.axis.name] = motion.target_pos / motion.axis.steps_per_unit

        _msg = "\n".join([f"{axis.name} --> {target_pos[axis.name]}" for axis in self.tripod_axes])
        print(_msg)
        print("----------------------------------------")


        ############  SAFETY CALCULATIONS  #####################

        pos_sz = target_pos['nsz']
        pos_srx = target_pos['nsrx']
        pos_sry = target_pos['nsry']


        # Can use:
        # self._tripod.v2l_matrix
        # self._tripod.l2v_matrix

        ########################################################


    def post_move(self, motion_list):
        print("--------------------post  move------------")

        # Δsy = -103.5 * ΔRX
        # Δsx =  103.5 * ΔRY

        corrections = list()
        for motion in motion_list:
            print (f"{motion.axis.name}  Delta={ motion.delta}")
            if motion.axis.name == "nsrx":
                corrections.append(f"SRX:   mvr(sy, {(103.5 * motion.delta / 1000):3.5f})\n")
            if motion.axis.name == "nsry":
                corrections.append(f"SRY:   mvr(sx, {(-103.5 * motion.delta / 1000):3.5f})\n")


        print("Corrections to perform to compensate tripod movement: ")
        for corr in corrections:
            print(corr)

        print("----------------------------------------")


