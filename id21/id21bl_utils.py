# -*- coding: utf-8 -*-
#
# This file is part of the BLISS project
#
# This file is a part of ID21 specific BLISS code repository
#
# Copyright (c) 2018 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.


import tabulate
import inspect
import time

from bliss.common.utils import ColorTags, BOLD, GREEN, YELLOW, BLUE, RED, ORANGE
from bliss.config.static import get_config

from bliss.config.static import get_config
from bliss.shell.standard import umv, mv, wm
from bliss.controllers.motor import CalcController



def id21wm(*motors):
    print("")
    lines = []
    line1 = [BOLD("    Name")]
    line2 = [BOLD("    Pos.")]
    for mot in motors:
        if isinstance(mot.controller, CalcController):
            line1.append(f"{ORANGE(mot.name)}({mot.unit})")
        else:
            line1.append(f"{BLUE(mot.name)}({mot.unit})")
        line2.append(f"{mot.position:.3f}")
    lines.append(line1)
    lines.append(line2)
    mystr = tabulate.tabulate(lines, tablefmt="plain")
    mystr += "\n"

    print(mystr)

def wkohzu():
    id21wm(get_config().get("Ekohzuund"), get_config().get("u32a"), get_config().get("u42b"), get_config().get("u42c"))
    id21wm(get_config().get("Ekohzu"), get_config().get("mono"))


