
def _get_musst_template(master_motor, *other_chans):
    """ Return musst replacement template """
    (master_name, master_chan)= master_motor

    data_alias= []
    data_store= []
    for (name, chan) in other_chans:
        data_alias.append("ALIAS DATA%d = CH%d\n"%(chan, chan))
        data_store.append("DATA%d "%(chan))

    template_replacement= { \
        "$MOTOR_CHANNEL$": "CH%d"%master_chan,
        "$DATA_ALIAS$": "\n".join(data_alias),
        "$DATA_STORE$": " ".join(data_store),
    }
    return template_replacement
