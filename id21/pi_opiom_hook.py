from bliss.common.hook import MotionHook

class PiOpiomHook(MotionHook):
    """Motion hook for PI motors: generate a pulse on an OPIOM card to
    trig the fscan movement

    On ID21 nano, the pulse is generated on OPIOM O1 and used on Musst
    Itrig.
    """

    def __init__(self, name, config):
        self.config = config
        self.name = name
        super().__init__()

        self.opiom = config['opiom']
        self.pulse_ms = 1
        self.pulse_nb = 1

        #                          O1   2MHZ            tics up                  tics down           nb_pulses
        self.opiom.comm_ack(f"CNT   1   CLK2   PULSE   {2000*self.pulse_ms}     {2000*self.pulse_nb}        1")


    def pre_move(self, motion_list):
        self.opiom.comm_ack(f"START 1")
