#
#


"""
ID21 preset to close / open shutter during l2scans.
"""

from bliss.scanning.chain import ChainPreset
from bliss import setup_globals
from bliss.common.standard import user_print
import gevent



 NOT CONFIGURED !!!
 
 NOT TESTED !!!!


 

class FastShutterPreset(ChainPreset):
    sleep_time = 0

    def __init__(self):
        ChainPreset.__init__(self)
        self.fs = setup_globals.fshut

    def prepare(self, chain):
        pass
        # if not fsh_eps_check():
        #    raise RuntimeError("Fast shutter EPS check failed")

    def start(self, chain):
        self.fs.open()
        assert self.fs.is_open
        if FastShutterPreset.sleep_time:
            user_print(f"Fast shutter opened - now waiting for {FastShutterPreset.sleep_time} s.")
            gevent.sleep(FastShutterPreset.sleep_time)

    def stop(self, chain):
        self.fs.close()
        assert self.fs.is_closed
