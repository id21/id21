


from bliss import setup_globals
from bliss.config import settings
from bliss.scanning.group import Sequence
from bliss.scanning.chain import AcquisitionChannel, AcquisitionChain
from bliss.scanning.toolbox import ChainBuilder
from bliss.scanning.scan import Scan, ScanState
from bliss.controllers.lima.lima_base import Lima
from bliss.common import plot
from bliss.common.utils import ColorTags, BOLD, BLUE, GREEN, YELLOW, RED
from bliss.common.cleanup import cleanup
from bliss.shell.standard import umv, umvr
from bliss.scanning.scan_info import ScanInfo

from bliss.scanning.group import Sequence
from bliss import setup_globals
from bliss.shell.standard import umv, umvr

import numpy
import time
def scan_piezo(motor, start, stop):

    title = "scan piezo/musst"
    scan_info = ScanInfo()
    scan_info.update({
        "title": title,
        "type": "piezo_musst",
        "dim": 1,
    })
    scan_info.add_curve_plot()
    scan_info.set_channel_meta("scans", group="sequence")
    scan_info.set_channel_meta("scan_numbers", group="sequence")
    scan_info.add_1d_plot(name=title, x="time",y=["ch1", "ch2", "ch3"])

    seq = Sequence(scan_info=scan_info, title=title)

    nbp = 10000
    seq.add_custom_channel(AcquisitionChannel("time",numpy.float64,(nbp,)))
    seq.add_custom_channel(AcquisitionChannel("ch1",numpy.float64,(nbp,)))
    seq.add_custom_channel(AcquisitionChannel("ch2",numpy.float64,(nbp,)))
    seq.add_custom_channel(AcquisitionChannel("ch3",numpy.float64,(nbp,)))

    with seq.sequence_context() as scan_seq:

        # move to start position
        umv(motor, start)

        # configure musst
        setup_globals.musst_nano.upload_file("musst_daq.mprg")

        # start musst
        setup_globals.musst_nano.run()

        # move motor
        umv(motor, stop)
        time.sleep(10)
        # read musst
        d=setup_globals.musst_nano.get_data(4)

        # Create channels
        data = d.transpose()
        nbp = len(data[0])

        # Emit channels data
        seq.custom_channels["time"].emit(data[0])
        seq.custom_channels["ch1"].emit(data[1])
        seq.custom_channels["ch2"].emit(data[2])
        seq.custom_channels["ch3"].emit(data[3])
