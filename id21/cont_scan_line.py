import traceback, sys

"""

debugoff("bliss.scans")

debugon("bliss.scans")

debugon(p201); debugon(fx8); debugon(musst_sxm)

scan21line(sampy, 50, 51, 5, 0.1, fx8, p201, save=True)
scan21line(sampy, 50, 51, 5, 0.1, p201, save=False)


"""

import time
import numpy
from bliss.scanning.toolbox import ChainBuilder
from bliss.common.logtools import disable_user_output

# from bliss.controllers.lima.lima_base import Lima
# from bliss.controllers.lima.roi import Roi
# from bliss.scanning.scan import ScanDisplay

from bliss.scanning.acquisition.motor import MotorMaster, LinearStepTriggerMaster
from bliss.scanning.acquisition.musst import MusstAcquisitionMaster, MusstAcquisitionSlave
from bliss.scanning.acquisition.calc import CalcChannelAcquisitionSlave

from bliss.scanning.scan import Scan, StepScanDataWatch
from bliss.scanning.chain import AcquisitionChain
# from bliss.common.scans import DEFAULT_CHAIN
# from bliss.common.scans import ascan

from bliss.controllers.counter import CalcCounterController
from bliss.controllers.counter import SamplingCounterController

from bliss.controllers.ct2.device import AcqMode as CT2AcqMode
from bliss.controllers.ct2.client import CT2Controller

from bliss.controllers.mca.base import TriggerMode, BaseMCA
from bliss.controllers.emh import EMH

from bliss import setup_globals

from .CalcChannels import time_calculator, positions_calculator


def scan21line (fast_motor, start_x, stop_x, n_interval_x,
                count_time, *counters, save=True, save_images=False):
    """
    'ZAPLINE'-like scan
    """

    print(f"[SCAN21LINE] fast motor name:{fast_motor.name}")

    nbpix_x = n_interval_x               # Number of pixels in x direction = Number of x-intervals.

    total_time_x = count_time * nbpix_x  # A single line duration.
    total_time = total_time_x            # Does not take return time into account.

    pixel_count = nbpix_x                # Total number of pixels.

    # Trigs per line to be generated by the MUSST.
    # musst_trig_count = int(nbpix_x )      # mode 5
    musst_trig_count = int(nbpix_x ) + 1  # mode 7 "+1" to have a trig at begining of the scan.

    print (f"[SCAN21LINE] pixel_count={pixel_count}"
           f"fast mot name:{fast_motor.name}"
           f"npoints={nbpix_x}  ntrigs={musst_trig_count}\n"
    )

    # ------ Objects involved in the scan. -------------------------------------
    chain = AcquisitionChain()
    opiom_multiplexer = setup_globals.mpx

    # ------ Devices parametrization for continuous scan -------------------
    # If cont scan, P201 is triggered by the MUSST OUTA.
    opiom_multiplexer.switch('SEL_TRIG_OUT', 'ZAP')


    # ------ Devices parametrization depending on selected motors -------------------
    # Select the MUSST program.
    # Select OPIOM multiplexers positions.

    if fast_motor.name in ["samy", "samz"]:
        musst_program = "/musst_program/TRIG-TIME_START-POS.mprg"
        #musst_program = "/musst_program/test_prog.mprg"
        undershoot = None
        fast_motor_musst_name = f'{fast_motor.name}'

    elif fast_motor.name in ["sampy", "sampz"]:
        opiom_multiplexer.switch('MUSST_TRIG', 'VSCANNER1')
        musst_program = "/musst_program/TRIG-TIME_START-TRIG.mprg"
        #musst_program = "/musst_program/test_prog.mprg"
        undershoot = 0

        fast_motor_musst_name = "samy"  # hummmm hardcoded in MUSST prog :(

    else:
        raise RuntimeError(f"[SCAN21LINE] OUPS WRONG MOTOR:{fast_motor.name}")

    print(f"[SCAN21LINE] use MUSST prog: {musst_program}")
    # List of channels the musst device will produce.
    musst_store_list=['TIMER', 'samy', 'samz']


    # ------ MOTORS ----------------------------------------------

    
    # Fast motor:
    # * scanning-speed of the motor is calculated.
    # * return-speed of the master motor is the nominal speed.
    # * motors are not returning automatically at initial position after scan.
    fast_master = MotorMaster(
        fast_motor, start_x, stop_x, time=total_time_x, undershoot=undershoot
    )

    chain.add(fast_master)
    print(f"[SCAN21LINE] fast_master.channels={fast_master.channels}")


    # ----- MUSST Master --------------------------------------------
    musst_ctrl = setup_globals.musst_sxm

    print(f"[SCAN21LINE] STARTPOS  {start_x}, {int(start_x * fast_motor.steps_per_unit)}")
    print(f"[SCAN21LINE] TIME_STEP {count_time}, {int(count_time * musst_ctrl.get_timer_factor())}")

    _start_pos = int(start_x * fast_motor.steps_per_unit)
    _time_step = int(count_time * musst_ctrl.get_timer_factor())

    print (f"[SCAN21LINE] MUSST: _start_pos={_start_pos}"
           f"_time_step={_time_step} musst_trig_count={musst_trig_count}"
       )

    musst_acq_master = MusstAcquisitionMaster(musst_ctrl,
                                              musst_program,
                                              program_start_name="TEST",
                                              vars={"STARTPOS": _start_pos,
                                                    "TIME_STEP": _time_step,
                                                    "NPULSES":musst_trig_count,
                                                })
    chain.add(fast_master, musst_acq_master)

    # ------ MUSST counters ----------------------------------------------------
    musst_acq_device = MusstAcquisitionSlave(musst_ctrl, store_list=musst_store_list)

    # print ("musst acq slave chans:" )
    # print ([ch.short_name for ch in musst_acq_device.channels])

# deprecated: was used to fool flint. better use calc chan/ calc counters.
# fast_master.add_external_channel(musst_acq_device, "samy", "axis_samy")
# fast_master.add_external_channel(musst_acq_device, "samz", "axis_samz")

    chain.add(musst_acq_master, musst_acq_device)


    # ------ BUILDER for counters ----------------------------------------------
    builder = ChainBuilder(counters)


    # ------ HANDLE MCA CONTROLLERS ----------------------------
    mca_params = {}
    mca_params["npoints"] = nbpix_x -1
    mca_params["trigger_mode"] = TriggerMode.SYNC
    # mca_params["preset_time"] = 0.9               #  not used in SYNC trigger mode
    # mca_params["block_size"] = None
    # mca_params["polling_time"] = 0.1
    # mca_params["spectrum_size"] = None

    mca_params["prepare_once"] = False   # False to re-do 'prepare' at each line.
    mca_params["start_once"]   = False   # False to re-do 'start' at each line.

    for node in builder.get_nodes_by_controller_type(BaseMCA):
        node.set_parameters( acq_params=mca_params)
        chain.add(musst_acq_master, node)

    # ------ HANDLE EMH CONTROLLERS ------------------------
    emh_params = {}
    emh_params["trigger"] = "DIO_1"
    emh_params["int_time"] = count_time
    emh_params["npoints"] = nbpix_x
    emh_params["trigger_type"] = "HARDWARE"

    for node in builder.get_nodes_by_controller_type(EMH):
        node.set_parameters( acq_params=emh_params)
        chain.add(musst_acq_master, node)

    slow_motor_musst_name = "samz"  # hummmm hardcoded in MUSST prog :(

#    # ------ CALC CHANNELS ACQ SLAVE --------------------------------------
#    # TIMING
#    timing_calculator = CalcChannelAcquisitionSlave("timing_chans",
#                                [musst_acq_device],
#                                time_calculator(),
#                                ["x_index", "y_index", "pixel_index", "trig_index",
#                                 #  "epoch_time", "scan_time",  "index"
#                                ],
#    )
#    chain.add(musst_acq_master, timing_calculator)
#
#    # POSITION
#
#    axes_parameters = {"fast_axis_start": start_x, "fast_axis_stop": stop_x, "nb_intervals":n_interval_x}
#
#    pos_calculator = CalcChannelAcquisitionSlave("position_chans",
#                                [musst_acq_device],
#                                positions_calculator(fast_motor_musst_name, slow_motor_musst_name, axes_parameters),
#                                ["fast_axis_pos", "slow_axis_pos"]   # slow ???
#    )
#    chain.add(musst_acq_master, pos_calculator)

    # ---- OTHER DEVICES LIKE SAMPLING COUNTERS  (simulation diode) ------------
    for node in builder.get_nodes_by_controller_type(SamplingCounterController):
        print(f"[SCAN21] adding SamplingCounter ??? from controller {node.controller.name}\n")

        # set param for SamplingCounters
        node.set_parameters( acq_params={"npoints": nbpix_x, "count_time": count_time})

        #chain.add(musst_acq_master, node)
        chain.add(fast_master, node)


    # ----- P201  as MASTER
    p201_epsilon = 100  # shift trig time ( in micro seconds )

    p201_params = {}
    p201_params["npoints"] = nbpix_x #
    p201_params["acq_expo_time"] = count_time - p201_epsilon/1e6         #  used in mode 5 only
    p201_params["acq_point_period"] = None
    # p201_params["acq_mode"] = CT2AcqMode.ExtTrigReadout  # 7 count until next trig
    p201_params["acq_mode"] = CT2AcqMode.ExtTrigMulti   # 5 count for a given time
    p201_params["prepare_once"] = False
    p201_params["start_once"] = False

    for node in builder.get_nodes_by_controller_type(CT2Controller):
        node.set_parameters( acq_params=p201_params)

        # Add p201 counters under p201 master
        for child_node in node.children:
            child_node.set_parameters(acq_params={"count_time": count_time})
            # no chain_add : children are automaticaly added under the parent.

        # Add p201 master under musst.
        chain.add(musst_acq_master, node)



    # ---- CACLC COUNTERS  !!!! TO BE DONE AFTER  P201  !!!
    for node in builder.get_nodes_by_controller_type(CalcCounterController):
        print(f"[SCAN21] adding CalcCounter ??? from controller {node.controller.name}\n")
        
        node.set_parameters( acq_params={"npoints": nbpix_x, "count_time": count_time})

        #chain.add(musst_acq_master, node)
        chain.add(musst_acq_master, node)

        
    # ---- Check synchro MUSST/motor
    # to be parametrized :(
    fast_mot = setup_globals.samy
    _ = fast_mot.__info__()  # F. lazy init...
    musst_ch3 = musst_ctrl.get_channel(3).value
    mot_enc = fast_mot.controller.read_encoder(fast_mot.encoder)
    if ( abs(musst_ch3 - mot_enc) > 5):
        _msg = "AIIIIE AIIIE AIIIE : musst counter CH3 no synchronized with smay encoder\n"
        _msg += f"musst_ch3={musst_ch3}  mot_enc={mot_enc}"
        raise RuntimeError(_msg)
    else:
        print(f"[SCAN21] Ok fast motor enc{mot_enc} and musst{musst_ch3} are synchronized.\n")

    # ---- Check for not initilized node
    if builder.get_nodes_not_ready():
        builder.print_tree(not_ready_only=False)
        raise RuntimeError("There are not ready nodes -> cannot use these counters")

    # ------ build Scan object --------------------------------
    builder.print_tree(not_ready_only=False)
    print(chain._tree)

    scan_info = {
        "npoints": 1,   # total number of lines ??
        "type":"scan21line",
        "npoints1": nbpix_x,
        "count_time": count_time,
        "dim": 1,
        "data_dim": 1,
        "start": [start_x],
        "stop": [stop_x],
    }

    command_line = f"scan21line {fast_motor.name} {start_x} {stop_x} {n_interval_x} {count_time}"

    sc = Scan(
        chain,
        name=command_line,
        scan_info=scan_info,
        save=save,
        save_images=save_images,
        scan_saving=None,
        data_watch_callback=StepScanDataWatch(),
    )

    with disable_user_output():
        sc.run()
