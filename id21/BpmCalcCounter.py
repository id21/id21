
from bliss.controllers.counter import CalcCounterController

class BpmCalcCounter(CalcCounterController):
            
    def calc_function(self, input_dict):
        # print(f"CCC -- calc_function <<<< ({input_dict})")
        
        # BPM calculation.
        #    __________________________
        #   |_upper_left_|_upper_right_|   ↑
        #   |------------|-------------|
        #   | lower_left | lower_right |   y  x→
        #    ---------------------------
        
        up_left   = input_dict["up_left"]
        up_right  = input_dict["up_right"]
        low_right = input_dict["low_right"]
        low_left  = input_dict["low_left"]
        
        csum = up_left + up_right + low_right + low_left
        
        #for ind in range(len(csum)):
        #    if csum[ind] != 0:
        #        bpm_x[ind] = ((up_left[ind] + low_left[ind]) - (up_right[ind] + low_right[ind])) / csum[ind]
        #        bpm_y[ind] = ((up_left[ind] + up_right[ind]) - (low_right[ind] + low_left[ind])) / csum[ind]
        #    else:
        #        bpm_x[ind] = 0
        #        bpm_y[ind] = 0

        bpm_x = ((up_left + low_left) - (up_right + low_right)) / csum
        bpm_y = ((up_left + up_right) - (low_right + low_left)) / csum

        bpm_values = {}
        bpm_values["bpmi"] = csum
        bpm_values["bpmx"] = bpm_x
        bpm_values["bpmy"] = bpm_y
       
        # print(f"CCC -- calc_function >>>> {bpm_values} ")

        return bpm_values
        
