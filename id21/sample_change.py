
import gevent
import time

import bliss
from bliss.config import static
cfg = static.get_config()

from id21.absorbers_open_close import abs2_close, abs2_open, abs2_status

def sample_mounting():
    """
    Function to call prior to change the sample.
    """
    print(" Moving sample_stage_MP in loading position")
    sample_stage_MP = cfg.get("sample_stage_MP")
    light = cfg.get("light_vlm")
    zoom_MP = cfg.get("zoom_MP")

    light.intensity = 100
    sample_stage_MP.loading()

    try:
        print("Closing Absorber2")
        abs2_close()
    except RuntimeError as rt_err:
        print("Error closing Absorber2: ", rt_err)
    except BaseException as be_err:
        print("BaseException Error closing Absorber2: ", be_err)

    try:
        print("Closing RV10")
        rv10 = cfg.get("rv10")
        rv10.close()
    except RuntimeError as rt_err:
        print("Error closing RV10: ", rt_err)
    except BaseException as be_err:
        print("BaseException Error closing RV10: ", be_err)

    zoom_MP.x1()

    print("You can now mount your sample")

def sample_loaded():
    """
    Function to call after user has installed the sample.
    """
    print("Opening RV10")
    rv10 = cfg.get("rv10")
    rv10.open()

    print("Opening Absorber2")
    abs2_open()

    print("Ready")
