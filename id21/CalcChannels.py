
import time
import numpy

from bliss.scanning.acquisition.calc import CalcHook

class time_calculator(CalcHook):
    """
    Timing Calculation Channel: create timing channels:
#    * pixel_time: trig times (reset to zero at each new line)
#    * epoch_time: absolute "epoch time" in seconds
#    * scan_time: time from begining of the scan line in seconds
    * x_index: x indexes of the point on curretn line
    * y_index: y indexes of the point
    * trig_index
    * pixel_index
    """
    def __init__(self):
        self.trig_index = 0
        self.x_trig_index = 0
        self.y_trig_index = 0
        self.index = 0
        self.x_index = 0
        self.y_index = 0

        self.x_count = 0

    def compute(self, sender, in_data_dict, index):
        # print("in_data_dict=" , in_data_dict)

        # Treat only 1 input_data: the "TIMER" one.
        try:
            input_data = in_data_dict["TIMER"]
        except:
            return

        # Data are treated by blocks.
        # Size of current block is len(input_data)
        # I assume size to be always >= 1
        data_count = len(input_data)

        # print(f"input_data={input_data}, index={self.index}, x_index={self.x_index} data_count={data_count}")

        # each output_data item must be of the length of input_data.
        output_data = dict()

        # TRIG INDEX
        output_data["trig_index"] = numpy.linspace(self.trig_index, self.trig_index + data_count - 1, data_count)

        # PIXEL INDEX
        if self.x_trig_index == 0:
            # first block of the line.
            # first point is removed.
            output_data["x_index"] = numpy.linspace(self.x_index, self.x_index + data_count - 2, data_count - 1)
            output_data["y_index"] = numpy.ones(data_count - 1) * (self.y_index - 1)
            self.x_trig_index += data_count
            self.x_index += (data_count - 1)
            self.trig_index += data_count
            self.index += (data_count - 1)


        else:
            # next blocks of the line
            output_data["x_index"] = numpy.linspace(self.x_index, self.x_index + data_count - 1, data_count)
            output_data["y_index"] = numpy.ones(data_count) * (self.y_index - 1)

            # print("ODPI=", output_data["pixel_index"])
            self.x_trig_index += data_count
            self.x_index += data_count
            self.trig_index += data_count
            self.index += data_count

        output_data["pixel_index"] = output_data["x_index"] + self.x_count * (self.y_index - 1)
        
        # EPOCH
#        output_data["epoch_time"] = numpy.ones((data_count)) * time.time()
#        output_data["scan_time"] = numpy.ones((data_count)) * (time.time() - self.scan_start_time)

        # print(f"time output_data={output_data['pixel_index']}")
        return(output_data)

    def prepare(self):
        # at each start of line
        if self.x_count == 0:
            self.x_count = self.x_index
        self.x_trig_index = 0
        self.y_trig_index += 1
        self.x_index = 0
        self.y_index += 1

    def start(self):
        self.scan_start_time = time.time()

    def stop(self):
        pass


class positions_calculator(CalcHook):
    """
    1 to 1 calc
    Compute pixels positions:
    * fast_axis_pos: x positions are in the middle of each intervals.
    * slow_axis_pos:
    """
    def __init__(self, fast_axis_name, slow_axis_name=None, axes_param=None):
        # Store names of the INPUT channels.
        self.fast_axis_name = fast_axis_name
        self.slow_axis_name = slow_axis_name

        self.fast_axis_start = axes_param.get("fast_axis_start", 0)
        self.fast_axis_stop = axes_param.get("fast_axis_stop", 1)
        self.fast_axis_nb_intervals = axes_param.get("nb_intervals", 1)

        self.scan_step = (self.fast_axis_stop - self.fast_axis_start) / self.fast_axis_nb_intervals
#        print(f" INIT : start={self.fast_axis_start} stop={self.fast_axis_stop}"
#              f" interv={self.fast_axis_nb_intervals} step={self.scan_step:.5f}")

        # dict of INPUT/OUTPUT matching.
        self.chan_out_name = {
            "samy": "fast_axis_pos",
            "samz": "slow_axis_pos",
        }

        self.input_names_list = ["samy", "samz"]

    def compute(self, sender, in_data_dict, index):
        """
        Fill 'output_data' dictionnary with OUTPUT DATA.
        * <in_data_dict> contains INPUT DATA delivered block by block.
        * <in_data_dict> contains only 1 key (data of a channel) by call.
        """


        input_key = list(in_data_dict.keys())[0]
        if input_key in self.input_names_list:
            input_data = in_data_dict[input_key]
        else:
            # print(f"pass on IK {input_key}")
            return

        #print(f"inputkey={input_key} index={index} IN_DATA_DICT={in_data_dict}")

        output_data = dict()
        pos_list = list()

        # for all points of the current block:
        data_len = len(input_data)
        for ii in range(data_len):
            scan_point_index  = ii + index - data_len + 1
            # print(f"ii={ii} index={index} data_len={data_len} scan_point_index={scan_point_index}")
            if self.last_position[input_key] is None:
                # special case for 1st point
                # STEPPER
                # self.last_position[input_key] = input_data[0]
                # VSCANNER
                self.last_position[input_key] = self.fast_axis_start

            else:
                if input_key == f"{self.fast_axis_name}":
                    # use "middle pos" for fast axis only
                    # STEPPER
                    # pos_list.append((self.last_position[input_key] + input_data[ii] ) / 2)

                    # VSCANNER
                    pos = self.scan_step * scan_point_index + self.fast_axis_start
                    # print(f"pos={pos}")
                    pos_list.append((self.last_position[input_key] + pos ) / 2 )

                    # update last_position
                    self.last_position[input_key] = pos_list[ii]

                else:
                    pos_list.append(input_data[ii])

                    # update last_position
                    self.last_position[input_key] = input_data[ii]

        output_data[self.chan_out_name[input_key]] = pos_list
        #print(f"output_data={output_data}")
        return(output_data)

    def prepare(self):
        self.last_position = dict()
        self.last_position[self.fast_axis_name] = None
        self.last_position[self.slow_axis_name] = None

    def start(self):
        self.scan_start_time = time.time()

    def stop(self):
        pass
