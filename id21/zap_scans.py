
import time
import numpy

from bliss.scanning.chain import AcquisitionMaster
from bliss.scanning.channel import AcquisitionChannel
from bliss.scanning.toolbox import ChainBuilder
from bliss.common.logtools import disable_user_output
from bliss.shell.standard import debugon

from bliss.scanning.acquisition.motor import MotorMaster, LinearStepTriggerMaster
from bliss.scanning.acquisition.musst import MusstAcquisitionMaster, MusstAcquisitionSlave
from bliss.scanning.acquisition.calc import CalcChannelAcquisitionSlave, CalcHook

from bliss.scanning.scan import Scan, ScanPreset
from bliss.scanning.chain import AcquisitionChain, ChainPreset, ChainIterationPreset

from bliss.controllers.counter import CalcCounterController
from bliss.controllers.counter import SamplingCounterController

from bliss.controllers.ct2.device import AcqMode as CT2AcqMode
from bliss.controllers.ct2.client import CT2Controller

from bliss.controllers.mca.base import TriggerMode, BaseMCA
from bliss.controllers.mosca.base import McaCounterController

from bliss.scanning.scan_info import ScanInfo

from bliss import setup_globals





class _musst_calculator(CalcHook):
    """ computes extra channels from musst raw channels:
       - time: must timer in seconds
       - samy: centred positions of samy [in samy units] (from encoder CH3)
       - samz:         positions of samz [in samz units] (from encoder CH4) """

    def __init__(self, scan_params):
        """ """
        self.params = scan_params

        self.channels = ['time']
        self.raw2calc = {'RAW_TIMER':'time', 'RAW_CH3': 'samy', 'RAW_CH4':'samz' }
        self.factor = {'time': self.params['musst_time_factor'], 'samy':self.params['samy_steps_per_unit'], 'samz':self.params['samz_steps_per_unit'] }
        
        if self.params['fast_motor'] in ['samy', 'samz']:
            self.channels.append(self.params['fast_motor'])

        if self.params['scan_dim'] == 2:
            if self.params['slow_motor'] in ['samy', 'samz']:
                self.channels.append(self.params['slow_motor'])

        self.last_value = {k:None for k in self.channels}

    def compute(self, sender, in_data_dict):
        # musst channels => ['RAW_TIMER', 'RAW_CH3', 'RAW_CH4']
        chname = sender.name.split(':')[-1]
        chan = self.raw2calc.get(chname)
        if chan in self.channels:
            data = in_data_dict[chname]/self.factor[chan]
            dlen = len(data)
            output = None
            
            if dlen > 0:
                if chname == 'RAW_CH4': # just emit converted encoder values
                    if self.last_value[chan] is not None:
                        output = {chan: data}
                    else:
                        output = {chan: data[1:]} # inhibit first position
                
                else: # convert and compute centred positions (for RAW_TIMER and RAW_CH3)

                    if self.last_value[chan] is not None:
                        dout = [(self.last_value[chan] + data[0])/2, ]
                        dout.extend( [ (data[i-1] + data[i])/2  for i in range(1, dlen)] )
                        output = {chan: dout}

                    elif dlen > 1:
                        dout = [ (data[i-1] + data[i])/2  for i in range(1, dlen)]
                        output = {chan: dout}
                
                self.last_value[chan] = data[-1]
                return output

    def prepare(self):
        pass

    def start(self):
        self.last_value = {k:None for k in self.channels}

    def stop(self):
        pass


class FastMotorMaster(MotorMaster):
    def __init__(self, positions,
        axis,
        start,
        end,
        time=0,
        undershoot=None,
        undershoot_start_margin=0,
        undershoot_end_margin=0,
        trigger_type=AcquisitionMaster.SOFTWARE,
        backnforth=False,
        **keys,
        ):

        super().__init__(axis,
        start,
        end,
        time=time,
        undershoot=undershoot,
        undershoot_start_margin=undershoot_start_margin,
        undershoot_end_margin=undershoot_end_margin,
        trigger_type=trigger_type,
        backnforth=backnforth,
        **keys,)


        self.chname = f"{self.name}:{axis.name}"
        chan = AcquisitionChannel(self.chname, numpy.float64, ())
        self.channels.append(chan)

        # The positions that will be emitted by the FastMotorMaster
        # For Flint to see the correct dimension from the first top-master found in the scan_info.
        # The first point is removed (start=1) in order to align with the integrated counters (i.e value per pixel/interval)
        self.positions = positions  # dimension == x_intervals

    def start(self):
        super().start()
        self.channels.update({self.chname: self.positions})


class SlowLinearStepTriggerMaster(LinearStepTriggerMaster):
    def __init__(self, xdim, nb_point, *args, **keys):
        super().__init__(nb_point, *args, **keys)
        self.xdim = xdim # number of intervals along the x axis
        self.nb_point = nb_point
        self._line_number = 0
        self._t0_line = time.time()

    def trigger(self):
        self._line_number += 1
        pld = time.time() - self._t0_line # previous line duration
        self._t0_line = time.time()

        # \33[2K: erase line ; \r: return to begining of line
        print(f"\33[2K \rSLSTM trigger() line:{self._line_number:4d}/{self.nb_point}"
              f" {self.next_mv_cmd_arg[1]:g}  previous line duration={pld:g}s",
              end="")

        self.trigger_slaves()

        # build list of slow axis positions (repeating the same position as much as the number of intervals along the x axis)
        axes = list(self._axes.keys()) 
        positions = [[axis.position]*self.xdim for axis in axes]
        self.channels.update_from_iterable(positions)

        self.wait_slaves()


def trig(musst_ctrl, gatewidth=0.01):
    musst_ctrl.BTRIG=1
    time.sleep(gatewidth)
    musst_ctrl.BTRIG=0


def check_fast_sync():
    # ---- Check synchro MUSST/motor
    # to be parametrized :(
    musst_ctrl = setup_globals.musst_sxm
    fast_mot = setup_globals.samy
    _ = fast_mot.__info__()  # F. lazy init...
    musst_ch3 = musst_ctrl.get_channel(3).value
    mot_enc = fast_mot.controller.read_encoder(fast_mot.encoder)
    if ( abs(musst_ch3 - mot_enc) > 5):
        _msg = "WARNING: musst counter CH3 not synchronized with samy encoder\n"
        _msg += f"musst_ch3={musst_ch3}  mot_enc={mot_enc}"
        print(_msg)
        musst_ctrl.get_channel(3).value = mot_enc
        musst_ch3 = musst_ctrl.get_channel(3).value
        _msg = f"resynchronization DONE: musst_ch3={musst_ch3} \n"
        print(_msg)


def build_fast_chain(chain, builder, fast_master, scan_params, mcablocksize=None):

    musst_ctrl = setup_globals.musst_sxm

    # ----- MUSST Master --------------------------------------------
    if 1:
        _start_pos = int(scan_params['x_start']    * scan_params['fast_fac'])
        _time_step = int(scan_params['count_time'] * scan_params['musst_time_factor'])
        _gatewidth = int(scan_params['gatewidth']  * scan_params['musst_time_factor'])

        store_list=['RAW_TIMER', 'RAW_CH3', 'RAW_CH4']
        if scan_params['fast_is_piezo']:
            musst_program = "/musst_program/BTRIG-TIME_START-TRIG.mprg"
            musst_vars = {"TIME_STEP": _time_step,
                          "NPULSES":scan_params['x_npoints'],
                          "GATEWIDTH":_gatewidth,
                          }
        else:
            musst_program = "/musst_program/BTRIG-TIME_START-POS.mprg"
            musst_vars = {"STARTPOS": _start_pos,
                          "TIME_STEP": _time_step,
                          "NPULSES":scan_params['x_npoints'],
                          "GATEWIDTH":_gatewidth,
                          }

        musst_acq_master = MusstAcquisitionMaster(musst_ctrl,
                                                musst_program,
                                                program_start_name="TEST",
                                                vars=musst_vars)

        chain.add(fast_master, musst_acq_master)

    # ------ MUSST counters ----------------------------------------------------
    if 1:
        musst_acq_device = MusstAcquisitionSlave(musst_ctrl, store_list=store_list)
        chain.add(musst_acq_master, musst_acq_device)


    # ------ HANDLE MCA CONTROLLERS ----------------------------
    if 1:
        # The MCA should always take mca_params["npoints"] = number of intervals|pixels  (i.e x_npoints - 1)
        #
        # Internally if using trigger_mode==SYNC, the mca.hardware_points is set to  mca_params["npoints"] + 1
        # in order to compensate the fact that in this mode the mca starts the acquisition on scan start()
        # and not when receiving the first hard trig when x_mot_pos == x_start.
        # The MCA acqObj handle this internally and it discards the first measure done @ x_mot_pos == x_start
        #
        #
        # (TO BE TESTED) If using trigger_mode==GATE, the acquisition is expected to start when receiving the first trigger
        # (up or down edge of the gate).

        mca_params = {}
        mca_params["npoints"] = scan_params['x_npoints'] - 1  # x_npoints == number of triggers (= intervals +1) (= number of pixels + 1)
        mca_params["trigger_mode"] = TriggerMode.SYNC
        mca_params["read_all_triggers"] = False        # False is the default value (it discards the first point wich corresponds to irrelevant data)
        # mca_params["preset_time"] = 0.9              #  not used in SYNC trigger mode

        # time_slice = 2.0
        # # adjuste block size to fetch data all <time_slice> seconds.
        line_duration = mca_params["npoints"] * scan_params['count_time']
        # if line_duration < time_slice:
        #     block_size = None # Passing None, maximum block_size will be set.
        # else:
        #     block_size = int(time_slice / scan_params['count_time'])

        if mcablocksize is None:
            block_size = scan_params['x_npoints']
        else:
            block_size = mcablocksize
        
        mca_params["block_size"] = block_size
        scan_count_time = scan_params['count_time']
        print(f"line_duration={line_duration:g}s  count_time={scan_count_time}s  block_size={block_size}" )

        # mca_params["polling_time"] = 0.1
        # mca_params["spectrum_size"] = None
        mca_params["prepare_once"] = False   # False to re-do 'prepare' at each line.
        mca_params["start_once"]   = False   # False to re-do 'start' at each line.

        for node in builder.get_nodes_by_controller_type(BaseMCA):
            refresh_rate = node.controller.refresh_rate
            if refresh_rate >= scan_params['count_time']:
                #raise ValueError(f"count_time {scan_params['count_time']} cannot be shorter than refresh_rate ({refresh_rate})")
                print(f"WARNING: count_time {scan_params['count_time']} is shorter than refresh_rate ({refresh_rate})")
            node.set_parameters(acq_params=mca_params)
            chain.add(musst_acq_master, node)

    # ------ HANDLE MOSCA CONTROLLERS ----------------------------
    if 1:
        mosca_params = {}
        mosca_params["npoints"] = scan_params['x_npoints'] # mosca wants number of triggers
        mosca_params["trigger_mode"] = 'SYNC'
        mosca_params["preset_time"] = scan_params['count_time']
        mosca_params["read_all_triggers"] = False # drop first trigger data

        ylinesnum = scan_params.get('y_npoints')
        if ylinesnum is not None:
            mosca_params["wait_frame_id"] = [scan_params['x_npoints']] * ylinesnum
            mosca_params["prepare_once"] = True
            mosca_params["start_once"] = False
        else:
            mosca_params["wait_frame_id"] = [scan_params['x_npoints']]

        mosca_params["read_all_triggers"] = False # drop first trigger data
        
        for node in builder.get_nodes_by_controller_type(McaCounterController):
            node.set_parameters(acq_params=mosca_params)
            chain.add(musst_acq_master, node)

    # ----- P201  as MASTER
    if 1:
        p201_epsilon = 100  # shift trig time ( in micro seconds )

        p201_params = {}
        p201_params["npoints"] = scan_params['x_npoints'] - 1
        p201_params["acq_expo_time"] = scan_params['count_time'] - p201_epsilon/1e6         #  used in mode 5 only
        p201_params["acq_point_period"] = None

        # p201_params["acq_mode"] = CT2AcqMode.ExtTrigReadout  # 7 count until next trig
        # if mode 7 is used -> need to add keep_first_point option.

        p201_params["acq_mode"] = CT2AcqMode.ExtTrigMulti   # 5 count for a given time  # (mode 5 keeps all points)

        p201_params["prepare_once"] = False
        p201_params["start_once"] = False

        for node in builder.get_nodes_by_controller_type(CT2Controller):
            node.set_parameters( acq_params=p201_params)

            # Add p201 counters under p201 master
            for child_node in node.children:
                child_node.set_parameters(acq_params={"count_time": scan_params['count_time']})
                # no chain_add : children are automaticaly added under the parent.

            # Add p201 master under musst.
            chain.add(musst_acq_master, node)

    # ---- CALC CHANNELS -------------------------------------
    if scan_params['fast_is_piezo'] == False or scan_params.get('slow_is_piezo') == False:

        _musstcalchook = _musst_calculator(scan_params)
        musst_calc_chan = CalcChannelAcquisitionSlave("musst_encoders",
                                    [musst_acq_device],
                                    _musstcalchook,
                                    _musstcalchook.channels,
        )
        chain.add(musst_acq_master, musst_calc_chan)

    # ---- CALC COUNTERS  !!!! TO BE DONE AFTER  P201  !!!
    if 1:
        for node in builder.get_nodes_by_controller_type(CalcCounterController):
            node.set_parameters( acq_params={"npoints": scan_params['x_npoints']-1, "count_time": scan_params['count_time']})
            chain.add(musst_acq_master, node)


def build_slow_chain(chain, builder, slow_master, scan_params):

    # ---- OTHER DEVICES LIKE SAMPLING COUNTERS  (simulation diode) ------------
    if 1:
        for node in builder.get_nodes_by_controller_type(SamplingCounterController):
            node.set_parameters( acq_params={"npoints": scan_params['y_npoints'], "count_time": scan_params['count_time']})
            chain.add(slow_master, node)


def lscan(fast_motor, x_start, x_stop, x_intervals, count_time,
          *counters, save=True, save_images=True, run=True, gatewidth=0.001, mcablocksize=None, OpenCloseShutter=True):

    """
    """

    #--- init scan parameters ---
    if 1:
        x_npoints = x_intervals + 1
        x_travel_time =  count_time * x_intervals
        x_travel_distance = x_stop-x_start
        delta_x = x_travel_distance/x_intervals

        # compute expected positions
        # positions = [ x_start + i*delta_x for i in range(x_npoints) ]
        # compute expected centered positions
        centred_positions = [ x_start + i*delta_x - delta_x/2 for i in range(1, x_npoints) ]

        #--- get musst and opiom ---
        musst_ctrl = setup_globals.musst_sxm
        opiom_multiplexer = setup_globals.mpx
        opiom_multiplexer.switch('SEL_TRIG_OUT', 'ZAP')

        if fast_motor.name in ['sampy','sampz']:
            fast_is_piezo = True
            undershoot = 0
        else:
            fast_is_piezo = False
            undershoot = None

        scan_params = {'count_time':count_time, 'musst_time_factor':musst_ctrl.get_timer_factor(),
            'gatewidth':gatewidth, 'scan_dim':1,
            'samy_steps_per_unit':setup_globals.samy.steps_per_unit,
            'samz_steps_per_unit':setup_globals.samz.steps_per_unit,
            'x_start':x_start, 'x_stop':x_stop, 'x_npoints':x_npoints,
            'fast_motor':fast_motor.name,
            'fast_is_piezo':fast_is_piezo,
            'fast_fac':fast_motor.steps_per_unit,
            }

    if fast_motor.name == 'samy':
        print("use optimal velocity for samy")
        fast_motor.velocity = 0.1                   # changed 2024-06-10 HC CG

    #--- check the scan parameters are valid
    if fast_motor.name == 'samz':
        raise ValueError(f"samz is not available as a fast axis yet (cf musst prog)")

    if count_time <= gatewidth:
        raise ValueError(f"count_time {count_time} cannot be shorter than gatewidth ({gatewidth})")

    if fast_is_piezo:
        opiom_multiplexer.switch('MUSST_TRIG', 'VSCANNER1')
    else:
        check_fast_sync()
        if x_travel_distance/x_travel_time > 0.4:
            raise ValueError(f"{fast_motor.name} speed must be <= 0.4 mm/s")

    
    #--- build the acquisition chain ---
    chain = AcquisitionChain()
    #fast_master = MotorMaster(fast_motor, x_start, x_stop, time=x_travel_time, undershoot=undershoot)
    fast_master = FastMotorMaster(centred_positions, fast_motor, x_start, x_stop, time=x_travel_time, undershoot=undershoot)
    chain.add(fast_master)

    # ------ BUILDER for counters ----------------------------------------------
    builder = ChainBuilder(counters)
    build_fast_chain(chain, builder, fast_master, scan_params, mcablocksize)

    builder.print_tree()

    # ---- Check for not initilized node
    if builder.get_nodes_not_ready():
        builder.print_tree(not_ready_only=False)
        raise RuntimeError("There are not ready nodes -> cannot use these counters")

    scan_info = {
        "npoints": x_npoints,
        "type":"lscan",
        "count_time": count_time,
    }

    command_line = f"lscan {fast_motor.name} {x_start} {x_stop} {x_intervals} {count_time}"

    sc = Scan(
        chain,
        name=command_line,
        scan_info=scan_info,
        save=save,
        save_images=save_images,
        scan_saving=None,
        # data_watch_callback=StepScanDataWatch(),
    )

    if OpenCloseShutter==True:
       # Add Fast shutter preset to lscan:
       shutter_preset = ID21FastShutterChainIteratorPreset()
       sc.acq_chain.add_preset(shutter_preset)

    if run:
        with disable_user_output():
            sc.run()

    if fast_motor.name == 'samy':
        print("restore safe velocity for samy")
        fast_motor.velocity = 0.1

    return sc


def l2scan(fast_motor, x_start, x_stop, x_intervals, slow_motor, y_start, y_stop, y_intervals, count_time,
         *counters, save=True, save_images=True, run=True, gatewidth=0.001, mcablocksize=None):
    """
    * fast_motor:
    *  x_start:
    *  x_stop:
    *  x_intervals:
    * slow_motor:
    *  y_start:
    *  y_stop:
    *  y_intervals:
    * count_time:
    * *counters:
    * save:
    * save_images:
    * run:
    * gatewidth.001:
    * printout:

    """

    # debugon("*erial*")


    assert fast_motor != slow_motor

    t0 = time.time()
    timestamp_str = time.strftime("%Y-%m-%d %H:%M:%S")
    print(f"Starting l2scan {timestamp_str}")
    _correction_factor = 1.0
    estim_duration = ( x_intervals + 1 ) * count_time * ( y_intervals + 1 ) * _correction_factor
    # print(f"Estimated scan duration: {estim_duration}")
    #--- init scan parameters ---
    if 1:
        x_npoints = x_intervals + 1
        y_npoints = y_intervals + 1

        x_travel_time =  count_time * x_intervals
        x_travel_distance = x_stop-x_start
        delta_x = x_travel_distance/x_intervals

        # compute expected positions
        # positions = [ x_start + i*delta_x for i in range(x_npoints) ]
        # compute expected centered positions
        centred_positions = [ x_start + i*delta_x - delta_x/2 for i in range(1, x_npoints) ]

        #--- get musst and opiom ---
        musst_ctrl = setup_globals.musst_sxm
        opiom_multiplexer = setup_globals.mpx
        opiom_multiplexer.switch('SEL_TRIG_OUT', 'ZAP')

        if fast_motor.name in ['sampy','sampz']:
            fast_is_piezo = True
            undershoot = 0
        else:
            fast_is_piezo = False
            undershoot = None

        if slow_motor.name in ['sampy','sampz']:
            slow_is_piezo = True
        else:
            slow_is_piezo = False

        scan_params = {'count_time':count_time, 'musst_time_factor':musst_ctrl.get_timer_factor(),
            'gatewidth':gatewidth, 'scan_dim':2,
            'samy_steps_per_unit':setup_globals.samy.steps_per_unit,
            'samz_steps_per_unit':setup_globals.samz.steps_per_unit,
            'x_start':x_start, 'x_stop':x_stop, 'x_npoints':x_npoints,
            'y_start':y_start, 'y_stop':y_stop, 'y_npoints':y_npoints,
            'fast_is_piezo':fast_is_piezo, 'slow_is_piezo':slow_is_piezo,
            'fast_motor':fast_motor.name, 'slow_motor':slow_motor.name,
            'fast_fac':fast_motor.steps_per_unit, 'slow_fac':slow_motor.steps_per_unit,
            }

    if fast_motor.name == 'samy':
        print("use optimal velocity for samy")
        fast_motor.velocity = 0.1                   # changed by HC CG 2024-06


    #--- check the scan parameters are valid
    if fast_motor.name == 'samz':
        raise ValueError(f"samz is not available as a fast axis yet (cf musst prog)")

    if count_time <= gatewidth:
        raise ValueError(f"count_time {count_time} cannot be shorter than gatewidth ({gatewidth})")

    if fast_is_piezo:
        opiom_multiplexer.switch('MUSST_TRIG', 'VSCANNER1')
    else:
        check_fast_sync()
        if x_travel_distance/x_travel_time > 0.4:
            raise ValueError(f"{fast_motor.name} speed must be <= 0.4 mm/s")

    #--- build the acquisition chain ---
    chain = AcquisitionChain()
    #fast_master = MotorMaster(fast_motor, x_start, x_stop, time=x_travel_time, undershoot=undershoot)
    fast_master = FastMotorMaster(centred_positions, fast_motor, x_start, x_stop, time=x_travel_time, undershoot=undershoot)
    #slow_master = LinearStepTriggerMaster(y_npoints, slow_motor, y_start, y_stop)
    slow_master = SlowLinearStepTriggerMaster(x_intervals, y_npoints, slow_motor, y_start, y_stop)

    chain.add(slow_master, fast_master)

    # ------ BUILDER for counters ----------------------------------------------
    builder = ChainBuilder(counters)
    build_fast_chain(chain, builder, fast_master, scan_params, mcablocksize)
    build_slow_chain(chain, builder, slow_master, scan_params)

    print(chain._tree)
    
    # ---- Check for not initilized node
    if builder.get_nodes_not_ready():
        builder.print_tree(not_ready_only=False)
        raise RuntimeError("There are not ready nodes -> cannot use these counters")

    total_points = x_intervals*y_npoints
    if fast_is_piezo:
        fast_axis_name = f"axis:{fast_motor.name}"
    else:
        fast_axis_name = f"{fast_motor.name}"

    if slow_is_piezo:
        slow_axis_name = f"axis:{slow_motor.name}"
    else:
        slow_axis_name = f"{slow_motor.name}"

    scan_info_dict = {
        "npoints": total_points,
        "npoints1": x_intervals,
        "npoints2": y_npoints,
        "type": "l2scan",              # random name to avoid to be ignored by scan info listener
        "count_time": count_time,
        "data_dim":2,
        "start": [y_start, x_start],
        "stop": [y_stop, x_stop ], 
    }

    scan_info = ScanInfo()
    scan_info.update(scan_info_dict)

    # X
    scan_info.set_channel_meta(
        fast_axis_name,
        group="g1",             # The group has to be the same for all this channels
        axis_id=0,               # This is the fast axis
        axis_kind="forth",        # In forth direction only
        start=x_start,              # The grid has to be specified.
        stop=x_stop,
        axis_points= x_intervals,  # 
        points=total_points,         # Optionally the full number of points can be specified
    )


    # Y
    scan_info.set_channel_meta(
        slow_axis_name,         # 
        group="g1",             # 
        axis_id=1,              # 
        axis_kind="forth",      # 
        start=y_start,          # 
        stop=y_stop,            # 
        axis_points=y_npoints,  # 
        points=total_points,    # 
    )

    # ???  to be parametrized depending on scanned counters.
    cnt_name = "fx2:realtime_det0"
    scan_info.set_channel_meta(cnt_name, group="g1")

    # Request a specific scatter to be displayed
    scan_info.add_scatter_plot(x=fast_axis_name, y=slow_axis_name, value=cnt_name)

    command_line = f"l2scan {fast_motor.name} {x_start} {x_stop} {x_intervals} "
    command_line += f"{slow_motor.name} {y_start} {y_stop} {y_intervals} {count_time}"

    sc = Scan(
        chain,
        name=command_line,
        scan_info=scan_info,
        save=save,
        save_images=save_images,
        scan_saving=None,
        # data_watch_callback=StepScanDataWatch(),
    )


    # Add Fast shutter preset to l2scan:
    shutter_preset = ID21FastShutterChainIteratorPreset()
    sc.acq_chain.add_preset(shutter_preset)

    if run:
        with disable_user_output():
            sc.run()

    if fast_motor.name == 'samy':
        print("restore safe velocity for samy")
        fast_motor.velocity = 0.1    # modified by Hiram due to error on samy closed loop

    real_duration = time.time() - t0
    print(f"\nscan duration={real_duration:g}s ({estim_duration})")

    return sc


class ID21FastShutterChainIteratorPreset(ChainPreset):
    """
    Custom preset to open/close shutter during lscan/l2scan
    """

    fast_shutter = setup_globals.fshut

    class Iterator(ChainIterationPreset):
        def __init__(self, shutter, iter_nb):
            self.fast_shutter = shutter
            self.iteration_nb = iter_nb
        def prepare(self):
            pass
        def start(self):
            self.fast_shutter.open()
            print(f"FSHUT_PRESET  Opening the shutter {self.iteration_nb}")
        def stop(self):
            self.fast_shutter.close()
            print(f"FSHUT_PRESET  closing the shutter {self.iteration_nb}")

            # restore GATE mode at end of the fast scan.
            self.fast_shutter.external_control.set("P201_GATE") 
            
    def get_iterator(self, acq_chain):
        iteration_nb = 0
        while True:
            yield ID21FastShutterChainIteratorPreset.Iterator(self.fast_shutter, iteration_nb)
            iteration_nb += 1



class ID21FastShutterChainPreset(ChainPreset):
    def prepare(self, acq_chain):
        print("FSHUT_PRESET Preparing")
    def start(self, acq_chain):
        print("FSHUT_PRESET  Opening the shutter ")
    def stop(self, acq_chain):
        print("FSHUT_PRESET  closing the shutter")
"""
from id21.zap_scans import ID21FastShutterChainPreset
shutter_preset = ID21FastShutterChainPreset()
DEFAULT_CHAIN.add_preset(shutter_preset)
DEFAULT_CHAIN.remove_preset(shutter_preset)
"""



class ID21FastShutterScanPreset(ScanPreset):
    def prepare(self, scan):
        print("FSHUT_PRESET Preparing")
    def start(self, scan):
        print("FSHUT_PRESET  Opening the shutter ")
    def stop(self, scan):
        print("FSHUT_PRESET  closing the shutter")
"""
from id21.zap_scans import ID21FastShutterChainIteratorPreset
#ss = ascan( sampy,10,30,10,1,p201,run=False)
shutter_preset = ID21FastShutterChainIteratorPreset()
ss. ?? add_preset(shutter_preset)
"""


#========================== NEW CODE WITH MOSCA AND TRIGGER_MODE GATE ======================================

MUST_PROG_GATE_START_TRIG = """
// scan parameters
UNSIGNED NTRIGGERS
UNSIGNED GATEWIDTH
UNSIGNED SLEEPTIME

// local variables
UNSIGNED NPOINTS
SIGNED LAST_TG

ALIAS SAMY = CH3
ALIAS SAMZ = CH4

// start on ITRIG
// trig in time
PROG LSCAN
    BTRIG 0
    CTSTOP TIMER
    CTRESET TIMER
    TIMER = 0
    CTSTART ONEVENT TIMER
    STORELIST TIMER SAMY SAMZ
    EMEM 0 AT 0
    NPOINTS = 0
    
    EVSOURCE ITRIG RISE
    AT ITRIG DO NOTHING
    
    LAST_TG = $TIMER
    WHILE (NPOINTS < NTRIGGERS) DO
       @TIMER = LAST_TG
       AT TIMER DO BTRIG
       @TIMER = LAST_TG + GATEWIDTH
       AT TIMER DO BTRIG STORE
       LAST_TG += GATEWIDTH
       LAST_TG += SLEEPTIME
       NPOINTS += 1
    ENDWHILE
    BTRIG 0
    
ENDPROG
"""

MUST_PROG_GATE_START_POS = """
// only for stepper motor as fast axis (use encoder or steps)

// scan parameters

UNSIGNED STARTPOS
UNSIGNED NTRIGGERS
UNSIGNED GATEWIDTH
UNSIGNED SLEEPTIME

// local variables
UNSIGNED NPOINTS
SIGNED LAST_TG

// 
ALIAS SAMY = CH3
ALIAS SAMZ = CH4


// start in position
// trig in time
PROG LSCAN
    BTRIG 0
    CTSTOP TIMER
    CTRESET TIMER
    TIMER = 0
    STORELIST TIMER SAMY SAMZ
    EMEM 0 AT 0
    
    NPOINTS = 0

    EVSOURCE SAMY UP
    @SAMY = STARTPOS
    AT SAMY DO CTSTART TIMER
    LAST_TG = 0
    WHILE (NPOINTS < NTRIGGERS) DO
       @TIMER = LAST_TG
       AT TIMER DO BTRIG
       @TIMER = LAST_TG + GATEWIDTH
       AT TIMER DO BTRIG STORE
       LAST_TG += GATEWIDTH
       LAST_TG += SLEEPTIME
       NPOINTS += 1
    ENDWHILE
    BTRIG 0
    
ENDPROG
"""


def lscan_mosca(fast_motor, x_start, x_stop, x_intervals, count_time,
          *counters, save=True, save_images=True, run=True, sleeptime=0.0001, OpenCloseShutter=True):

    """
    """

    #--- init scan parameters ---
    if 1:
        x_npoints = x_intervals + 1
        x_travel_time =  count_time * x_intervals
        x_travel_distance = x_stop-x_start
        delta_x = x_travel_distance/x_intervals

        # compute expected positions
        # positions = [ x_start + i*delta_x for i in range(x_npoints) ]
        # compute expected centered positions
        centred_positions = [ x_start + i*delta_x - delta_x/2 for i in range(1, x_npoints) ]

        #--- get musst and opiom ---
        musst_ctrl = setup_globals.musst_sxm
        opiom_multiplexer = setup_globals.mpx
        opiom_multiplexer.switch('SEL_TRIG_OUT', 'ZAP')

        if fast_motor.name in ['sampy','sampz']:
            fast_is_piezo = True
            undershoot = 0
        else:
            fast_is_piezo = False
            undershoot = None

        scan_params = {'count_time':count_time, 'musst_time_factor':musst_ctrl.get_timer_factor(),
            'sleeptime':sleeptime, 'scan_dim':1,
            'samy_steps_per_unit':setup_globals.samy.steps_per_unit,
            'samz_steps_per_unit':setup_globals.samz.steps_per_unit,
            'x_start':x_start, 'x_stop':x_stop, 'x_npoints':x_npoints,
            'fast_motor':fast_motor.name,
            'fast_is_piezo':fast_is_piezo,
            'fast_fac':fast_motor.steps_per_unit,
            }

    if fast_motor.name == 'samy':
        print("use optimal velocity for samy")
        fast_motor.velocity = 0.1                   # changed by HC CG 2024-06

    #--- check the scan parameters are valid
    if fast_motor.name == 'samz':
        raise ValueError(f"samz is not available as a fast axis yet (cf musst prog)")

    if fast_is_piezo:
        opiom_multiplexer.switch('MUSST_TRIG', 'VSCANNER1')
    else:
        check_fast_sync()
        if x_travel_distance/x_travel_time > 0.4:
            raise ValueError(f"{fast_motor.name} speed must be <= 0.4 mm/s")

    
    #--- build the acquisition chain ---
    chain = AcquisitionChain()
    fast_master = FastMotorMaster(centred_positions, fast_motor, x_start, x_stop, time=x_travel_time, undershoot=undershoot)
    chain.add(fast_master)

    # ------ BUILDER for counters ----------------------------------------------
    builder = ChainBuilder(counters)
    build_fast_chain_mosca(chain, builder, fast_master, scan_params)
    builder.print_tree()

    # ---- Check for not initilized node
    if builder.get_nodes_not_ready():
        builder.print_tree(not_ready_only=False)
        raise RuntimeError("There are not ready nodes -> cannot use these counters")

    scan_info = {
        "npoints": x_npoints,
        "type":"lscan",
        "count_time": count_time,
    }

    command_line = f"lscan {fast_motor.name} {x_start} {x_stop} {x_intervals} {count_time}"

    sc = Scan(
        chain,
        name=command_line,
        scan_info=scan_info,
        save=save,
        save_images=save_images,
        scan_saving=None,
    )

    if OpenCloseShutter==True:
       # Add Fast shutter preset to lscan:
       shutter_preset = ID21FastShutterChainIteratorPreset()
       sc.acq_chain.add_preset(shutter_preset)

    if run:
        with disable_user_output():
            sc.run()

    if fast_motor.name == 'samy':
        print("restore safe velocity for samy")
        fast_motor.velocity = 0.1 

    return sc


def build_fast_chain_mosca(chain, builder, fast_master, scan_params):

    musst_ctrl = setup_globals.musst_sxm

    # ----- MUSST Master --------------------------------------------
    if 1:
        _start_pos = int(scan_params['x_start']    * scan_params['fast_fac'])
        _gatewidth = int(scan_params['count_time'] * scan_params['musst_time_factor'])
        _sleeptime = int(scan_params['sleeptime']  * scan_params['musst_time_factor'])

        store_list=['RAW_TIMER', 'RAW_CH3', 'RAW_CH4']
        if scan_params['fast_is_piezo']:
            musst_program = MUST_PROG_GATE_START_TRIG
            musst_vars = {
                          "NTRIGGERS":scan_params['x_npoints'],
                          "GATEWIDTH":_gatewidth,
                          "SLEEPTIME":_sleeptime,
                          }
        else:
            musst_program = MUST_PROG_GATE_START_POS
            musst_vars = {"STARTPOS": _start_pos,
                          "NTRIGGERS":scan_params['x_npoints'],
                          "GATEWIDTH":_gatewidth,
                          "SLEEPTIME":_sleeptime,
                          }

        musst_acq_master = MusstAcquisitionMaster(musst_ctrl,
                                                program_data=musst_program,
                                                program_start_name="LSCAN",
                                                vars=musst_vars)

        chain.add(fast_master, musst_acq_master)

    # ------ MUSST counters ----------------------------------------------------
    if 1:
        musst_acq_device = MusstAcquisitionSlave(musst_ctrl, store_list=store_list)
        chain.add(musst_acq_master, musst_acq_device)


    # ------ HANDLE MOSCA CONTROLLERS ----------------------------
    if 1:
        
        mosca_params = {}
        mosca_params["npoints"] = scan_params['x_npoints'] - 1
        mosca_params["trigger_mode"] = 'GATE'
        mosca_params["preset_time"] = scan_params['count_time']
        mosca_params["wait_frame_id"] = [scan_params['x_npoints'] - 1]

        for node in builder.get_nodes_by_controller_type(McaCounterController):
            node.set_parameters(acq_params=mosca_params)
            chain.add(musst_acq_master, node)

    # ----- P201  as MASTER
    if 1:
        p201_params = {}
        p201_params["npoints"] = scan_params['x_npoints'] - 1
        p201_params["acq_expo_time"] = scan_params['count_time']
        p201_params["acq_point_period"] = None
        p201_params["acq_mode"] = CT2AcqMode.ExtGate
        p201_params["prepare_once"] = True
        p201_params["start_once"] = True

        for node in builder.get_nodes_by_controller_type(CT2Controller):
            node.set_parameters( acq_params=p201_params)

            # Add p201 counters under p201 master
            for child_node in node.children:
                child_node.set_parameters(acq_params={"count_time": scan_params['count_time']})
                # no chain_add : children are automaticaly added under the parent.

            # Add p201 master under musst.
            chain.add(musst_acq_master, node)

    # ---- CALC CHANNELS -------------------------------------
    if scan_params['fast_is_piezo'] == False or scan_params.get('slow_is_piezo') == False:

        _musstcalchook = _musst_calculator(scan_params)
        musst_calc_chan = CalcChannelAcquisitionSlave("musst_encoders",
                                    [musst_acq_device],
                                    _musstcalchook,
                                    _musstcalchook.channels,
        )
        chain.add(musst_acq_master, musst_calc_chan)

    # ---- CALC COUNTERS  !!!! TO BE DONE AFTER  P201  !!!
    if 1:
        for node in builder.get_nodes_by_controller_type(CalcCounterController):
            node.set_parameters( acq_params={"npoints": scan_params['x_npoints']-1, "count_time": scan_params['count_time']})
            chain.add(musst_acq_master, node)



    
