

#
#    A QUOI CA SERT ?
#


class icepap_pos_motor(object):
    
    def __init__(self, axis):
        self.axis = axis
        
    def read(self):
        controller = self.axis.controller
        address = self.axis.address
        command = f"?POS motor {address}"
        val = int(controller.raw_write_read(command))
        return val
