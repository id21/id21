


# to be load with :
#  from id21.absorbers_open_close import abs1_close, abs1_open, abs1_status
#  from id21.absorbers_open_close import abs2_close, abs2_open, abs2_status


from bliss.config import static
cfg = static.get_config()

from bliss.common.logtools import user_print


import gevent

"""
Functions to re-open absorbers (bsh1 / bsh2).
Downstream valves have to be re-opened first.
"""


# def bas1_test_reset():
#     bsh1 = cfg.get("bsh1")
# 
#     bsh1._TangoShutter__control.reset()
#     while (bsh1.state.name == 'FAULT'):
#         print(bsh1.state)
#         gevent.sleep(0.1)
# 


def abs1_open():
    """
    * Open rv4
    * Reset bsh1
    * Open bsh1
    """
    bsh1 = cfg.get("bsh1")
    user_print("bsh1 is", bsh1.state.value)
    user_print("Opening RV4")
    rv4 = cfg.get("rv4")
    rv4.open()
    user_print("Resetting BSH1")
    bsh1.reset()
    user_print("Waiting bsh1 to be resetted", end="") # takes ~ 1 to 2 seconds
    while(bsh1._tango_state == 'FAULT' ):
        user_print(".", end="")
        gevent.sleep(0.2)
    user_print("")
    user_print("Opening BSH1")
    bsh1.open()

def abs1_close():
    bsh1 = cfg.get("bsh1")
    bsh1.close(timeout=5)


def abs1_status():
    bsh1 = cfg.get("bsh1")
    return bsh1.state.value


def abs2_open():
    """
    * Open rv9
    * Reset bsh2
    * Open bsh2
    """
    bsh2 = cfg.get("bsh2")
    user_print("bsh2 is", bsh2.state.value)
    user_print("Opening RV9")
    rv9 = cfg.get("rv9")
    rv9.open()
    user_print("Resetting BSH2")
    bsh2.reset()
    user_print("Waiting bsh2to be resetted", end="") # takes ~ 1 to 2 seconds

    gevent.sleep(3)

    while(bsh2._tango_state == 'FAULT'):
        user_print(".", end="")
        gevent.sleep(0.2)
    user_print("")
    user_print("Opening BSH2")
    bsh2.open()

def abs2_close():
    bsh2 = cfg.get("bsh2")
    bsh2.close(timeout=5)

def abs2_status():
    bsh1 = cfg.get("bsh2")
    return bsh1.state.value

