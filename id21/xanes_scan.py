from bliss import setup_globals
from bliss.common.scans import ascan
from bliss.common.logtools import user_print

def xanes_scan(motor, start_pos, stop_pos, intervals, count_time, *detectors_and_shut):
    """
    Custom ID21 Xanes scan.
    """
    fast_shutter = setup_globals.fshut
    das_list = list(detectors_and_shut)

    if fast_shutter in das_list:
        # remove fshut from list:
        das_list.remove(fast_shutter)

        user_print("[Fast Shutter]: Gated by the P201.")

        fast_shutter.external_control.set('P201_GATE')
        scan = ascan( motor, start_pos, stop_pos, intervals, count_time, *das_list)
    else:
        user_print("[Fast Shutter]: Open along all the scan.")

        # This will open the shutter and close it at end
        # of the ascan and in case of scan abort (ctrl-c or error).
        with fast_shutter:
            scan = ascan( motor, start_pos, stop_pos, intervals, count_time, *das_list)

    return scan
