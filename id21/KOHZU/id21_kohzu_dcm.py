# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2020 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

# 2023

import numpy
import tabulate
import gevent
import time
from scipy import signal
import glob


from bliss import setup_globals
from bliss.config import settings
from bliss.config.static import get_config
from bliss.shell.standard import umv
from bliss.common.utils import ColorTags, BOLD, RED
from bliss.common.plot import plot
from bliss.common.logtools import user_print
from bliss.scanning.chain import ChainPreset
from bliss.controllers.bliss_controller import BlissController
from bliss.controllers.monochromator import MonochromatorFixExit, EnergyTrackingObject
from bliss.shell.getval import getval_yes_no
from bliss.controllers.motors.icepap import Icepap

from id21.ESRF_DCM.esrf_dcm_acquisition_master import TrajectoryEnergyTrackerMaster

class ID21kohzu(MonochromatorFixExit):

    def __init__(self, config):
        super().__init__(config)

    """
    Initialization
    """
    def _init(self):
        super()._init()

    """
    Xtals
    """
    def _xtal_is_in(self, xtal):
        """
        Return True if <xtal> crystal is in the beam (whitin limits).
        """
        mot = self._motors["xtal_change"]
        _low_pos = self._xtals.get_xtals_config("low_pos")[xtal]
        _high_pos = self._xtals.get_xtals_config("high_pos")[xtal]
        _pos = mot.position

        if _pos >= _low_pos and _pos <= _high_pos:
            return True

        return False

    def _xtal_change(self, xtal):
        """
        <xtal> : xtal string from config (ex: 'Si333')
        Put crystal <xtal> into the beam.
        """
        print(RED("Macro DESACTIVATED"))
        if 0:
            mot =  self._motors["xtal_change"]

            _low_pos = self._xtals.get_xtals_config("low_pos")[xtal]
            _high_pos = self._xtals.get_xtals_config("high_pos")[xtal]
            _mid_pos = (_low_pos + _high_pos) / 2

            # Ask to user if brakes have been released.
            if not getval_yes_no("Are KOHZU xtal brakes released ?"):
                user_print("ok, aborting")
                return 0

            # move
            user_print(f"Moving {mot.name} to crystal {xtal} ({_mid_pos})")
            mot.move(_mid_pos)

class ID21undulatorTrackingObject(EnergyTrackingObject):

    def __init__(self, config):
        super().__init__(config)

        #Constants
        me = 9.1093836e-31  # kg
        qe = 1.6021766e-19
        hbar = 1.054572e-34 # m^2 kg/s
        c = 299792458 # m/s

        ### Source parameters
        Ering = 6.0 #GeV
        und_step = 0.05001 ### time quant of acceleration of undulator gap, seconds

        ### Calculated constants
        gamma = Ering / 0.511e-3 # Ering is in GeV
        self._c1 = qe / (2.0 * numpy.pi * me * c) / 1e3 # To have E in keV and gap in mm
        self._c2 = 8.0 * numpy.pi * hbar * c * gamma**2 / qe # To have E in keV and gap in mm

    def _ene2gap(self, energy, config):
        harm = float(config["harmonic"])
        Uperiod = float(config["Uperiod"])
        alpha = float(config["alpha"])

        UPPI = Uperiod / numpy.pi
        C2H = self._c2 * harm
        C1A1U = self._c1 * alpha * Uperiod
        gap = UPPI * numpy.log(C1A1U / numpy.sqrt(C2H /(Uperiod * energy) - 2.0))
        #gap = Uperiod / numpy.pi * numpy.log(self._c1 * self._a_un * Uperiod / (numpy.sqrt(self._c2 * harm /(Uperiod * energy) - 2.0)))

        return gap

    def _gap2ene(self, gap, config):
        harm = float(config["harmonic"])
        Uperiod = float(config["Uperiod"])
        alpha = float(config["alpha"])

        UPPI = Uperiod / numpy.pi
        C2H = self._c2 * harm
        C1A1U = self._c1 * alpha * Uperiod

        exp = numpy.exp(gap / UPPI)
        num = 2 + numpy.power(C1A1U / exp, 2)
        energy = C2H / (Uperiod * (num))

        return energy

class KohzuScanPreset(ChainPreset):
    
    def __init__(self, name, config):
        
        self._name = name
        self._config = config
        
        self._mono = config.get("monochromator")
        self._musst = config.get("musst")
        self._encoder = config.get("encoder")

        self._wanted = False
        
    def get_iterator(self, chain):
        """Yield ChainIterationPreset instances, if needed"""
        pass

    def prepare(self, chain):
            
        mono = self._mono._motors["bragg"]
        mono_enc = get_config().get("mono_enc")
        
        channel = self._musst.get_channel_by_name("mono").channel_id
        channel_str = f"CH{channel}"
        self._musst.putget(f"CH {channel_str} {mono.position*self._encoder.steps_per_unit}")

    def start(self, chain):
        pass

    def before_stop(self, chain):
        """
        Called at the end of the scan just before calling **stop** on detectors
        """
        pass

    def stop(self, chain):
        """
        Called at the end of the chain iteration.
        """
        pass
