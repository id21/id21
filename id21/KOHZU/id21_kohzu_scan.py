
from id21.SCANS.mscan import MonochromatorScans

class ID21kohzuContScan(MonochromatorScans):

    def __init__(self, name, config):
    
        super().__init__(name, config)
        
        self._scan_mux = None
        opiom_mux = config.get("_par_scan_multiplexer", None)
        if opiom_mux is not None:
            self._scan_mux = {}
            for mux in opiom_mux:
                for key, val in mux.items():
                    self._scan_mux[key] = val

    def _prescan(self):
            
        # OPIOM multiplexer to select trig source.
        if self._scan_mux is not None:
            opiom_mux = self._scan_mux["opiom_mux"]
            selector = self._scan_mux["selector"]
            if self._opiom_mux["value_out"] == "LAST":
                self._opiom_mux["value_saved"] = opiom_mux.getOutputStat(selector)
            opiom_mux.switch(selector, self._opiom_mux["value_in"])
