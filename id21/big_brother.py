from bliss import global_map
from bliss import setup_globals
from bliss.comm import rpc

# from bliss.config.beacon_object import BeaconObject

import pprint


class BigBrother:
    """
    helper to access RPC vacuum monitoring server.
    """
    def __init__(self, name, config):
        # self.beacon_obj = BeaconObject(config, share_hardware=False)
        global_map.register(self, tag=name)

        self.access = "tcp://0.0.0.0:8899"
        self.proxy = rpc.Client(self.access)

    def __info__(self):
        """info string"""
        str_info = " vacuum monitoring RPC server Access\n"
        str_info += f"{self.access} \n\n"
        str_info += f"monitoring state = {self.proxy.state()}\n\n"
        str_info += f"monitoring status = \n{self.proxy.status()}\n"

        str_info += self.get_log()
        return str_info

    def get_log(self):
        return self.proxy.get_log()

    def clear_log(self):
        self.proxy.clear_log()

    def debug_on(self):
        self.proxy.set_verbose_level(1)

    def debug_off(self):
        self.proxy.set_verbose_level(0)

    def stop(self):
        self.proxy.stop()

    def start(self):
        self.proxy.start()

    def state(self):
        return self.proxy.state()

    def status(self):
        return self.proxy.status()
