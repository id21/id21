"""
Tripod Nano21

Tripod Calculational Motor Controller for ID21 Nano end-station.

REAL:
               Leg1
                 ^
                 |
                 |
                 |
                 |
                 X Sample
                 |
                 height                   ^
                 |                        |
                 |                        |
                 V                       BEAM
 Leg2 <---------base------> Leg3

d(Leg1, Leg2) = d(Leg2, Leg3) = d(Leg3, Leg1) = base = 224.65 mm
height = 194.55 mm

dz(tripod_plane, sample) = 103.5 mm

CALC:
  - TZ mm
  - RX mrad
  - RY mrad

* Automatic calculation and move of legs for TZ / RX / RY
* Calculation but no move for corrections (just print values for manual corrections)

Δty = -103.5 * ΔRX
Δtx =  103.5 * ΔRY

base = 224.65
height = 194.55

Calculations:
    TZ = ( sz1 + sz2 + sz3 ) / 3.0
    RX = 1000 * ( sz2 - sz3 )  / base
    Ry = 1000 * ( 1 / height ) * ( ( sz2 + sz3 ) / 2 - sz1 )
ie:
 [ 1/3 ,           1/3,        1/3 ]
 [ 0,           1000/b,    -1000/b ]
 [ -1000/h, 1000/(2*h), 1000/(2*h) ]

"""

import time
import tabulate
from bliss.controllers.motor import CalcController
import numpy as np

class tripod_nano21(CalcController):
    def __init__(self, *args, **kwargs):
        CalcController.__init__(self, *args, **kwargs)

        self.base = self.config.get("base", float)
        self.height = self.config.get("height", float)

        self.t0 = time.perf_counter()

        if not self.base or not self.height :
            raise RuntimeError("Parameters `base` and `height` are not configured")

        self._axes_names = {}
        for axis in self.config.config_dict["axes"]:
            if "real leg" in axis['tags']:
                self._axes_names[axis["tags"]] = axis["name"].name
            else:
                self._axes_names[axis["tags"]] = axis["name"]

        # leg to virtual and virtual to leg matrix:
        b = self.base
        h = self.height
        self.l2v_matrix = np.array([[1/3 ,1/3, 1/3], [0, 1000/b , -1000/b], [-1000/h, 1000/(2*h), 1000/(2*h)] ])
        self.v2l_matrix = np.linalg.inv(self.l2v_matrix)

        self.real_axes = []

        # ???
        self.no_offset = self.config.get("no_offset", bool, True)


    def __info__(self):
        """
        info about tripod
        """
        print("--------- build info_str ... -----------", flush=True)

        info_str = "ID21 NANO TRIPOD \n"
        info_str += f"Base   = {self.base} mm\n"
        info_str += f"Height = {self.height} mm \n"

        l1 = self.axes[self._axes_names['real leg1']]
        l2 = self.axes[self._axes_names['real leg2']]
        l3 = self.axes[self._axes_names['real leg3']]

        tz = self.axes[self._axes_names['tz']]
        rx = self.axes[self._axes_names['rx']]
        ry = self.axes[self._axes_names['ry']]

        tab_axes = ["leg1", "leg2", "leg3", "TZ", "RX" ,"RY"]
        tab_real = ["nsz1", "nsz2", "nsz3"]

        tab_onoff = []
        for leg in self.real_axes:
            _onoff = not leg.controller.get_TS(leg)["motor_off"]
            tab_onoff.append("on" if _onoff else "off")

        tab_pos = [f"{xx.position:g}" for xx in [l1, l2, l3, tz, rx, ry]]

        # tabulate.PRESERVE_WHITESPACE = True
        # pos_table = tabulate.tabulate([tab_axes, tab_pos], tablefmt="plain") + "\n"
        pos_table = tabulate.tabulate([tab_axes, tab_real, tab_onoff, tab_pos])

        self.t0 = time.perf_counter()

        info_str += pos_table

#        info_str += f"   leg2 = {l2.position} \n "
#        info_str += f"   leg3 = {l3.position} \n "
#        info_str += f"   TZ = {tz.position} \n "
#        info_str += f"   RX = {rx.position} \n "
#        info_str += f"   RY = {ry.position} \n "

        return info_str


    def synchronize(self):
        """
        Call sync_hard on all real legs.
        ??? why not use .synch_hard() ???
        """
        for leg in self.real_axes:
            leg.sync_hard()

        time.sleep(0.2)
        print("\n", self.__info__(), "\n")

    def on(self):
        """
        Enable real motors
        """
        for leg in self.real_axes:
            leg.on()

        time.sleep(0.2)
        print("\n", self.__info__(), "\n")

    def off(self):
        """
        Disable real motors
        """
        for leg in self.real_axes:
            leg.off()

        time.sleep(0.2)
        print("\n", self.__info__(), "\n")

    def initialize_axis(self, axis):
        CalcController.initialize_axis(self, axis)

        # Store real axes in a list (for on() / off())
        # self.real_axes = [self.config.get("nsz1"), self.config.get("nsz2"), self.config.get("nsz3")]
        if self.real_axes == []:
            self.real_axes.append(self.axes[self._axes_names['real leg1']])
            self.real_axes.append(self.axes[self._axes_names['real leg2']])
            self.real_axes.append(self.axes[self._axes_names['real leg3']])


        # no_offset feature (True by default) to avoid offset accumulation.
        axis.no_offset = self.no_offset

    def calc_from_real(self, positions_dict):
        """
        Real legs lengths (l1, l2, l3 ) to Virtual Axes (Tz Rx Ry) Calculation.
        """
        l1 = positions_dict["leg1"]
        l2 = positions_dict["leg2"]
        l3 = positions_dict["leg3"]

        v_legs = np.array([l1, l2, l3])

        print(f"{time.perf_counter() - self.t0} v_legs=", v_legs)

        # 'Legs to Virtual' matrix
        v_virt = np.dot(self.l2v_matrix, v_legs)

        tz = v_virt[0]
        rx = v_virt[1]
        ry = v_virt[2]

        return {"tz": tz, "rx": rx, "ry": ry}

    def calc_to_real(self, positions_dict):
        """
        Inverse calculation: Virtual Axes (Tz Rx Ry) to Legs lengths (l1, l2, l3 ).
        """
        rx = positions_dict["rx"]
        ry = positions_dict["ry"]
        tz = positions_dict["tz"]

        v_virt = np.array([tz, rx, ry])

        # Virtual axes to legs matrix
        v_legs = np.dot(self.v2l_matrix, v_virt)

        l1 = v_legs[0]
        l2 = v_legs[1]
        l3 = v_legs[2]

        return {"leg1": l1, "leg2": l2, "leg3": l3}


