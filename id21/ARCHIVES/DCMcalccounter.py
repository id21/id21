import numpy

from bliss.common.measurement import CalcCounterController
from bliss.scanning.acquisition.calc import CalcHook

# Mean caclulaion
class LaserCalcCounterBase(CalcHook):
    
    def __init__(self, name, role, dep_counters):
        self.name = name
        self. role = role
        self.dep_names = [cnt.name for cnt in dep_counters]
        self.l0 = 2300.0
        self.l1 = 100.0
        self.l2 = 1054.0
        self.l1pl2 = self.l1 + self.l2
        self.l1ml2 = self.l1 - self.l2
        self.l2ml1 = self.l2 - self.l1
        self.l0pl1 = self.l0 + self.l1
        self.l2ml1m2l0 = self.l2 - self.l1 - 2*self.l0
	
        
    def prepare(self):
        self.data = {}
        
    def compute(self,sender,data_dict):
        # Gathering all needed data to calculate the mean
        # Datas of several counters are not emitted at the same time
        nb_point_to_emit = numpy.inf
        for cnt_name in self.dep_names:
            cnt_data = data_dict.get(cnt_name,[])
            data = self.data.get(cnt_name,[])
            if len(cnt_data):
                data = numpy.append(data,cnt_data)
                self.data[cnt_name]=data
            nb_point_to_emit = min(nb_point_to_emit,len(data))
        # Maybe noting to do
        if not nb_point_to_emit:
            return

        # Calculation
        data = {}
        data["zd1"] = 0.758 * self.data['em1z'][:nb_point_to_emit]
        data["yd1"] = 0.55 * self.data['em1y'][:nb_point_to_emit]
        data["zd2"] = 0.3356 * self.data['em2z'][:nb_point_to_emit]
        data["yd2"] = 0.282 * self.data['em2y'][:nb_point_to_emit]
                     
        data["tz"] = 0.5 * (1 - self.l2ml1m2l0 / self.l1pl2) * data["zd1"] - 0.5 * (1 + self.l2ml1m2l0 / self.l1pl2) * data["zd2"]
        data["ty"] = (1 + self.l0pl1 / self.l2ml1) * data["yd1"] - (self.l0pl1 / self.l2ml1) * data["yd2"]
        data["ry"] = numpy.arctan((data["zd1"] + data["zd2"]) / self.l1pl2) * 1e6
        data["rx"] = numpy.arctan((data["yd2"] - data["yd1"]) / self.l2ml1) * 1e6
                
        # Removing already computed raw datas
        self.data = {key:data[nb_point_to_emit:]
                     for key,data in self.data.items()}
                     
        # Return name musst be the same as the counter name:   
        ret_val = {self.name:data[self.role]}
        return ret_val

def LaserCreateCalcCounter(name, role, *counters):
    
    obj = LaserCalcCounterBase(name, role, counters)

    return CalcCounterController(name, obj, *counters)

