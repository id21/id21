import numpy

from bliss.scanning.chain import ChainNode
from bliss.common.measurement import BaseCounter
from bliss.common.utils import autocomplete_property
from bliss.controllers.acquisition import AcquisitionController, counter_namespace
from bliss.scanning.chain import AcquisitionDevice
from bliss.scanning.channel import AcquisitionChannel
from bliss.common.event import dispatcher
from bliss import global_map

class CalcCounterChainNode(ChainNode):
    def get_acquisition_object(self, scan_params, acq_params):

        # Check if Acquisition Devices of dependant counters already exist
        acq_devices = []

        for node in self._calc_dep_nodes.values():
            acq_obj = node.acquisition_obj
            if acq_obj is None:
                raise ValueError(
                    f"cannot create CalcAcquisitionDevice: acquisition object of {node}({node.controller}) is None!"
                )
            else:
                acq_devices.append(acq_obj)

        return CalcAcquisitionDevice(self.controller, acq_devices)

class CalcCounter(BaseCounter):
    def __init__(self, name, controller):
        self.__name = name
        self.__controller = controller

    @property
    def name(self):
        return self.__name

    @property
    def fullname(self):
        return self.name

    @autocomplete_property
    def controller(self):
        return self.__controller


class CalcCounterController(AcquisitionController):
    def __init__(self, name, config):
        
        super().__init__(name, chain_node_class=CalcCounterChainNode)
        
        counters = config.get("counters")
        
        self.__input_counters = []
        self.__output_counters = []
        self._counters = {}
        self.tags = {}
        
        # Config reading
        for cnt_conf in counters:
            cnt_name = cnt_conf.get("counter_name")
            controller = cnt_conf.get("controller")
            tags = cnt_conf.get("tags")
           
            if "input" in tags.split():
                cnt = self.get_counter_from_name(controller, cnt_name)
                self.tags[cnt.name] = tags.replace("input", "").strip()
                self.__input_counters.append(cnt)
            else:
                cnt = CalcCounter(cnt_name, self)
                self.__output_counters.append(cnt)
                self.tags[cnt.name] = tags
        
        # MAP registration
        global_map.register(self, ["counters"], tag=self.name)
        for cnt in self.__input_counters:
            global_map.register(cnt, [self.name], tag="input_"+cnt.name)
        for cnt in self.__output_counters:
            global_map.register(cnt, [self.name], tag="output_"+cnt.name)
            
    def get_counter_from_name(self, controller, name):
        for cnt in controller.counters:
            if cnt.name == name:
                return cnt
    
    @property
    def dependant_counters(self):
        '''
        Return the list of input counters and their dependancies
        '''
        dep_counters = {}
        for cnt in self.__input_counters:
            dep_counters[cnt.name] = cnt
            if isinstance(cnt, CalcCounter):
                dep_counters.update(
                    {cnt.name: cnt for cnt in cnt.controller.dependant_counters}
                )
        return counter_namespace(dep_counters)
    
    @property
    def counters(self):
        '''
        Return list of output counters
        '''
        return counter_namespace(self.__output_counters)
        
    def compute(self, sender, data_dict):
        """
        This method works only if all input_counters will generate the same number of points !!!
        It registers all data comming from the input counters.
        It calls calc_function with input counters data which have reach the same index
        This function is called once per counter (input and output).

        * <sender> = AcquisitionChannel 
        * <data_dict> = {'em1ch1': array([0.00256367])}
        """

        for cnt in self.__input_counters:
            # get registered data for this counter
            data = self.data.get(cnt.name,[])

            # get new data for this counter
            new_data = data_dict.get(cnt.name,[])
            
            # get number of registered data for this counter
            data_index = self.data_index.get(cnt.name, 0)
    
            # If any, add new data to registered data
            if len(new_data):
                data = numpy.append(data, new_data)
                self.data[cnt.name] = data
                
            self.data_index[cnt.name] = data_index + len(new_data)
        
        input_counter_index = [self.data_index[cnt.name] for cnt in self.__input_counters]
        new_data_index = min(input_counter_index)
        
        # print(f"\n{self.name} - {new_data_index} - {input_counter_index}")
        
        if self.emitted_index == new_data_index-1:
            return None
        
        # Build a dict of input counter data value indexed by tags instead of counter names.
        input_data_dict = {}
        for cnt in self.__input_counters:
            input_data_dict[self.tags[cnt.name]] = numpy.copy(self.data[cnt.name][self.emitted_index+1:new_data_index])
            
        self.emitted_index = new_data_index-1
        
        output_data_dict = self.calc_function(input_data_dict)
        
        return output_data_dict

    def prepare(self):
        # Store read input counter datas
        self.data = {}
        # last index of read input counter datas
        self.data_index = {}
        # index of last calculated counter datas
        self.emitted_index = -1

    def start(self):
        pass

    def stop(self):
        pass
        
    def calc_function(self, input_dict):
        raise NotImplementedError


class CalcAcquisitionDevice(AcquisitionDevice):
    """
    Helper to do some extra Calculation on counters.
    i.e: compute encoder position to user position
    Args:
        controller -- CalcCounterController Object
        src_acq_devices_list -- list or tuple of acq(device/master) you want to listen to.
    """

    def __init__(self, controller, src_acq_devices_list):
        
        name = "AD_" + controller.name
        
        AcquisitionDevice.__init__(
            self, controller, name, trigger_type=AcquisitionDevice.HARDWARE
        )
        self._connected = False

        self.src_acq_devices_list = src_acq_devices_list
        
        self.output_counters = {}

    def add_counter(self, counter):
        self.channels.append(
            AcquisitionChannel(counter.name, counter.dtype, counter.shape)
        )
        self.output_counters[counter.name] = counter

    def connect(self):
        if self._connected:
            return
        for acq_device in self.src_acq_devices_list:
            for channel in acq_device.channels:
                dispatcher.connect(self.new_data_received, "new_data", channel)
        self._connected = True

    def disconnect(self):
        if not self._connected:
            return
        for acq_device in self.src_acq_devices_list:
            for channel in acq_device.channels:
                dispatcher.disconnect(self.new_data_received, "new_data", channel)
        self._connected = False

    def prepare(self):
        self.device.prepare()
        self.connect()

    def new_data_received(self, event_dict=None, signal=None, sender=None):

        channel_data = event_dict.get("data")
        if channel_data is None:
            return
        channel = sender
        output_channels_data_dict = self.device.compute(
            sender, {channel.short_name: channel_data}
        )
                
        if output_channels_data_dict:
            for channel in self.channels:
                channel_data = output_channels_data_dict.get(self.device.tags[channel.short_name])
                if channel_data is not None:
                    # print(f"EMIT - {channel.short_name} - {len(channel_data)}")
                    channel.emit(channel_data)

    def start(self):
        self.device.start()

    def stop(self):
        self.disconnect()
        self.device.stop()
        
























