
import time
import numpy

from bliss.scanning.chain import AcquisitionMaster
from bliss.scanning.channel import AcquisitionChannel
from bliss.scanning.toolbox import ChainBuilder
from bliss.common.logtools import disable_user_output

from bliss.scanning.acquisition.motor import MotorMaster, LinearStepTriggerMaster
from bliss.scanning.acquisition.musst import MusstAcquisitionMaster, MusstAcquisitionSlave
from bliss.scanning.acquisition.calc import CalcChannelAcquisitionSlave, CalcHook

from bliss.scanning.scan import Scan, StepScanDataWatch
from bliss.scanning.chain import AcquisitionChain

from bliss.controllers.counter import CalcCounterController
from bliss.controllers.counter import SamplingCounterController

from bliss.controllers.ct2.device import AcqMode as CT2AcqMode
from bliss.controllers.ct2.client import CT2Controller

from bliss.controllers.mca.base import TriggerMode, BaseMCA
from bliss.controllers.emh import EMH

from bliss.data.display import FormatedTab
from bliss.flint.client.live_plots import LiveImagePlot

from bliss import setup_globals

class _musst_calculator(CalcHook):
    """
    """

    def __init__(self, scan_params):
        
        """ """
        self.index = None
        self.timer = None
        self.xi = None
        self.yi = None
        self.xreal = None
        self.yreal = None
        self.xcalc = None
        self.ycalc = None
        self.xerr = None
        self.yerr = None
        
        self.printout = scan_params['printout']
        self.count_time = scan_params['count_time']
        self.x_fac = scan_params['x_fac']
        self.x_npoints = scan_params['x_npoints']
        self.x_start = scan_params['x_start']
        self.x_stop = scan_params['x_stop']
        self.delta_x = scan_params['delta_x']
        
        self.scan_dim = scan_params['scan_dim']
        self.fast_is_piezo = scan_params['fast_is_piezo']
        
        if self.scan_dim == 2:
            self.y_fac = scan_params['y_fac']
            self.y_npoints = scan_params['y_npoints']
            self.y_start = scan_params['y_start']
            self.y_stop = scan_params['y_stop']
            self.delta_y = scan_params['delta_y']

            if self.fast_is_piezo:
                self.channels = ['index', 'timer', 'xpix', 'ypix', 'xcalc', 'ycalc']
            else:
                self.channels = ['index', 'timer', 'xpix', 'ypix', 'xcalc', 'xreal', 'xerr', 'ycalc', 'yreal', 'yerr']
        else:
            if self.fast_is_piezo:
                self.channels = ['index', 'timer', 'xpix', 'xcalc']
            else:
                self.channels = ['index', 'timer', 'xpix', 'xcalc', 'xreal', 'xerr']
            
        if self.printout:
            self._tab = FormatedTab(
                    [self.channels],
                    minwidth=12,
                    maxwidth=30,
                    col_sep='|',
                    lmargin="   ",
                )
            self._tab.set_column_params(0, {"flag": ""})
            self._tab.set_column_params(2, {"flag": ""})

            if self.scan_dim == 2:
                self._tab.set_column_params(3, {"flag": ""})
                if not self.fast_is_piezo:
                    self._tab.set_column_params(6, {"fpreci":".3", "dtype": "e"})
                    self._tab.set_column_params(9, {"fpreci":".3", "dtype": "e"})

            elif self.scan_dim == 1:
                if not self.fast_is_piezo:
                    self._tab.set_column_params(5, {"fpreci":".3", "dtype": "e"})

            self._tab.add_separator("-")
            self._tab.resize(maxwidth=30)

        
        if self.fast_is_piezo:
            self.musst_data = {'TIMER':[]}
        else:
            self.musst_data = {'TIMER':[], 'samy':[], 'samz':[]}
        
    def compute(self, sender, in_data_dict):

        """  """
        
        chname = sender.name.split(':')[-1]
        
        lg = len(list(in_data_dict[chname]))
        if lg>1:
            print(f"len(received[{chname}])", lg)
            
        for v in list(in_data_dict[chname]):
            self.musst_data[chname].append(v) 
        
        self.index = len(self.musst_data['TIMER']) - 1
        self.xi = self.index  % self.x_npoints
        self.xcalc = self.x_start + self.xi * self.delta_x - self.delta_x/2

        if self.scan_dim == 2:
            self.yi = self.index // self.x_npoints
            self.ycalc = self.y_start + self.yi * self.delta_y
        
        l = [len(v) for v in self.musst_data.values() ]
        if len(set(l)) == 1:  #check all len are equal
            if self.index == 0 and self.printout:
                print(self._tab)

            self.timer = self.musst_data['TIMER'][-1]

            data = {}
            data['index'] = self.index
            data['timer'] = self.timer
            data['xpix'] = self.xi
            data['xcalc'] = self.xcalc
            
            if not self.fast_is_piezo:
                self.xreal = self.musst_data['samy'][-1]/self.x_fac - self.delta_x/2
                self.xerr = self.xreal - self.xcalc
                data['xreal'] = self.xreal
                data['xerr'] = self.xerr

            if self.scan_dim == 2:
                data['ypix'] = self.yi
                data['ycalc'] = self.ycalc
                if not self.fast_is_piezo:
                    self.yreal = self.musst_data['samz'][-1]/self.y_fac
                    self.yerr = self.yreal - self.ycalc
                    data['yreal'] = self.yreal
                    data['yerr'] = self.yerr
            
            output = { label:data[label] for label in self.channels }

            if self.printout:
                values = list(output.values())
                line = self._tab.add_line(values)
                print(line)
            
            if self.xi != 0:
                return output
            
    def prepare(self):
        pass

    def start(self):
        pass

    def stop(self):
        pass


class FastMotorMaster(MotorMaster):
    def __init__(self, positions,
        axis,
        start,
        end,
        time=0,
        undershoot=None,
        undershoot_start_margin=0,
        undershoot_end_margin=0,
        trigger_type=AcquisitionMaster.SOFTWARE,
        backnforth=False,
        **keys,
        ):
            
        super().__init__(axis,
        start,
        end,
        time=time,
        undershoot=undershoot,
        undershoot_start_margin=undershoot_start_margin,
        undershoot_end_margin=undershoot_end_margin,
        trigger_type=trigger_type,
        backnforth=backnforth,
        **keys,)
        
        
        self.chname = f"{self.name}:{axis.name}"
        chan = AcquisitionChannel(self.chname, numpy.float64, ())
        self.channels.append(chan)
        
        self.positions = positions
    
    #def prepare(self):
    #    super().prepare()

    def start(self):
        super().start()
        
        #self.channels.update_from_iterable(self, iterable)
        #self.channels.update_from_array(data)
        self.channels.update({self.chname: self.positions})

        
class SlowLinearStepTriggerMaster(LinearStepTriggerMaster):
    def __init__(self, xdim, nb_point, *args, **keys):
        super().__init__(nb_point, *args, **keys)
        self.xdim = xdim
        
    def trigger(self):
        self.trigger_slaves()
        positions = [[axis.position]*self.xdim for axis in self._axes + self._monitor_axes]
        self.channels.update_from_iterable(positions)
        self.wait_slaves()    


def trig(musst_ctrl, gatewidth=0.01):
    musst_ctrl.BTRIG=1
    time.sleep(gatewidth)
    musst_ctrl.BTRIG=0


def check_fast_sync():
    # ---- Check synchro MUSST/motor
    # to be parametrized :(
    musst_ctrl = setup_globals.musst_sxm
    fast_mot = setup_globals.samy
    _ = fast_mot.__info__()  # F. lazy init...
    musst_ch3 = musst_ctrl.get_channel(3).value
    mot_enc = fast_mot.controller.read_encoder(fast_mot.encoder)
    if ( abs(musst_ch3 - mot_enc) > 5):
        _msg = "!!! WARNING !!! : musst counter CH3 no synchronized with samy encoder\n"
        _msg += f"musst_ch3={musst_ch3}  mot_enc={mot_enc}"
        raise RuntimeError(_msg)
    

def build_fast_chain(chain, builder, fast_master, scan_params):

    musst_ctrl = setup_globals.musst_sxm

    # ----- MUSST Master --------------------------------------------
    if 1:
        _start_pos = int(scan_params['x_start']    * scan_params['x_fac'])
        _time_step = int(scan_params['count_time'] * scan_params['t_fac'])
        _gatewidth = int(scan_params['gatewidth']  * scan_params['t_fac'])
        
        if scan_params['fast_is_piezo']:
            musst_program = "/musst_program/BTRIG-TIME_START-TRIG.mprg"
            store_list=['TIMER']
            musst_vars = {"TIME_STEP": _time_step,
                          "NPULSES":scan_params['x_npoints'],
                          "GATEWIDTH":_gatewidth,
                          }
        else:
            musst_program = "/musst_program/BTRIG-TIME_START-POS.mprg"
            store_list=['TIMER', 'samy', 'samz']
            musst_vars = {"STARTPOS": _start_pos,
                          "TIME_STEP": _time_step,
                          "NPULSES":scan_params['x_npoints'],
                          "GATEWIDTH":_gatewidth,
                          }
        
        musst_acq_master = MusstAcquisitionMaster(musst_ctrl,
                                                musst_program,
                                                program_start_name="TEST",
                                                vars=musst_vars)
                                        
        chain.add(fast_master, musst_acq_master)

    # ------ MUSST counters ----------------------------------------------------
    if 1:
        musst_acq_device = MusstAcquisitionSlave(musst_ctrl, store_list=store_list)
        chain.add(musst_acq_master, musst_acq_device)


    # ------ HANDLE MCA CONTROLLERS ----------------------------
    if 1:
        # The MCA should always take mca_params["npoints"] = number of intervals  (i.e x_npoints - 1)
        #
        # Internally if using trigger_mode==SYNC, the mca.hardware_points is set to  mca_params["npoints"] + 1
        # in order to compensate the fact that in this mode the mca starts the acquisition on scan start() 
        # and not when receiving the first hard trig when x_mot_pos == x_start.
        # The MCA acqObj handle this internally and it discards the first measure done @ x_mot_pos == x_start
        #
        #
        # (TO BE TESTED) If using trigger_mode==GATE, the acquisition is expected to start when receiving the first trigger 
        # (up or down edge of the gate).
        
        mca_params = {}
        mca_params["npoints"] = scan_params['x_npoints'] - 1  # x_npoints == number of triggers (= intervals +1)
        mca_params["trigger_mode"] = TriggerMode.SYNC
        # mca_params["keep_first_point"] = True        # handled by the acqObj for now (depends on trigger_mode)
        # mca_params["preset_time"] = 0.9               #  not used in SYNC trigger mode
        # mca_params["block_size"] = None
        # mca_params["polling_time"] = 0.1
        # mca_params["spectrum_size"] = None
        mca_params["prepare_once"] = False   # False to re-do 'prepare' at each line.
        mca_params["start_once"]   = False   # False to re-do 'start' at each line.

        for node in builder.get_nodes_by_controller_type(BaseMCA):
            # refresh_rate = node.controller.refresh_rate
            # if refresh_rate >= scan_params['count_time']:
            #     raise ValueError(f"count_time {scan_params['count_time']} cannot be shorter than refresh_rate ({refresh_rate})")
            node.set_parameters(acq_params=mca_params)
            chain.add(musst_acq_master, node)
    
    
    # ------ HANDLE EMH CONTROLLERS ------------------------
    if 1:
        emh_params = {}
        emh_params["trigger"] = "DIO_1"
        emh_params["int_time"] = scan_params['count_time']
        emh_params["npoints"] = scan_params['x_npoints']
        emh_params["trigger_type"] = "HARDWARE"

        for node in builder.get_nodes_by_controller_type(EMH):
            node.set_parameters( acq_params=emh_params)
            chain.add(musst_acq_master, node)
    
    # ----- P201  as MASTER
    if 1:
        p201_epsilon = 100  # shift trig time ( in micro seconds )

        p201_params = {}
        p201_params["npoints"] = scan_params['x_npoints'] - 1
        p201_params["acq_expo_time"] = scan_params['count_time'] - p201_epsilon/1e6         #  used in mode 5 only
        p201_params["acq_point_period"] = None
        # p201_params["acq_mode"] = CT2AcqMode.ExtTrigReadout  # 7 count until next trig
        p201_params["acq_mode"] = CT2AcqMode.ExtTrigMulti   # 5 count for a given time
        p201_params["prepare_once"] = False
        p201_params["start_once"] = False

        for node in builder.get_nodes_by_controller_type(CT2Controller):
            node.set_parameters( acq_params=p201_params)

            # Add p201 counters under p201 master
            for child_node in node.children:
                child_node.set_parameters(acq_params={"count_time": scan_params['count_time']})
                # no chain_add : children are automaticaly added under the parent.

            # Add p201 master under musst.
            chain.add(musst_acq_master, node)
    
    # ---- CALC CHANNELS -------------------------------------
    if 1:
        _musstcalchook = _musst_calculator(scan_params)

        musst_calc_chan = CalcChannelAcquisitionSlave("musst_calc_chan",
                                    [musst_acq_device],
                                    _musstcalchook,
                                    _musstcalchook.channels,
        )
        chain.add(musst_acq_master, musst_calc_chan)

    # ---- CACLC COUNTERS  !!!! TO BE DONE AFTER  P201  !!!
    if 0:
        for node in builder.get_nodes_by_controller_type(CalcCounterController):
            node.set_parameters( acq_params={"npoints": scan_params['x_npoints'], "count_time": scan_params['count_time']})
            chain.add(musst_acq_master, node)


def build_slow_chain(chain, builder, slow_master, scan_params):

    # ---- OTHER DEVICES LIKE SAMPLING COUNTERS  (simulation diode) ------------
    if 1:
        for node in builder.get_nodes_by_controller_type(SamplingCounterController):
            node.set_parameters( acq_params={"npoints": scan_params['y_npoints'], "count_time": scan_params['count_time']})
            chain.add(slow_master, node)
    

def lscan(fast_motor, x_start, x_stop, x_intervals, count_time,
         *counters, save=False, save_images=False, run=True, gatewidth=0.001, printout=False):

    """
    """

    #--- init scan parameters ---
    if 1:
        x_npoints = x_intervals + 1
        x_travel_time =  count_time * x_intervals
        x_travel_distance = x_stop-x_start
        delta_x = x_travel_distance/x_intervals
        
        #--- get musst and opiom ---
        musst_ctrl = setup_globals.musst_sxm
        opiom_multiplexer = setup_globals.mpx
        opiom_multiplexer.switch('SEL_TRIG_OUT', 'ZAP')

        t_fac = musst_ctrl.get_timer_factor()
        x_fac = fast_motor.steps_per_unit

        if fast_motor.name in ['sampy','sampz']:
            fast_is_piezo = True
            undershoot = 0
        else:
            fast_is_piezo = False
            undershoot = None

        scan_params = {'count_time':count_time, 't_fac':t_fac, 
            'x_start':x_start, 'x_stop':x_stop, 'x_npoints':x_npoints, 
            'delta_x':delta_x, 'x_fac':x_fac,
            'fast_is_piezo':fast_is_piezo, 'gatewidth':gatewidth, 'scan_dim':1,
            'printout':printout,
            }

    print("scan_params", scan_params)
     
    #--- check the scan parameters are valid
    if fast_motor.name == 'samz':
        raise ValueError(f"samz is not available as a fast axis yet (cf musst prog)")

    if count_time <= gatewidth:
        raise ValueError(f"count_time {count_time} cannot be shorter than gatewidth ({gatewidth})")
        
    if fast_is_piezo:
        opiom_multiplexer.switch('MUSST_TRIG', 'VSCANNER1')
    else:
        check_fast_sync()
        if x_travel_distance/x_travel_time > 0.4:
            raise ValueError(f"{fast_motor.name} speed must be <= 0.4 mm/s")

    # Prepare the positions that will be emitted by the FastMotorMaster 
    # (for Flint to see the correct dimension from the first top-master found in the scan_info)
    # The first point is removed (start=1) in order to align with the integrated counters (i.e value per pixel/interval) 
    positions = [ x_start + i*delta_x -delta_x/2 for i in range(1, x_npoints) ] 
    
    #--- build the acquisition chain ---
    chain = AcquisitionChain()
    #fast_master = MotorMaster(fast_motor, x_start, x_stop, time=x_travel_time, undershoot=undershoot)
    fast_master = FastMotorMaster(positions, fast_motor, x_start, x_stop, time=x_travel_time, undershoot=undershoot)
    chain.add(fast_master)
    
    # ------ BUILDER for counters ----------------------------------------------
    builder = ChainBuilder(counters)
    build_fast_chain(chain, builder, fast_master, scan_params)
    
    # ---- Check for not initilized node
    if builder.get_nodes_not_ready():
        builder.print_tree(not_ready_only=False)
        raise RuntimeError("There are not ready nodes -> cannot use these counters")

    scan_info = {
        "npoints": x_npoints,   
        "type":"lscan",
        "count_time": count_time,
    }

    command_line = f"lscan {fast_motor.name} {x_start} {x_stop} {x_intervals} {count_time}"

    sc = Scan(
        chain,
        name=command_line,
        scan_info=scan_info,
        save=save,
        save_images=save_images,
        scan_saving=None,
        data_watch_callback=StepScanDataWatch(),
    )

    if run:
        with disable_user_output():
            sc.run()
    
    return sc
    

def l2scan(fast_motor, x_start, x_stop, x_intervals, slow_motor, y_start, y_stop, y_intervals, count_time,
         *counters, save=False, save_images=False, run=True, gatewidth=0.001, printout=False):
    """
    """

    #--- init scan parameters ---
    if 1:
        x_npoints = x_intervals + 1
        y_npoints = y_intervals + 1

        x_travel_time =  count_time * x_intervals
        x_travel_distance = x_stop-x_start
        delta_x = x_travel_distance/x_intervals
        
        y_travel_time =  x_travel_time * y_npoints
        y_travel_distance = y_stop-y_start
        delta_y = y_travel_distance/y_intervals

        #--- get musst and opiom ---
        musst_ctrl = setup_globals.musst_sxm
        opiom_multiplexer = setup_globals.mpx
        opiom_multiplexer.switch('SEL_TRIG_OUT', 'ZAP')

        t_fac = musst_ctrl.get_timer_factor()
        x_fac = fast_motor.steps_per_unit
        y_fac = slow_motor.steps_per_unit

        if fast_motor.name in ['sampy','sampz']:
            fast_is_piezo = True
            undershoot = 0
        else:
            fast_is_piezo = False
            undershoot = None

        scan_params = {'count_time':count_time, 't_fac':t_fac, 
            'x_start':x_start, 'x_stop':x_stop, 'x_npoints':x_npoints, 
            'delta_x':delta_x, 'x_fac':x_fac,
            'y_start':y_start, 'y_stop':y_stop, 'y_npoints':y_npoints, 
            'delta_y':delta_y, 'y_fac':y_fac, 
            'fast_is_piezo':fast_is_piezo, 'gatewidth':gatewidth, 'scan_dim':2,
            'printout':printout,
            }

        
    #--- check the scan parameters are valid
    if fast_motor.name == 'samz':
        raise ValueError(f"samz is not available as a fast axis yet (cf musst prog)")

    if count_time <= gatewidth:
        raise ValueError(f"count_time {count_time} cannot be shorter than gatewidth ({gatewidth})")
        
    if fast_is_piezo:
        opiom_multiplexer.switch('MUSST_TRIG', 'VSCANNER1')
    else:
        check_fast_sync()
        if x_travel_distance/x_travel_time > 0.4:
            raise ValueError(f"{fast_motor.name} speed must be <= 0.4 mm/s")

    
    positions = [ x_start + i*delta_x -delta_x/2 for i in range(1, x_npoints) ]
    
    #--- build the acquisition chain ---
    chain = AcquisitionChain()
    #fast_master = MotorMaster(fast_motor, x_start, x_stop, time=x_travel_time, undershoot=undershoot)
    fast_master = FastMotorMaster(positions, fast_motor, x_start, x_stop, time=x_travel_time, undershoot=undershoot)
    #slow_master = LinearStepTriggerMaster(y_npoints, slow_motor, y_start, y_stop)
    slow_master = SlowLinearStepTriggerMaster(x_npoints-1, y_npoints, slow_motor, y_start, y_stop)
    
    chain.add(slow_master, fast_master)
    
    # ------ BUILDER for counters ----------------------------------------------
    builder = ChainBuilder(counters)
    build_fast_chain(chain, builder, fast_master, scan_params)
    build_slow_chain(chain, builder, slow_master, scan_params)
    
    
    
    
    print(chain._tree)
    # ---- Check for not initilized node
    if builder.get_nodes_not_ready():
        builder.print_tree(not_ready_only=False)
        raise RuntimeError("There are not ready nodes -> cannot use these counters")

    
    total_points = (x_npoints-1)*y_npoints
    requests = {}
    requests["xcalc"] = {"axis-kind": "fast", "axis-points": x_npoints-1, "points": total_points}
    requests["ycalc"] = {"axis-kind": "slow", "axis-points": y_npoints, "points": total_points}
    
    scan_info = {
        "npoints": total_points,
        "npoints1": x_npoints-1,
        "npoints2": y_npoints,   
        "type":None,
        "count_time": count_time,
        "requests": requests,
        "data_dim":2,
        "start": [y_start, x_start-delta_x/2],
        "stop": [y_stop, x_stop-delta_x/2],
    }
    
    #scan_info = {
    #    "npoints": total_points,       # total number of lines  (workaround for Flint )
    #    "type":"l2scan",
    #    "npoints1": x_npoints-1,
    #    "npoints2": y_npoints,
    #    "count_time": count_time,
    #    "dim": 2,                   # <--------dimension of the scan ... pb : I've only 1 master :(
    #    "data_dim": 2,              # <--------dimension of the scan ... pb : I've only 1 master :(
    #    #"start": [y_start, x_start],
    #    #"stop": [y_stop, x_stop],
    #}
    
    command_line = f"l2scan {fast_motor.name} {x_start} {x_stop} {x_intervals} "
    command_line += f"{slow_motor.name} {y_start} {y_stop} {y_intervals} {count_time}"

    sc = Scan(
        chain,
        name=command_line,
        scan_info=scan_info,
        save=save,
        save_images=save_images,
        scan_saving=None,
        data_watch_callback=StepScanDataWatch(),
    )

    if run:
        with disable_user_output():
            sc.run()
        data_post_proc = DataPostProcessing(sc, (-1, y_npoints) ) #x_npoints
    
    return data_post_proc




class DataPostProcessing:

    def __init__(self, scan, size):
        
        self.scan = scan
        self.size = size
        self.plt = LiveImagePlot()

    def get_data(self, cname):
        return self.scan.get_data(cname)

    def as_image(self, cname):
        w,h = self.size
        arry = self.scan.get_data(cname)
        img = arry.reshape((h,w))
        return img

    def plot(self, cname, view=None):
        img = self.as_image(cname)
        if view:
            img = eval(f"img{view}")
        self.plt.plot(img)

