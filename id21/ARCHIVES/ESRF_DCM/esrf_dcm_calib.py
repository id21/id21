import numpy
from datetime import datetime
import xcalibu
import glob
import time
import gevent
import os
import warnings
import tabulate
import matplotlib.pyplot as plt
from scipy.signal import kaiserord, lfilter, firwin, detrend
from scipy.io import savemat

from bliss.shell.getval import getval_yes_no
from bliss.common.logtools import log_debug, log_info
from bliss.common import plot
from bliss.common.cleanup import cleanup
from bliss.shell.standard import umv, umvr, dscan, ascan
from bliss.config import settings
from bliss.config.static import get_config
from bliss.scanning import scan_math
from bliss.common.utils import ColorTags, BOLD, GREEN, YELLOW, BLUE, RED,ORANGE

from id21.ESRF_DCM.esrf_dcm_utils import fjs_sync

class EsrfDCMcalib:

    def __init__(self, mono):
        self._mono = mono
        self._goat_ctl = self._mono._goat._hwc
        
        self._motors = {
            "fjsz": get_config().get("fjsz"),
            "fjsrx": get_config().get("fjsrx"),
            "fjsry": get_config().get("fjsry"),
            "mfjsur":  get_config().get("mfjsur"),
            "mfjsuh":  get_config().get("mfjsuh"),
            "mfjsd":  get_config().get("mfjsd"),
            "braggfe": get_config().get("braggfe"),
        }

        # Fast Jack Calibration
        self.fjs = EsrfDCMfjsCalib(mono, self._motors)

        # Interferometer calibrations
        self.interf = EsrfDCMinterfCalib(mono, self._motors)
        
        # List of calibrations (string, different calibrations are separated by spaces)
        self._calib_id = settings.SimpleSetting(f"EsrfDCM_calib_id_{self._mono.name}", default_value="")


    def __info__(self):
        print(self._get_info())
        return ""


    def _get_info(self):
        mystr = ""
        
        on_str = GREEN("ON")
        off_str = RED("OFF")
        
        mystr += "    Fjs Calibratio  : "
        if self.fjs.lut_use:
            mystr += f"{on_str}\n"
        else:
            mystr += f"{off_str}\n"
            
        mystr += "    Fjs Offset      : "
        if self.fjs.offset_use:
            mystr += f"{on_str}\n"
        else:
            mystr += f"{off_str}\n"
            
        rx_state, ry_state, dz_state = self.interf._state()
        mystr += "    Interf Calib.   : "
        if rx_state:
            mystr += f"Rx {on_str} - "
        else:
            mystr += f"Rx {off_str} - "
        if ry_state:
            mystr += f"Ry {on_str} - "
        else:
            mystr += f"Ry {off_str} - "
        if dz_state:
            mystr += f"Dz {on_str}\n"
        else:
            mystr += f"Dz {off_str}\n"
            
        return mystr


    def restart_all(self, do_print=True):
        """ Function used to reset the DCM:
            - Homing the FastJacks
            - Synchronize FJS steps between IcePAP and Speedgoat
            - Reset interferometer position """
        
        if do_print:
            print("Remove Regululation")
        self._mono.regul.off()

        if do_print:
            print("Deactivate Interferometer Calibration")
        self.interf.off()

        if do_print:
            print("Homing of Fast Jacks")
        self.fjs._fjs_home()

        if do_print:
            print("Synchronize FastJack Position with Speedgoat")
        self.fjs._reset_offset_speedgoat()

        if do_print:
            print("Go at 10 degrees for reference position")
        umv(self._mono._motors["bragg"], 10)
        self.fjs.offset_use = True

        if do_print:
            print("Reset Interferometers and Photodiodes")
        gevent.sleep(2.0)
        self.interf._realign_interf()

    def calibration_build(self, calib_id, from_ene, to_ene, time_per_point, tracker, mode, param_id, rc_number=20, rc_nbp=60, bpm_number=100, poly_deg=3, bragg_delta=0.25, show_calib=False):
        """ Function used to build a new calibration.
            This is done between 'from_ene' and 'to_ene'.
            The calibrations will be saved with the name 'calib_id'.
            'tracker' corresponds to the ID to be used.
            'mode' is the working mode of the 'tracker'.
            'param_id' """
        # Make sure to_ene > from_ene
        if (from_ene > to_ene):
            from_eneb = from_ene
            from_ene = to_ene
            to_ene = from_eneb

        # Reset Fast jacks and interferoneters
        self.restart_all(do_print=False)

        # Bragg range
        from_bragg = self._mono.energy2bragg(from_ene) - self._mono._bragg_offset.get()
        to_bragg = self._mono.energy2bragg(to_ene) - self._mono._bragg_offset.get()
        
        if numpy.abs(from_bragg - to_bragg) < self._mono._max_coil_range:
            # Small range: only one interval
            intervals = [from_ene, to_ene]
        else:
            # Large range: need several intervals
            interval_nb = int(1 + numpy.abs(from_bragg - to_bragg) // self._mono._max_coil_range)
            intervals = self._mono.bragg2energy(numpy.linspace(to_bragg, from_bragg, interval_nb + 1) + self._mono._bragg_offset.get())
        
        # Build Fjs LUT
        self.fjs.build(calib_id, interval=intervals, remove=True)

        # Manage undulator
        self._mono.tracking.all_off()
        tracker.tracking.on()
        tracker.tracking.mode.set(mode)
        tracker.tracking.param_id._set_param_id(param_id)

        # TODO - Should put photodiode IN
        dioz = get_config().get("dioz")
        umv(dioz, -33)
        
        # Calibrate Ry using rocking curves
        self.interf.calib_ry_rc(calib_id, from_ene, to_ene, rc_number, rc_nbp=rc_nbp, show_calib=show_calib, poly_deg=poly_deg, bragg_delta=bragg_delta)

        # TODO - Should put photodiode OUT
        umv(dioz, -25)

        # Build FastJack Calibration
        self.fjs.build(calib_id, interval=intervals, remove=True)

        # Calibrate Rx and/or Dz using BPM
        self.interf.calib_bpm(calib_id, from_ene, to_ene, bpm_number, time_per_point, focused=False, show_calib=show_calib, poly_deg=poly_deg, bragg_delta=bragg_delta)

        # Build Fast Jack Calibration
        self.fjs.build(calib_id, remove=True)

        # Move at to_ene with LUT and then Regul ON
        umv(self._mono.motors["energy_tracker"], to_ene)
        self._mono.regul.on()
        
        # Save calibration ID
        self._calibration_add(calib_id)

    def calibration_build_focused(self, calib_id, from_ene, to_ene, time_per_point, tracker, mode, param_id, bpm_number=100, poly_deg=3, show_calib=False):
        """
        - use =add_mode=
        - Add =_focused= at the end
        - Copy rz with _focused at the end
        - Make FJS calibration with =_focused= radix at the end
        - Add that to the list of calibrations """

        # Make sure to_ene > from_ene
        if (from_ene > to_ene):
            from_eneb = from_ene
            from_ene = to_ene
            to_ene = from_eneb

        # Manage undulator
        self._mono.tracking.all_off()
        tracker.tracking.on()
        tracker.tracking.mode.set(mode)
        tracker.tracking.param_id._set_param_id(param_id)

        # Calibrate Rx and Ry using BPM
        self.interf.calib_bpm(f"{calib_id}_focused", from_ene, to_ene, bpm_number, time_per_point, focused=True, rx_add_mode=True, ry_add_mode=True, show_calib=show_calib, poly_deg=poly_deg)

        # Copy Dz calibration
        xtal = self._mono._xtals.xtal_sel
        os.popen(f"cp {self.interf._data_dir}/{calib_id}_{xtal}_dz.dat {self.interf._data_dir}/{calib_id}_focused_{xtal}_dz.dat") 
        os.popen(f"cp {self.interf._data_dir}/{calib_id}_{xtal}_dz_raw.dat {self.interf._data_dir}/{calib_id}_focused_{xtal}_dz_raw.dat") 

        # Build Fast Jack Calibration over full stroke
        self.fjs.build(f"{calib_id}_focused", remove=True)

        # Move at to_ene with LUT and then Regul ON
        umv(self._mono.motors["energy_tracker"], to_ene)
        self._mono.regul.on()
        
        # Save calibration ID
        self._calibration_add(f"{calib_id}_focused")
    
    def _calibration_add(self, calib_id):
        """ Add calibration to saved ones. """
        calib_id_list = self._calib_id.get().split()
        if calib_id not in calib_id_list:
            calib_id_list.append(calib_id)
            self._calib_id.set(" ".join(calib_id_list))
    
    def _calibration_rem(self, calib_id):
        """ Remove calibration from saved ones. """
        calib_id_list = self._calib_id.get().split()
        if calib_id in calib_id_list:
            calib_id_list.remove(calib_id)
            self._calib_id.set(" ".join(calib_id_list))

    def list_calibrations(self):
        """ List all saved calibrations. """
        for cal in self._calib_id.get().split():
            print(f"    - {cal}")

    def display_calibration(self, calib_id, x_axis_energy=True):
        """ Display one calibration (FJS, interferometers) """
        # =========================
        # Display FJS Calibration
        # =========================

        # Load FJS Calibrations
        filename = f"{self.fjs._data_dir}/{calib_id}_{self._mono._xtals.xtal_sel}"
        lut_table_ur = numpy.loadtxt(f"{filename}_ur.dat", dtype=float, delimiter=" ").transpose()
        lut_table_uh = numpy.loadtxt(f"{filename}_uh.dat", dtype=float, delimiter=" ").transpose()
        lut_table_d  = numpy.loadtxt(f"{filename}_d.dat",  dtype=float, delimiter=" ").transpose()

        fjs_correction_ur = lut_table_ur[1]-lut_table_ur[0]
        fjs_correction_uh = lut_table_uh[1]-lut_table_uh[0]
        fjs_correction_d  = lut_table_d[1] -lut_table_d[0]

        # Only keep relevant data
        ur_filter = (fjs_correction_ur != fjs_correction_ur[-1]) & (fjs_correction_ur != fjs_correction_ur[0])
        uh_filter = (fjs_correction_uh != fjs_correction_uh[-1]) & (fjs_correction_uh != fjs_correction_uh[0])
        d_filter  = (fjs_correction_d  != fjs_correction_d[-1])  & (fjs_correction_d  != fjs_correction_d[0])

        fjs_correction_ur = fjs_correction_ur[ur_filter]
        fjs_correction_uh = fjs_correction_uh[uh_filter]
        fjs_correction_d = fjs_correction_d[d_filter]

        ax = plt.subplots()[1]
        ax.plot(lut_table_ur[0][ur_filter], 1e3*fjs_correction_ur, label='ur')
        ax.plot(lut_table_uh[0][uh_filter], 1e3*fjs_correction_uh, label='uh')
        ax.plot(lut_table_d[0][d_filter],   1e3*fjs_correction_d,  label='d')
        ax.set_xlabel('Position [mm]')
        ax.set_ylabel('Correction [um]')
        ax.grid(True)
        ax.legend(loc='upper left')
        plt.show()
        
        # =========================
        # Display Interf Calibration
        # =========================

        # Ry Calibrations
        filename = f"{self.interf._data_dir}/{calib_id}_{self._mono._xtals.xtal_sel}"
        interf_ry = numpy.loadtxt(f"{filename}_ry.dat", dtype=float, delimiter=" ").transpose()
        interf_ry_raw = numpy.loadtxt(f"{filename}_ry_raw.dat", dtype=float, delimiter=" ").transpose()
        ax = plt.subplots()[1]
        if x_axis_energy:
            ax.plot(self._mono.bragg2energy(interf_ry_raw[0] + self._mono._bragg_offset.get()), interf_ry_raw[1], '-')
            ax.plot(self._mono.bragg2energy(interf_ry[0] + self._mono._bragg_offset.get()), interf_ry[1], 'o-')
            ax.set_xlabel('Energy [keV]')
        else:
            ax.plot(interf_ry_raw[0], interf_ry_raw[1], '-')
            ax.plot(interf_ry[0], interf_ry[1], 'o-')
            ax.set_xlabel('Bragg Angle [deg]')
        ax.set_ylabel('Ry Correction [nrad]')
        ax.grid(True)
        plt.show()

        # Rx Calibrations
        filename = f"{self.interf._data_dir}/{calib_id}_{self._mono._xtals.xtal_sel}"
        interf_rx = numpy.loadtxt(f"{filename}_rx.dat", dtype=float, delimiter=" ").transpose()
        interf_rx_raw = numpy.loadtxt(f"{filename}_rx_raw.dat", dtype=float, delimiter=" ").transpose()
        ax = plt.subplots()[1]
        if x_axis_energy:
            ax.plot(self._mono.bragg2energy(interf_rx_raw[0] + self._mono._bragg_offset.get()), interf_rx_raw[1], '-')
            ax.plot(self._mono.bragg2energy(interf_rx[0] + self._mono._bragg_offset.get()), interf_rx[1], 'o-')
            ax.set_xlabel('Energy [keV]')
        else:
            ax.plot(interf_rx_raw[0], interf_rx_raw[1], '-')
            ax.plot(interf_rx[0], interf_rx[1], 'o-')
            ax.set_xlabel('Bragg Angle [deg]')
        ax.set_ylabel('Rx Correction [nrad]')
        ax.grid(True)
        plt.show()

        # Dz Calibrations
        filename = f"{self.interf._data_dir}/{calib_id}_{self._mono._xtals.xtal_sel}"
        interf_dz = numpy.loadtxt(f"{filename}_dz.dat", dtype=float, delimiter=" ").transpose()
        interf_dz_raw = numpy.loadtxt(f"{filename}_dz_raw.dat", dtype=float, delimiter=" ").transpose()
        ax = plt.subplots()[1]
        if x_axis_energy:
            ax.plot(self._mono.bragg2energy(interf_dz_raw[0] + self._mono._bragg_offset.get()), interf_dz_raw[1], '-')
            ax.plot(self._mono.bragg2energy(interf_dz[0] + self._mono._bragg_offset.get()), interf_dz[1], 'o-')
            ax.set_xlabel('Energy [keV]')
        else:
            ax.plot(interf_dz_raw[0], interf_dz_raw[1], '-')
            ax.plot(interf_dz[0], interf_dz[1], 'o-')
            ax.set_xlabel('Bragg Angle [deg]')
        ax.set_ylabel('Dz Correction [nm]')
        ax.grid(True)
        plt.show()

    def calibration_load(self, calib_id):
        """ Load FJS Calib and Interf CALIB corresponding to ID and regul=ON """
        if not calib_id in self._calib_id.get().split():
            print(RED("WARNING: Unkown Calibration"))
            if not getval_yes_no("Still try load the calibrations?", default="yes"):
                return

        self.fjs.load(calib_id, do_print=True)
        self.interf.load(radix=calib_id, do_print=True)
        self._mono.regul.on()


"""
FJS Calibration (LUT)
"""
class EsrfDCMfjsCalib:
    def __init__(self, mono, motors):
        
        self._mono = mono
        self._goat = self._mono._goat
        self._goat_ctl = self._mono._goat._hwc
        
        self._motors = motors
        self._motors["fjstraj"] = get_config().get("fjstraj")
        self._motors["mcoil"] = get_config().get("mcoil")
        
        self._fastdaq = self._goat_ctl.fastdaq.fastdaq
        
        calib_param = self._mono.config.get("calibration", None)
        if calib_param is None:
            raise RuntimeError("esrf_dcm/FjsCalib: No calibration parameters")
            
        fjs_data_dir = calib_param.get("fjs_data_dir", None)
        if fjs_data_dir is None:
            raise RuntimeError("esrf_dcm/FjsCalib: No data directory given for FJS calibration")
        self._data_dir = fjs_data_dir
        
        # Load Bragg intervals for FJS calibration. Verify that it is compatible with the disabling of mcoil
        bragg_interval = calib_param.get("fjs_interval", None)
        if bragg_interval is None:
            raise RuntimeError("esrf_dcm/FjsCalib: No Bragg intervals given for FJS calibration")
        self._bragg_interval = list(bragg_interval)
        self._bragg_interval.sort(reverse=True)
        for i in range(len(self._bragg_interval) - 1):
            if numpy.abs(self._bragg_interval[i+1] - self._bragg_interval[i]) > self._mono._max_coil_range:
                raise RuntimeError("esrf_dcm/FjsCalib: Bragg intervals greater that Max. Coil Range")
        
        self._radix_setting = settings.SimpleSetting(f"EsrfDCM_fjscalib_{self._mono.name}_radix", default_value="fjs_default")
                
        self._fjs_margin = 0.005 # Small Margin in [mm] when making the LUT
        self._fjs_default_speed = 0.125 # [mm/s]
        
        fjs_min, fjs_max = self._motors["fjsz"].limits
        self._fjsz_min = fjs_min
        self._fjsz_max = fjs_max

        # fjsrx and fjsry offset
        self._offset_use = settings.SimpleSetting(f"EsrfDCM_fjscalib_{self._mono.name}_use_offset", default_value=False)
        self._offset_rx = settings.SimpleSetting(f"EsrfDCM_fjscalib_{self._mono.name}_rx_offset", default_value=0.0)
        self._offset_ry = settings.SimpleSetting(f"EsrfDCM_fjscalib_{self._mono.name}_ry_offset", default_value=0.0)

        # FJS Calibration
        self.calib_name = ("fjsur", "fjsuh", "fjsd")
        self.real_tag = {
            "fjsur": "lega",
            "fjsuh": "legb",
            "fjsd": "legc",
        }
        self.pseudo_tag = {
            "fjsur": "fjsz",
            "fjsuh": "fjsrx",
            "fjsd": "fjsry",
        }

        # Create Xcalibu object
        self._fjs_calib = {}
        for name in self.calib_name:
            self._fjs_calib[name] = xcalibu.Xcalibu()
            self._fjs_calib[name].set_calib_name(name)
            self._fjs_calib[name].set_calib_time(0)
            self._fjs_calib[name].set_calib_type("TABLE")
            self._fjs_calib[name].set_reconstruction_method("INTERPOLATION")

        self._lut_use = settings.SimpleSetting(f"EsrfDCM_fjscalib_{self._mono.name}_lut_use", default_value=False)
        try:
            # Load last Saved LUT by default
            self.load(do_print=True)
        except:
            # No LUT available
            self._lut_table_ur = None
            self._lut_table_uh = None
            self._lut_table_d = None
        
        if self.lut_use:
            if self._lut_table_ur is None:
                # If no loaded tables, lut_use = False
                self.lut_use = False
            else:
                # Load Xcalibu tables
                self._lut_to_calib()


    def __info__(self):
        lines = [["FJS Calibration", ""]]
        lines.append(["Use Offset: ", self.offset_use])
        lines.append(["Offset Rx: ", self._offset_rx.get()])
        lines.append(["Offset Ry: ", self._offset_ry.get()])
        lines.append(["Use LUT: ", self.lut_use])
        mystr = "\n"+tabulate.tabulate(lines, tablefmt="plain", stralign="right")
        return mystr


    @property
    def _radix(self):
        return self._radix_setting.get()
        
    @_radix.setter
    def _radix(self, value):
        self._radix_setting.set(value)

    def _update_fjs_steps_counter(self):
        old_table = numpy.loadtxt(f"{self._data_dir}/FJ_STEPS/fjs_steps_current.dat", dtype=float, delimiter=" ")
        fjs_table = self._goat_ctl.signal.get("fjs_steps_counter/fjs_table")

        new_table = old_table + fjs_table;
        numpy.savetxt(f"{self._data_dir}/FJ_STEPS/fjs_steps_current.dat", new_table)
        now = datetime.now().strftime('%Y-%m-%d')
        numpy.savetxt(f"{self._data_dir}/FJ_STEPS/fjs_steps_{now}.dat", new_table)

        self._reset_fjs_steps_counter()

    def _reset_fjs_steps_counter(self):
        """ Reset the FJS table that monitors the FJS steps. """
        reset_bias = int(self._goat_ctl.parameter.get("fjs_steps_counter/reset_fjs_table/Bias"))
        self._goat_ctl.parameter.set("fjs_steps_counter/reset_fjs_table/Bias", reset_bias+1)

    def _display_fjs_steps_counter(self, date=None):
        """ Date should be in '%Y-%m-%d' format. """
        if date is None:
            old_table = numpy.loadtxt(f"{self._data_dir}/FJ_STEPS/fjs_steps_current.dat", dtype=float, delimiter=" ")
            fjs_table = self._goat_ctl.signal.get("fjs_steps_counter/fjs_table")

            new_table = old_table + fjs_table;
        else:
            new_table = numpy.loadtxt(f"{self._data_dir}/FJ_STEPS/fjs_steps_{date}.dat", dtype=float, delimiter=" ")

        fjs_inc = self._goat_ctl.parameter.get("inc_table_inc_")
        fjs_positions = numpy.linspace(0, 1e-6*fjs_inc*(len(new_table)-1), len(new_table))
        
        fig = plt.figure(dpi=150)
        ax = fig.add_subplot(1, 1, 1)
        ax.plot(fjs_positions, new_table, linestyle='', marker='o')
        ax.set_xlabel('FJS Position [mm]')
        ax.set_ylabel('Count')
        plt.show()

    # FJS rx/ry offset management
    @property
    def offset(self):
        return (self._offset_rx.get(), self._offset_ry.get())
        
    @offset.setter
    def offset(self, offset):
        self._offset_rx.set(offset[0])
        self._offset_ry.set(offset[1])
    
    @property
    def offset_use(self):
        return self._offset_use.get()
        
    @offset_use.setter
    def offset_use(self, value):
        if value:
            offset_rx, offset_ry = self.offset
            self._motors["fjsrx"].offset = 0
            self._motors["fjsry"].offset = 0
            umv(self._motors["fjsrx"], offset_rx, self._motors["fjsry"], offset_ry)
            self._motors["fjsrx"].offset = - offset_rx
            self._motors["fjsry"].offset = - offset_ry
        else:
            self._motors["fjsrx"].offset = 0.0
            self._motors["fjsry"].offset = 0.0
            
        self._offset_use.set(value)
    

    # FJS LUT management
    @property
    def lut_use(self):
        return self._lut_use.get()
        
    @lut_use.setter
    def lut_use(self, value):
        self._lut_use.set(value)
        if value:
            self._lut_to_calib()
    
    def plot(self, name, display=False, save=True):
        """ Used to Plot the FJS calibration. Name should be ur, uh or d """
        self._lut_to_calib()
        filename = f"{self._data_dir}/{self._radix}_{self._mono._xtals.xtal_sel}_{name}"
        self._fjs_calib[name].plot("var", display=display, save=save, file_name=filename)

    def _fjs_home(self):
        """ Homing of all three fast jacks """
        self.offset_use = False
        self.lut_use = False
        umv(self._motors["fjsrx"], 0, self._motors["fjsry"], 0)
        umv(self._motors["fjsz"], 0)
        gevent.sleep(1)
        
        umvr(self._motors["fjsz"], 0.1)
        gevent.sleep(1)
        
        self._motors["mfjsur"].home()
        gevent.sleep(0.2)
        self._motors["mfjsuh"].home()
        gevent.sleep(0.2)
        self._motors["mfjsd"].home()
        gevent.sleep(0.2)

        fjs_sync()
        
    def load(self, radix=None, do_print=False):
        """ Load the FJS calibrations. Will be used for further scans """
        if radix is not None:
            self._radix = radix

        filename = f"{self._data_dir}/{self._radix}_{self._mono._xtals.xtal_sel}"
        
        if do_print:
            print(f"Load LUT Table from :")
            print(f"    - {filename}_ur.dat")
            print(f"    - {filename}_uh.dat")
            print(f"    - {filename}_d.dat")
        
        self._lut_table_ur = numpy.loadtxt(f"{filename}_ur.dat", dtype=float, delimiter=" ").transpose()
        self._lut_table_uh = numpy.loadtxt(f"{filename}_uh.dat", dtype=float, delimiter=" ").transpose()
        self._lut_table_d = numpy.loadtxt(f"{filename}_d.dat", dtype=float, delimiter=" ").transpose()

        self._lut_to_calib()
        self.lut_use = True

    def _check_file(self, radix, remove=False):
        path_radix = f"{self._data_dir}/{radix}_{self._mono._xtals.xtal_sel}_*.dat"
        file_list = glob.glob(path_radix)
        if len(file_list) != 0:
            if remove:
                for f in file_list:
                    os.remove(f)
            else:
                msg_str = f"Radix {radix} already exists in directory {self._data_dir}"
                raise RuntimeError(msg_str)

    def list(self):
        file_list = glob.glob(f"{self._data_dir}/*")
        print("\n")
        for fullname in file_list:
            filename = fullname.split("/")[-1]
            print(f"    {filename}")
        print("\n")

    def _save(self):
        data_ur = numpy.transpose(self._lut_table_ur)
        data_uh = numpy.transpose(self._lut_table_uh)
        data_d = numpy.transpose(self._lut_table_d)
        filename = f"{self._data_dir}/{self._radix}_{self._mono._xtals.xtal_sel}"
        fn_ur = f"{filename}_ur.dat"
        fn_uh = f"{filename}_uh.dat"
        fn_d = f"{filename}_d.dat"
        print(f"Save LUT Table in :")
        print(f"    - {filename}_ur.dat")
        print(f"    - {filename}_uh.dat")
        print(f"    - {filename}_d.dat")
        numpy.savetxt(fn_ur, data_ur)
        numpy.savetxt(fn_uh, data_uh)
        numpy.savetxt(fn_d, data_d)

    def _reset_offset_speedgoat(self):
        """ Used to match the FJS steps between the Speedgoat and the IcePAP """
        mfjsur = self._motors["mfjsur"]
        mfjsuh = self._motors["mfjsuh"]
        mfjsd  = self._motors["mfjsd"]
        fjs_sync()

        self._goat_ctl.parameter.fjsur_reset_steps = int(mfjsur.position * mfjsur.steps_per_unit + 0.5)
        self._goat_ctl.parameter.fjsuh_reset_steps = int(mfjsuh.position * mfjsuh.steps_per_unit + 0.5)
        self._goat_ctl.parameter.fjsd_reset_steps = int(mfjsd.position * mfjsd.steps_per_unit + 0.5)
        
        self._goat_ctl.trigger.reset_fj.trig()

    def build(self, radix, interval=None, remove=False, fjs_speed=None):
        """ Make Several Scans to build a LUT over the Full Stroke.
        Scans are performed from high bragg angles to low bragg angles.

        Args:
            lut_radix: name of the LUT files
            remove: Should remove LUT with same radix?
        """
        
        # First check if LUT can be saved.
        self._check_file(radix, remove=remove)
        self._radix = radix
        
        if fjs_speed is None:
            used_fjs_speed = self._fjs_default_speed
        else:
            used_fjs_speed = fjs_speed
            
        # Initialize LUT Data
        if interval is None:
            bragg_interval = self._bragg_interval
        else:
            bragg_interval = []
            for val in interval:
                bragg_interval.append(self._mono.energy2bragg(val) - self._mono._bragg_offset.get())
            bragg_interval.sort(reverse=True)
            for i in range(len(bragg_interval) - 1):
                if numpy.abs(bragg_interval[i+1] - bragg_interval[i]) > self._mono._max_coil_range:
                    raise RuntimeError("esrf_dcm/FjsCalib: Bragg intervals greater that Max. Coil Range")

        lut_nb_interval = len(bragg_interval)-1
            
        lut_ur_full = numpy.zeros((2,1))
        lut_uh_full = numpy.zeros((2,1))
        lut_d_full  = numpy.zeros((2,1))

        # Scan Bragg angle
        for idx in range(lut_nb_interval):
            fjs1 = self._mono.bragg2xtal(bragg_interval[idx+1])
            fjs2 = self._mono.bragg2xtal(bragg_interval[idx])
            data = self._acq_zone_fjs(fjs1, fjs2, speed=used_fjs_speed)

            # Save raw data
            savemat(f"{self._data_dir}/{self._radix}_{self._mono._xtals.xtal_sel}_raw_{idx+1}.mat", data)

            # Calculate/Save LUT:
            lut_ur, lut_uh, lut_d = self._calc_lut(data)            

            # Concatenate the LUT for URoeffR is None: no reverse poly calculated

            merge_ur = (lut_ur_full[0,-1] + lut_ur[0,0])/2
            lut_ur_full = numpy.concatenate((
                lut_ur_full[:,lut_ur_full[0,:] < merge_ur],
                lut_ur[     :,lut_ur[0,:]      > merge_ur]
            ), axis=1)

            # Concatenate the LUT for UH
            merge_uh = (lut_uh_full[0,-1] + lut_uh[0,0])/2
            lut_uh_full = numpy.concatenate((
                lut_uh_full[:,lut_uh_full[0,:] < merge_uh],
                lut_uh[:,lut_uh[0,:] > merge_uh]
                ), axis=1)
                
            # Concatenate the LUT for D
            merge_d = (lut_d_full[0,-1] + lut_d[0,0])/2
            lut_d_full = numpy.concatenate((
                lut_d_full[:,lut_d_full[0,:] < merge_d],
                lut_d[:,lut_d[0,:] > merge_d]
                ), axis=1)

        # Remove dummy first point of the lut
        lut_ur_full = lut_ur_full[:, 1:]
        lut_uh_full = lut_uh_full[:,1:]
        lut_d_full = lut_d_full[:,1:]

        # Interpolate (one point every 100nm)
        lut_ur_full = self._interp_lut(lut_ur_full)
        lut_uh_full = self._interp_lut(lut_uh_full)
        lut_d_full  = self._interp_lut(lut_d_full)

        # Add dummy points
        lut_ur, lut_uh, lut_d = self._add_dummy_points_lut(lut_ur_full, lut_uh_full, lut_d_full)
        
        # Save the LUT
        self._lut_table_ur = lut_ur
        self._lut_table_uh = lut_uh
        self._lut_table_d = lut_d
        self._lut_to_calib()
        self.lut_use = True
        self._save()

        # Activate the regulation
        self._mono.regul.on()

    def _acq_zone_fjs(self, fj1, fj2, speed):
        """ Make fjstraj scan that will be used for making the LUT.
        The scan is made for increasing energies, i.e. for small to large fast jack positions

        Args:
            fj1: Fast Jack position 1 [mm]
            fj2: Fast Jack position 2 [mm]
            speed: Maximum Fast Jack Velocity [mm/s]

        Returns:
            Numpy Array containing the raw measurements acquired by the FastDAQ

        """
        # Make sure fj2 > fj1
        if (fj1 > fj2):
            fj1b = fj1
            fj1 = fj2
            fj2 = fj1b
        assert fj2 > fj1, "fj2 should be greater than fj1"

        # Make sure that mcoil can be disabled
        th1 = self._mono.xtal2bragg(fj1-self._fjs_margin)
        th2 = self._mono.xtal2bragg(fj2+self._fjs_margin)
        if numpy.abs(th1-th2) > self._mono._max_coil_range:
            raise ValueError(f"Stroke ({numpy.abs(th1-th2)}) is too large to disable mcoil (Max: {self._mono._max_coil_range}).")

        # Load Trajectory larger than built LUT
        # self._mono.trajectory.load_fjs(fj1-self.fj_traj_offset, fj2+self.fj_traj_offset, 1000, use_lut=False)
        # Thomas - Changed this otherwise there is some velocity issues (too small)
        self.lut_use = False
        self._mono.trajectory.load_fjs(fj1-1.1*self._fjs_margin, fj2+1.1*self._fjs_margin, 1000, use_lut=False)

        # Make sure regul is off (Mode A)
        self._mono.regul.off()

        # Go to Start position
        umv(self._motors["braggfe"], self._mono.xtal2bragg(fj1-self._fjs_margin))
        umv(self._motors["fjstraj"], fj1-self._fjs_margin)
        # If scan without mcoil, put mcoil at middle position
        self._motors["fjstraj"].disable_axis(axis=self._motors["mcoil"])
        print("MCOIL is disabled. Send it at mean position: %.1f [deg]" % ((th1+th2)/2.0))
        umv(self._motors["mcoil"], (th1+th2)/2.0)

        # Set maximum velocity
        max_fjs_vel = self._motors["fjstraj"]._get_max_velocity()
        if (speed < max_fjs_vel):
            self._motors["fjstraj"].velocity = speed
        else:
            self._motors["fjstraj"].velocity = 0.95 * max_fjs_vel
        print("Fast Jack velocity is set to: %.2f [mm/s]" % self._motors["fjstraj"].velocity)

        # Compute approximate time of scan [s]
        scan_time = 2+2*self._motors["fjstraj"].acctime+(fj2-fj1)/self._motors["fjstraj"].velocity
        print("Scan will last ~ %.1f [s]" % scan_time)

        # create counters
        fast_daq_counters = [
            self._goat.counters.e_fj_ur,
            self._goat.counters.e_fj_uh,
            self._goat.counters.e_fj_d,
            self._goat.counters.fjsur,
            self._goat.counters.fjsuh,
            self._goat.counters.fjsd
        ]
        # Run fast DAC
        self._fastdaq.prepare_time(scan_time, fast_daq_counters)
        self._fastdaq.start(wait=False)

        # Make the scan
        umv(self._motors["fjstraj"], fj2+self._fjs_margin)

        # Wait end of fast DAC
        self._fastdaq.wait_finished()

        # If disable mcoil, put it back at end position and re-enable it
        print("Position the MCOIL motor to end position: %.1f [deg]" % th2)
        self._motors["fjstraj"].enable_axis(axis=self._motors["mcoil"])
        umv(self._motors["fjstraj"], fj2)

        # Get Data from Fast DAQ
        daq_data = self._fastdaq.get_data()

        return daq_data

    def _calc_lut(self, daq_data):
        """ Computes the LUT for ur/uh/d

        Args:
            data: Acquired data during the LUT scan
            lut_inc: Distance between everypoint of the LUT

        """
        # Get all data in SI units
        fjur = 1e-9*daq_data["fjsur"] # ur Fast Jack Step in [m]
        fjuh = 1e-9*daq_data["fjsuh"] # uh Fast Jack Step in [m]
        fjd  = 1e-9*daq_data["fjsd"] # d Fast Jack Step in [m]
        fjur_e = 1e-9*daq_data["e_fj_ur"] # ur Errors in [m]
        fjuh_e = 1e-9*daq_data["e_fj_uh"] # uh Errors in [m]
        fjd_e  = 1e-9*daq_data["e_fj_d"] # d Errors in [m]

        # Generate Low pass FIR Filter
        nyq_rate = self._mono._goat_ctl._Fs / 2.0 # Nyquist Rate [Hz]
        cutoff_hz = 27 # The cutoff frequency of the filter [Hz]

        # Window with specific ripple [dB] and width [Nyquist Fraction]
        N, beta = kaiserord(60, 4/nyq_rate)

        N_delay = int((N-1)/2) # Delay in ticks

        # Fitler generation
        taps = firwin(N, cutoff_hz/nyq_rate, window=('kaiser', beta))

        # Low Pass Filter the Data and compensation of the delay introduced by the FIR filter
        fjur_e_lpf = lfilter(taps, 1.0, fjur_e)[N:]
        fjuh_e_lpf = lfilter(taps, 1.0, fjuh_e)[N:]
        fjd_e_lpf  = lfilter(taps, 1.0, fjd_e)[N:]
        fjur_lpf   = fjur[N_delay+1:-N_delay]
        fjuh_lpf   = fjuh[N_delay+1:-N_delay]
        fjd_lpf    = fjd[ N_delay+1:-N_delay]

        # Get Only Interesting Data (remove points outside the margin)
        filt_array = numpy.where(numpy.logical_or(fjd_lpf < fjd[0] + 1e-3*self._fjs_margin, fjd_lpf > fjd[-1] - 1e-3*self._fjs_margin))

        fjur_e_filt = numpy.delete(fjur_e_lpf, filt_array)
        fjuh_e_filt = numpy.delete(fjuh_e_lpf, filt_array)
        fjd_e_filt  = numpy.delete(fjd_e_lpf , filt_array)
        fjur_filt   = numpy.delete(fjur_lpf, filt_array)
        fjuh_filt   = numpy.delete(fjuh_lpf, filt_array)
        fjd_filt    = numpy.delete(fjd_lpf , filt_array)

        # Build individual LUT
        u,c = numpy.unique(fjur_filt, return_index=True)
        lut_ur = numpy.stack((u-fjur_e_filt[c], u))
        u,c = numpy.unique(fjuh_filt, return_index=True)
        lut_uh = numpy.stack((u-fjuh_e_filt[c], u))
        u,c = numpy.unique(fjd_filt, return_index=True)
        lut_d = numpy.stack((u-fjd_e_filt[c], u))

        return 1e3*lut_ur, 1e3*lut_uh, 1e3*lut_d

    def _interp_lut(self, lut, interp_increment=100e-6):
        """ Interpolate the LUT. It is used to reduce the number of points.
            interp_increment (in mm) can be set to 100nm (smallest FJ errors we want to model is 5um)
        """
        lut_x = numpy.arange(lut[0][0], lut[0][-1]+interp_increment, interp_increment)
        lut_new = numpy.zeros((2, len(lut_x)))
        lut_new[0] = lut_x
        lut_new[1] = numpy.interp(lut_x , lut[0], lut[1])
        return lut_new
        
    def _add_dummy_points_lut(self, lut_ur, lut_uh, lut_d):
        """ Add (theoretical) points at beginning and ending of LUT.
            These points are here only to make sure that there is no problem when loading a trajectory.
        """

        lut_ur = numpy.concatenate(
            (
                numpy.concatenate((numpy.arange(self._fjsz_min, lut_ur[0][0], 1)[numpy.newaxis,:], lut_ur[1][0] - lut_ur[0][0] + numpy.arange(self._fjsz_min, lut_ur[0][0], 1)[numpy.newaxis,:])),
                lut_ur,
                numpy.concatenate((numpy.arange(lut_ur[0][-1]+1, self._fjsz_max+1, 1)[numpy.newaxis,:], lut_ur[1][-1] - lut_ur[0][-1] + numpy.arange(lut_ur[0][-1]+1, self._fjsz_max+1, 1)[numpy.newaxis,:])),
            ),
            axis=1
        )
        lut_uh = numpy.concatenate(
            (
                numpy.concatenate((numpy.arange(self._fjsz_min, lut_uh[0][0], 1)[numpy.newaxis,:], lut_uh[1][0] - lut_uh[0][0] + numpy.arange(self._fjsz_min, lut_uh[0][0], 1)[numpy.newaxis,:])),
                lut_uh,
                numpy.concatenate((numpy.arange(lut_uh[0][-1]+1, self._fjsz_max+1, 1)[numpy.newaxis,:], lut_uh[1][-1] - lut_uh[0][-1] + numpy.arange(lut_uh[0][-1]+1, self._fjsz_max+1, 1)[numpy.newaxis,:])),
            ),
            axis=1
        )
        lut_d = numpy.concatenate(
            (
                numpy.concatenate((numpy.arange(self._fjsz_min, lut_d[0][0], 1)[numpy.newaxis,:], lut_d[1][0] - lut_d[0][0] + numpy.arange(self._fjsz_min, lut_d[0][0], 1)[numpy.newaxis,:])),
                lut_d,
                numpy.concatenate((numpy.arange(lut_d[0][-1]+1, self._fjsz_max+1, 1)[numpy.newaxis,:], lut_d[1][-1] - lut_d[0][-1] + numpy.arange(lut_d[0][-1]+1, self._fjsz_max+1, 1)[numpy.newaxis,:])),
            ),
            axis=1
        )
        
        return lut_ur, lut_uh, lut_d

    def _lut_to_calib(self):
        """ Load the calibration table in Xcalibu (will be used for loading in the IcePAP) """ 

        # UR
        self._fjs_calib["fjsur"].set_raw_x(numpy.copy(self._lut_table_ur[0]))
        self._fjs_calib["fjsur"].set_raw_y(numpy.copy(self._lut_table_ur[1]))
        self._fjs_calib["fjsur"].check_monotonic()
        self._fjs_calib["fjsur"].compute_interpolation()

        # UH
        self._fjs_calib["fjsuh"].set_raw_x(numpy.copy(self._lut_table_uh[0]))
        self._fjs_calib["fjsuh"].set_raw_y(numpy.copy(self._lut_table_uh[1]))
        self._fjs_calib["fjsuh"].check_monotonic()
        self._fjs_calib["fjsuh"].compute_interpolation()

        # D
        self._fjs_calib["fjsd"].set_raw_x(numpy.copy(self._lut_table_d[0]))
        self._fjs_calib["fjsd"].set_raw_y(numpy.copy(self._lut_table_d[1]))
        self._fjs_calib["fjsd"].check_monotonic()
        self._fjs_calib["fjsd"].compute_interpolation()


class EsrfDCMinterfCalib:
    def __init__(self, mono, motors):
        self._mono = mono
        self._goat_ctl = self._mono._goat._hwc
        self._motors = motors

        calib_param = self._mono.config.get("calibration", None)
        if calib_param is None:
            raise RuntimeError("esrf_dcm/FjsCalib: No calibration parameters")
                    
        interf_data_dir = calib_param.get("interf_data_dir", None)
        if interf_data_dir is None:
            raise RuntimeError("esrf_dcm/InterfCalib: No data directory given for INTERF calibration")
        self._data_dir = interf_data_dir
        
        self._radix_setting = settings.SimpleSetting(f"EsrfDCM_interfcalib_{self._mono.name}", default_value="interf_default")

        # TODO Check default parameters
        self._rc_nbp = calib_param.get("interf_rc_nbp", 50)
        self._rc_motor = calib_param.get("interf_rc_motor", None)
        self._rc_delta = calib_param.get("interf_rc_delta", 0.03)
        self._rc_I0 = calib_param.get("interf_rc_I0_counter", 0.03)
        self._rc_time = calib_param.get("interf_rc_time", 0.1)
        
        self._bpm_cnt_y = calib_param.get("interf_bpm_y_counter", None)
        self._bpm_cnt_z = calib_param.get("interf_bpm_z_counter", None)
        self._bpm_pixel_y = calib_param.get("interf_bpm_pixel_y", 100)
        self._bpm_pixel_z = calib_param.get("interf_bpm_pixel_z", 100)
        self._bpm_dcm_bpm = calib_param.get("interf_bpm_dcm_bpm", 10)
        self._bpm_src_dcm = calib_param.get("interf_bpm_src_dcm", 10)
        self._bpm_src_kb_v = calib_param.get("interf_bpm_src_kb_v", 10)
        self._bpm_src_kb_h = calib_param.get("interf_bpm_src_kb_h", 10)
        self._bpm_kb_bpm_v = calib_param.get("interf_bpm_kb_bpm_v", 10)
        self._bpm_kb_bpm_h = calib_param.get("interf_bpm_kb_bpm_h", 10)
    
    def __info__(self):
        print(self._get_info())
        return ""

    def _get_info(self):
        """ Display Status of interferometer calibration """
        mystr = ""
        
        on_str = GREEN("ON")
        off_str = RED("OFF")
        
        rx_state, ry_state, dz_state = self._state()

        mystr += "    Interf Calib.   : "
        if rx_state:
            mystr += f"Rx {on_str} - "
        else:
            mystr += f"Rx {off_str} - "
        if ry_state:
            mystr += f"Ry {on_str} - "
        else:
            mystr += f"Ry {off_str} - "
        if dz_state:
            mystr += f"Dz {on_str}\n"
        else:
            mystr += f"Dz {off_str}\n"
            
        return mystr
        
    @property
    def _radix(self):
        return self._radix_setting.get()
        
    @_radix.setter
    def _radix(self, value):
        self._radix_setting.set(value)
        
    def load(self, lut=None, radix=None, do_print=False):
        """ Load Interferometer calibration from .dat file.
            The calibration is automatically loaded in the Speedogat. """

        if radix is not None:
            self._radix = radix
            
        xtal = self._mono._xtals.xtal_sel
        filename = f"{self._data_dir}/{self._radix}_{xtal}"
        
        if do_print:
            print(f"Load Interf calibration table from :")
            
        if lut is None or lut == "rx":
            if do_print:
                print(f"    - {filename}_rx.dat")
            data = numpy.loadtxt(f"{filename}_rx.dat", dtype=float, delimiter=" ").transpose()
            self._calib2speedgoat("rx", data)
        
        if lut is None or lut == "ry":
            if do_print:
                print(f"    - {filename}_ry.dat")
            data = numpy.loadtxt(f"{filename}_ry.dat", dtype=float, delimiter=" ").transpose()
            self._calib2speedgoat("ry", data)
        
        if lut is None or lut == "dz":
            if do_print:
                print(f"    - {filename}_dz.dat")
            data = numpy.loadtxt(f"{filename}_dz.dat", dtype=float, delimiter=" ").transpose()
            self._calib2speedgoat("dz", data)

    def _load_calibration_file(self, radix, direction):
        """ Function used to easily load calibration files. """
        return numpy.loadtxt(f"{self._data_dir}/{radix}_{self._mono._xtals.xtal_sel}_{direction}.dat", dtype=float, delimiter=" ").transpose()

    def _state(self):
        rx_state = self._mono._goat_ctl.lut._luts[self._lut_id("rx")].enabled
        ry_state = self._mono._goat_ctl.lut._luts[self._lut_id("ry")].enabled
        dz_state = self._mono._goat_ctl.lut._luts[self._lut_id("dz")].enabled
        return (rx_state, ry_state, dz_state)

    def on(self, lut=None):
        if lut is None or lut == "rx":
            self._mono._goat_ctl.lut._luts[self._lut_id("rx")].enable()
        if lut is None or lut == "ry":
            self._mono._goat_ctl.lut._luts[self._lut_id("ry")].enable()
        if lut is None or lut == "dz":
            self._mono._goat_ctl.lut._luts[self._lut_id("dz")].enable()

    def off(self, lut=None):
        if lut is None or lut == "rx":
            self._mono._goat_ctl.lut._luts[self._lut_id("rx")].disable()
        if lut is None or lut == "ry":
            self._mono._goat_ctl.lut._luts[self._lut_id("ry")].disable()
        if lut is None or lut == "dz":
            self._mono._goat_ctl.lut._luts[self._lut_id("dz")].disable()

    def _realign_interf(self):
        """
            Function to reset the interferometers to zeros.
            After realign, xtal1 = 0, metro_frame=0 and xtal2 corresponds to the wanted fjsz
        """
        if self._mono._xtals.get_xtals_config("speedgoat_id")[self._mono._xtals.xtal_sel] == "ring":
            self._goat_ctl.trigger.reset_interf_ring.trig()
        else:
            self._goat_ctl.trigger.reset_interf_hall.trig()

    def calib_bpm(self, radix, ene_from, ene_to, npoints, time_per_point, focused=False, rx_add_mode=False, ry_add_mode=False, show_calib=False, poly_deg=3, bragg_delta=0.25, x_axis_energy=True):
        """ Calibrate the DCM interferometers using a BPM (detector).
            The calibration is performed between 'ene_from' and 'ene_to' with 'nppoints'.
            'time_per_point' should be large enought to let the regul settle.
            This can be in focused (focused=True) or unfocused mode (focused=False).
            If in focused, Rx and Ry can be calibrated.
            If in unfocused, Rx and Dz can be calibrated.
            Calibration can be in "add" mode (rx_add_mode=True, ry_add_mode=True), meaning that the calibrations are updated (and not replaced) with the new calibration.
            This is working only in focused mode.
            'bragg_delta' corresponds to the bragg increment for the fitted values. """

        # Deactivate the loaded calibrations only if not in "add mode"
        if focused:
            if not rx_add_mode:
                self.off(lut="rx")
            else:
                self.on(lut="rx")
            if not ry_add_mode:
                self.off(lut="ry")
            else:
                self.on(lut="ry")
        else:
            self.off(lut="rx")
            self.off(lut="dz")

        # Move at start position and start the regul
        umv(self._mono.motors["energy_tracker"], ene_from)
        self._mono.regul.on()

        # TODO - Make a continous scan instead (will be better because the regul will stay ON all the time)
        # ascan: At each energy, record the Y-Z position of the beam on the dectector
        sc = ascan(
            self._mono._motors["energy_tracker"],
            ene_from,
            ene_to,
            npoints,
            time_per_point,
            self._bpm_cnt_y,
            self._bpm_cnt_z,
            self._mono._goat.counters.bragg,
            self._mono._goat.counters.ctrl_status,
            self._mono._goat.counters.e_xtal_rx, # Counters to verify that regulation is working fine
            self._mono._goat.counters.e_xtal_ry, # Counters to verify that regulation is working fine
            self._mono._goat.counters.e_xtal_dz, # Counters to verify that regulation is working fine
        )
        
        # Verify that control is ON all the time
        data_regul_state = sc.get_data()["goat_scc:ctrl_status"]
        if not numpy.all(data_regul_state):
            raise RuntimeError("Regulation failed during scan")
            
        # Get bragg angle [deg]
        data_bragg = sc.get_data()["goat_scc:bragg"]
        # Get y,z position on the detector, convert it to [nm] and remove the mean value
        data_y = detrend(sc.get_data()[self._bpm_cnt_y.fullname] * self._bpm_pixel_y, type='constant')
        data_z = detrend(sc.get_data()[self._bpm_cnt_z.fullname] * self._bpm_pixel_z, type='constant')

        if focused:
            # COMPUTE RY ERROR
            data_ry_err = -0.5 * data_z * self._bpm_src_kb_v / (self._bpm_src_dcm * self._bpm_kb_bpm_v)

            # Save raw data
            calib_ry_raw = numpy.stack((data_bragg, data_ry_err))
            print(f"Raw data are saved in {self._data_dir}/{radix}_{self._mono._xtals.xtal_sel}_ry_raw.dat")
            self._save(radix, "ry_raw", calib_ry_raw)

            # Plot the raw data and ask user if modifications are to be made
            if show_calib:
                fig = plt.figure(dpi=150)
                ax = fig.add_subplot(1, 1, 1)
                if x_axis_energy:
                    ax.plot(self._mono.bragg2energy(calib_ry_raw[0] + self._mono._bragg_offset.get()), calib_ry_raw[1], marker='o')
                    ax.set_xlabel('Energy [keV]')
                else:
                    ax.plot(calib_ry_raw[0], calib_ry_raw[1], marker='o')
                    ax.set_xlabel('Bragg [deg]')
                ax.set_ylabel('Error Ry [nrad]')
                ax.set_title('Ry calibrations with KB - Raw data')
                plt.show()
                if not getval_yes_no("Make any modification to the raw data and then press Enter to continue", default="yes"):
                    pass # Should raise error

            # Load (possibly modified) raw data
            raw_ry_lut = self._load_calibration_file(radix, "ry_raw")

            if ry_add_mode:
                # If in 'add' mode, use already loaded bragg angles
                previous_bragg_fit = numpy.copy(self._goat_ctl.lut._luts[self._lut_id("ry")].x_raw)
                previous_ry_fit    = numpy.copy(self._goat_ctl.lut._luts[self._lut_id("ry")].y_raw)

                # Get indexes that will be updated
                matching_calib = (previous_bragg_fit != 0) & (previous_bragg_fit > numpy.min(raw_ry_lut[0])) & (previous_bragg_fit < numpy.max(raw_ry_lut[0]))
     
                # Make a fit
                ry_fit_to_update = self._fit_calib(raw_ry_lut[0], raw_ry_lut[1], previous_bragg_fit[matching_calib], "ry", radix, poly_deg=poly_deg, show_calib=show_calib, save_figure=True)
 
                # Add data to already loaded calibration
                previous_ry_fit[matching_calib] += ry_fit_to_update
                # Add offsets to start and end of calibrations
                previous_ry_fit[(previous_bragg_fit != 0) & (previous_bragg_fit < numpy.min(raw_ry_lut[0]))] += ry_fit_to_update[0]
                previous_ry_fit[(previous_bragg_fit != 0) & (previous_bragg_fit > numpy.max(raw_ry_lut[0]))] += ry_fit_to_update[-1]
            
                ry_lut = numpy.stack((previous_bragg_fit, previous_ry_fit))

            else:
                # Not in 'add mode'
                bragg_fit_ry = numpy.arange(numpy.min(raw_ry_lut[0])-bragg_delta, numpy.max(raw_ry_lut[0])+bragg_delta, bragg_delta)
     
                # Make a fit
                ry_fit = self._fit_calib(raw_ry_lut[0], raw_ry_lut[1], bragg_fit_ry, "ry", radix, poly_deg=poly_deg, show_calib=show_calib, save_figure=True)
            
                ry_lut = numpy.stack((bragg_fit_ry, ry_fit))

            # Save, load in Xcalibu and activate Calibrations
            self._save(radix, "ry", ry_lut)
            self._calib2speedgoat("ry", ry_lut)
            self.on(lut="ry")

            # COMPUTE RX ERROR
            data_rx_err = -0.5 * data_y * self._bpm_src_kb_h / (self._bpm_src_dcm * self._bpm_kb_bpm_h * numpy.sin(numpy.radians(data_bragg)))

            # Save raw data
            calib_rx_raw = numpy.stack((data_bragg, data_rx_err))
            print(f"Raw data are saved in {self._data_dir}/{radix}_{self._mono._xtals.xtal_sel}_rx_raw.dat")
            self._save(radix, "rx_raw", calib_rx_raw)

            # Plot the raw data and ask user if modifications are to be made
            if show_calib:
                fig = plt.figure(dpi=150)
                ax = fig.add_subplot(1, 1, 1)
                if x_axis_energy:
                    ax.plot(self._mono.bragg2energy(calib_rx_raw[0] + self._mono._bragg_offset.get()), calib_rx_raw[1], marker='o')
                    ax.set_xlabel('Energy [keV]')
                else:
                    ax.plot(calib_rx_raw[0], calib_rx_raw[1], marker='o')
                    ax.set_xlabel('Bragg [deg]')
                ax.set_ylabel('Error Rx [nrad]')
                ax.set_title('Rx calibrations with KB - Raw data')
                plt.show()
                if not getval_yes_no("Make any modification to the raw data and then press Enter to continue", default="yes"):
                    pass # Should raise error

            # Load (possibly modified) raw data
            raw_rx_lut = self._load_calibration_file(radix, "rx_raw")

            if rx_add_mode:
                # If in 'add' mode, use already loaded bragg angles
                previous_bragg_fit = numpy.copy(self._goat_ctl.lut._luts[self._lut_id("rx")].x_raw)
                previous_rx_fit    = numpy.copy(self._goat_ctl.lut._luts[self._lut_id("rx")].y_raw)

                # Get indexes that will be updated
                matching_calib = (previous_bragg_fit != 0) & (previous_bragg_fit > numpy.min(raw_rx_lut[0])) & (previous_bragg_fit < numpy.max(raw_rx_lut[0]))
     
                # Make a fit
                rx_fit_to_update = self._fit_calib(raw_rx_lut[0], raw_rx_lut[1], previous_bragg_fit[matching_calib], "rx", radix, poly_deg=poly_deg, show_calib=show_calib, save_figure=True)
 
                # Add data to already loaded calibration
                previous_rx_fit[matching_calib] += rx_fit_to_update
                # Add offsets to start and end of calibrations
                previous_rx_fit[(previous_bragg_fit != 0) & (previous_bragg_fit < numpy.min(raw_rx_lut[0]))] += rx_fit_to_update[0]
                previous_rx_fit[(previous_bragg_fit != 0) & (previous_bragg_fit > numpy.max(raw_rx_lut[0]))] += rx_fit_to_update[-1]
            
                rx_lut = numpy.stack((previous_bragg_fit, previous_rx_fit))

            else:
                # Not in 'add mode'
                bragg_fit_rx = numpy.arange(numpy.min(raw_rx_lut[0])-bragg_delta, numpy.max(raw_rx_lut[0])+bragg_delta, bragg_delta)
     
                # Make a fit
                rx_fit = self._fit_calib(raw_rx_lut[0], raw_rx_lut[1], bragg_fit_rx, "rx", radix, poly_deg=poly_deg, show_calib=show_calib, save_figure=True)
            
                rx_lut = numpy.stack((bragg_fit_rx, rx_fit))

            # Save, load in Xcalibu and activate Calibrations
            self._save(radix, "rx", rx_lut)
            self._calib2speedgoat("rx", rx_lut)
            self.on(lut="rx")

        else:
            # Compute Rx Error (in nrad)
            data_rx_err = 0.5 * data_y / (self._bpm_dcm_bpm * numpy.sin(numpy.radians(data_bragg)))

            # Save raw data
            calib_rx_raw = numpy.stack((data_bragg, data_rx_err))
            print(f"Raw data are saved in {self._data_dir}/{radix}_{self._mono._xtals.xtal_sel}_rx_raw.dat")
            self._save(radix, "rx_raw", calib_rx_raw)

            # Plot the raw data and ask user if modifications are to be made
            if show_calib:
                fig = plt.figure(dpi=150)
                ax = fig.add_subplot(1, 1, 1)
                if x_axis_energy:
                    ax.plot(self._mono.bragg2energy(calib_rx_raw[0] + self._mono._bragg_offset.get()), calib_rx_raw[1], marker='o')
                    ax.set_xlabel('Energy [keV]')
                else:
                    ax.plot(calib_rx_raw[0], calib_rx_raw[1], marker='o')
                    ax.set_xlabel('Bragg [deg]')
                ax.set_ylabel('Error Rx [nrad]')
                ax.set_title('Rx calibrations (unfocused) - Raw data')
                plt.show()
                if not getval_yes_no("Make any modification to the raw data and then press Enter to continue", default="yes"):
                    pass # Should raise error

            # Load (possibly modified) raw data
            raw_rx_lut = self._load_calibration_file(radix, "rx_raw")

            # Chosse values for Bragg angle
            bragg_fit = numpy.arange(numpy.min(raw_rx_lut[0])-bragg_delta, numpy.max(raw_rx_lut[0])+bragg_delta, bragg_delta)
            
            # Make a fit
            rx_fit = self._fit_calib(raw_rx_lut[0], raw_rx_lut[1], bragg_fit, 'rx', radix, show_calib=show_calib, poly_deg=poly_deg, save_figure=True)

            # Save, load in Xcalibu and activate Calibrations
            rx_lut = numpy.stack((bragg_fit, rx_fit))
            self._save(radix, 'rx', rx_lut)
            self._calib2speedgoat("rx", rx_lut)
            self.on(lut="rx")

            # Compute error in Crystal distance (Dz, in nm)
            data_dz_err = - 0.5 * data_z / numpy.cos(numpy.radians(data_bragg))

            # Save raw data
            calib_dz_raw = numpy.stack((data_bragg, data_dz_err))
            print(f"Raw data are saved in {self._data_dir}/{radix}_{self._mono._xtals.xtal_sel}_dz_raw.dat")
            self._save(radix, "dz_raw", calib_dz_raw)

            # Plot the raw data and ask user if modifications are to be made
            if show_calib:
                fig = plt.figure(dpi=150)
                ax = fig.add_subplot(1, 1, 1)
                if x_axis_energy:
                    ax.plot(self._mono.bragg2energy(calib_dz_raw[0] + self._mono._bragg_offset.get()), calib_dz_raw[1], marker='o')
                    ax.set_xlabel('Energy [keV]')
                else:
                    ax.plot(calib_dz_raw[0], calib_dz_raw[1], marker='o')
                    ax.set_xlabel('Bragg [deg]')
                ax.set_ylabel('Error Dz [nm]')
                ax.set_title('Dz calibrations (unfocused) - Raw data')
                plt.show()
                if not getval_yes_no("Make any modification to the raw data and then press Enter to continue", default="yes"):
                    pass # Should raise error

            # Load (possibly modified) raw data
            raw_dz_lut = self._load_calibration_file(radix, "dz_raw")

            # Chosse values for Bragg angle
            bragg_fit = numpy.arange(numpy.min(raw_dz_lut[0])-bragg_delta, numpy.max(raw_dz_lut[0])+bragg_delta, bragg_delta)

            # Make a fit
            dz_fit = self._fit_calib(raw_dz_lut[0], raw_dz_lut[1], bragg_fit, 'dz', radix, show_calib=show_calib, poly_deg=poly_deg, save_figure=True)
            
            # Save, load in Xcalibu and activate Calibrations
            dz_lut = numpy.stack((bragg_fit, dz_fit))
            self._save(radix, 'dz', dz_lut)
            self._calib2speedgoat("dz", dz_lut)
            self.on(lut="dz")

    def _fit_calib(self, data_bragg, data_y, data_bragg_new, direction, radix, poly_deg=3, show_calib=False, save_figure=False, x_axis_energy=True):
        """ Make a polynomial fit of the data (data_bragg, data_y) of degree 'poly_deg'.
            Returns new data array based on polynomial fit.
            The returned data are sampled for bragg angles in 'data_bragg_new'. """
        polynom_coef = numpy.polyfit(data_bragg, data_y, poly_deg)
        data_y_new = numpy.polyval(polynom_coef, data_bragg_new)

        fig = plt.figure(dpi=150)
        ax1 = fig.add_subplot(1, 1, 1)
        if x_axis_energy:
            ax1.plot(self._mono.bragg2energy(data_bragg + self._mono._bragg_offset.get()), data_y, label='raw')
        else:
            ax1.plot(data_bragg, data_y, label='raw')

        if x_axis_energy:
            ax1.plot(self._mono.bragg2energy(data_bragg + self._mono._bragg_offset.get()), numpy.polyval(polynom_coef, data_bragg), label='fit')
        else:
            ax1.plot(data_bragg, numpy.polyval(polynom_coef, data_bragg), label='fit')

        if x_axis_energy:
            ax1.plot(self._mono.bragg2energy(data_bragg_new + self._mono._bragg_offset.get()), data_y_new, marker='o', label='kept')
        else:
            ax1.plot(data_bragg_new, data_y_new, marker='o', label='kept')

        if x_axis_energy:
            ax1.set_xlabel('Energy [keV]')
        else:
            ax1.set_xlabel('Bragg angle [deg]')

        if direction == 'rx':
            ax1.set_ylabel('Error Rx [nrad]')
        elif direction == 'ry':
            ax1.set_ylabel('Error Ry [nrad]')
        elif direction == 'dz':
            ax1.set_ylabel('Error Dz [nm]')
        else:
            pass # Should raise error

        ax1.grid(True)
        ax1.legend(loc='upper left')

        ax2 = ax1.twinx()
        if x_axis_energy:
            ax2.plot(self._mono.bragg2energy(data_bragg + self._mono._bragg_offset.get()), data_y - numpy.polyval(polynom_coef, data_bragg), label='fit error', color='red')
        else:
            ax2.plot(data_bragg, data_y - numpy.polyval(polynom_coef, data_bragg), label='fit error', color='red')

        ax2.legend(loc='upper right')

        if show_calib:
            plt.show()
            if not getval_yes_no("Is calibration fit OK?", default="yes"):
                pass # Should raise error

        if save_figure:
            fig.savefig("%s/%s_%s_%s.pdf" % (self._data_dir, radix, self._mono._xtals.xtal_sel, direction))

        plt.close(fig)

        return data_y_new


    def calib_ry_rc(self, radix, from_ene, to_ene, rc_number, rc_nbp=None, show_calib=False, poly_deg=3, bragg_delta=0.25, x_axis_energy=True):
        """ Calibrate the DCM metrology in Pitch (Ry) by using rocking curves.
            Will perform 'rc_number' rocking curves between 'from_ene' and 'to_ene'.
            Each rocking curve is composed of 'rc_npb' points (default in .yml file).
            A polynomial fit of order 'poly_deg' is perform, and the fitted data are sampled with intervals 'bragg_delta'.
            The resampled fitted data are then loaded in the Speedgoat. """

        # Make sure from_ene < to_ene
        if from_ene > to_ene:
            aux = from_ene
            from_ene = to_ene
            to_ene = aux

        # No regul as we use Stepper for rocking curves
        self._mono.regul.off()

        # Ry calibration is deactivated as it should be overwritten
        self.off(lut="ry")

        # Use FJS Offset and Calibration
        self._mono.calib.fjs.offset_use = True
        self._mono.calib.fjs.lut_use = True

        # Compute the angles at which the rocking curves will be performed (linearly spaced)
        bragg_start = self._mono.energy2bragg(from_ene) - self._mono._bragg_offset.get()
        bragg_stop = self._mono.energy2bragg(to_ene) - self._mono._bragg_offset.get()
        print(f"Perform the rocking curves : bragg_start={bragg_start} bragg_stop={bragg_stop} rc_number={rc_number}")
        bragg_angles = numpy.linspace(bragg_start, bragg_stop, rc_number+1)

        # Perform all the rocking curves
        calib_ry_raw = numpy.zeros((2, rc_number+1))
        for idx in range(rc_number+1):
            print(GREEN(f"RC #{idx+1} / ({rc_number+1})\n"))
            calib_ry_raw[0][rc_number-idx] = bragg_angles[idx]
            # Move at wanted energy
            umv(self._mono._motors["energy_tracker"], self._mono.bragg2energy(bragg_angles[idx] + self._mono._bragg_offset.get()))
            # Perform one rocking curve, and save the CEN (i.e. calibration)
            calib_ry_raw[1][rc_number-idx] = self._single_rocking_curve(rc_nbp=rc_nbp)

        # Save the raw data
        print(f"Raw data are saved in {self._data_dir}/{radix}_{self._mono._xtals.xtal_sel}_ry_raw.dat")
        self._save(radix, "ry_raw", calib_ry_raw)

        # Plot the raw data and ask user if modifications are to be made
        if show_calib:
            fig = plt.figure(dpi=150)
            ax = fig.add_subplot(1, 1, 1)
            if x_axis_energy:
                ax.plot(self._mono.bragg2energy(calib_ry_raw[0] + self._mono._bragg_offset.get()), calib_ry_raw[1], marker='o')
                ax.set_xlabel('Energy [keV]')
            else:
                ax.plot(calib_ry_raw[0], calib_ry_raw[1], marker='o')
                ax.set_xlabel('Bragg [deg]')
            ax.set_ylabel('Error Ry [nrad]')
            ax.set_title('Rocking Curve calibrations - Raw data')
            plt.show()
            if not getval_yes_no("Make any modification to the raw data and then press Enter to continue", default="yes"):
                pass # Should raise error

        # Load (possibly modified) raw data, perform a fit, save the fitted data and load them in the Speedgoat
        self._fit_raw_data_and_load(radix, "ry", bragg_delta=bragg_delta, poly_deg=poly_deg, show_calib=show_calib)

    def _fit_raw_data_and_load(self, radix, direction, bragg_delta=0.25, poly_deg=3, show_calib=False):
        """ Load raw data, perform a polynomial fit, and then load the fitted data in the Speedgoat. """
        xtal = self._mono._xtals.xtal_sel
        filename = f"{self._data_dir}/{radix}_{xtal}"

        calib_raw = numpy.loadtxt(f"{filename}_{direction}_raw.dat", dtype=float, delimiter=" ").transpose()

        # Compute the bragg angles that will be loaded in the Speedgoat
        bragg_angles_lut = numpy.linspace(calib_raw[0][0], calib_raw[0][-1], int(2+numpy.abs(calib_raw[0][0]-calib_raw[0][-1]) // bragg_delta + 0.5))
        # Make a fit of the raw data for the wanted bragg angles
        fitted_calib = self._fit_calib(calib_raw[0], calib_raw[1], bragg_angles_lut, direction, radix, poly_deg=poly_deg, show_calib=show_calib, save_figure=True)
        calib_fitted = numpy.stack((bragg_angles_lut, fitted_calib))

        self._calib2speedgoat(direction, calib_fitted)
        self._save(radix, direction, calib_fitted)
        
        # Activate interf calib
        self.on(lut=direction)

    def _single_rocking_curve(self, rc_nbp=None):
        """ Perform one rocking curve.
            Is number of points is not defined, the default _rc_npb will be used (specified in .yml file)
            Returns the interferometric Ry value corresponding to maximum intensity (using cen function) """

        if rc_nbp is None:
            rc_nbp = self._rc_nbp

        ry_filtered = self._mono._goat.counters.r_y_filtered
        s = dscan(
            self._rc_motor, 
            -self._rc_delta,
            self._rc_delta,
            rc_nbp,
            self._rc_time,
            self._rc_I0,
            ry_filtered,
        )
        ry_data = s.get_data()[ry_filtered.fullname]
        diode_data = s.get_data()[self._rc_I0.fullname]
        cen = scan_math.cen(ry_data, diode_data)
        return cen[0]
    
    def _save(self, radix, direction, data):
        """ Save the calibration as a .dat file for further use.
            'direction' can be 'ry', 'rx' or 'dz'.
            If the file already exists, it will be deleted. """
        if radix is not None:
            xtal = self._mono._xtals.xtal_sel
            filename = f"{self._data_dir}/{radix}_{xtal}_{direction}.dat"
            try:
                os.remove(filename)
            except:
                pass
            numpy.savetxt(filename, data.transpose())
            
    def _lut_id(self, lut):
        """ Returns the LUT identification (ring or hall with direction) """
        xtal = self._mono._xtals.xtal_sel
        speedgoat_id = self._mono._xtals.get_xtals_config("speedgoat_id")[xtal]
        return f"{speedgoat_id}_{lut}"

    def _calib2speedgoat(self, lut, data):
        """ Load the calibration (data) in the Speedgoat.
        Preprend values with bragg=0 and calib equal to first value to match the size of the calibration. """
        lut_id = self._lut_id(lut)

        calib_nb = self._goat_ctl.lut._luts[lut_id].length
        if calib_nb < len(data[0]):
            raise RuntimeError(f"Calibration {lut_id}: Too many data points for speedgoat arrays")
        
        # data_x are bragg angles, start with only 0, and then usefull data
        data_x = numpy.zeros(calib_nb)
        data_x[calib_nb-len(data[0]):calib_nb] = data[0]
        
        # data_x are calibration offsets, starting with the first values (for all bragg angles = 0), and then usefull data
        data_y = data[1][0]*numpy.ones(calib_nb)
        data_y[calib_nb-len(data[0]):calib_nb] = data[1]
        
        self._goat_ctl.lut._luts[lut_id].disable()
        self._goat_ctl.lut._luts[lut_id].x_raw = data_x
        self._goat_ctl.lut._luts[lut_id].y_raw = data_y
        self._goat_ctl.lut._luts[lut_id].enable()
