import numpy
import gevent
from bliss.config import settings
from bliss.common.utils import GREEN, RED
import matplotlib.pyplot as plt
from matplotlib.mlab import psd, csd
from datetime import datetime
from scipy.io import savemat

class EsrfDCMregul:
    
    def __init__(self, mono):
        self._mono = mono
        self._goat_ctl = self._mono._goat._hwc
        self._fastdaq = self._goat_ctl.fastdaq.fastdaq
        self._results_dir = "/data/id21/inhouse/DCM/CALIB/DYNAMICS" # Where to store identification results

        # Get configuration for regulation
        regul_param = self._mono.config.get("regulation", None)
        if regul_param is None:
            raise RuntimeError("esrf_dcm/Regulation: No calibration parameters")
   
        # Get data directory to store regulation informations
        regul_data_dir = regul_param.get("data_dir", None)
        if regul_data_dir is None:
            raise RuntimeError("esrf_dcm/Regulation: No data directory given for REGUL")
        self._data_dir = regul_data_dir

        # Default filter for regulation
        self._radix_setting = settings.SimpleSetting(f"EsrfDCM_regul_{self._mono.name}_radix", default_value="regul_default")

        # Set feedback bandwidth as wanted
        self._wanted_fb = regul_param.get("wanted_fb", None)
        if regul_data_dir is not None:
            self._change_bandwidth(self._wanted_fb)
        
        # Load default Filters if not already loaded
        if not self._is_loaded():
            self.load()

    def __info__(self):
        return self._goat_ctl.regul.hac.__info__()
 
    def _get_info(self):
        mystr = ""
        mystr += "    Regulation      : "
        if self.state().value == 0:
            mystr += RED("OFF")
        else:
            mystr += GREEN("ON")
        return mystr

    def state(self):
        return self._goat_ctl.regul.hac.state
        
    def off(self):
        self._goat_ctl.regul.hac.off()
        
    def on(self):
        self._goat_ctl.regul.hac.on()

    def error(self):
        """ Print Position error for each fastjack.
        Useful to check if regul is working. """
        print(f"UR: {self._goat_ctl.counter.e_fj_ur_filtered.value : .1f} nm")
        print(f"UH: {self._goat_ctl.counter.e_fj_uh_filtered.value : .1f} nm")
        print(f"D : {self._goat_ctl.counter.e_fj_d_filtered.value : .1f} nm")

    def _change_bandwidth(self, fb):
        """ Change the wanted feedback bandwidth """
        assert fb > 0, "fb should be greater than 0"
        assert fb <= 50, "fb should be less than 20Hz for stability purposes"

        self._wanted_fb = fb

        # Change the wanted bandwidth
        self._goat_ctl.parameter.K_wn = fb*2*numpy.pi

    def _is_loaded(self):
        return numpy.any(self._goat_ctl.filter.Ginv_ur.num_coef)
        
    def load(self, radix=None):
        """ Load a specific controller (inverse plant in case of complementary filter control). """
        # Disable regul
        wanted = False
        if self.state != 0:
            wanted = True
            self.off()

        if radix is not None:
            self.radix = radix
        filename = f"{self._data_dir}/{self.radix}.dat"

        # Load Regul File
        Ginv = numpy.loadtxt(filename, dtype=float, delimiter=" ")

        # Enable filters
        self._goat_ctl.filter.Ginv_ur.disable()
        self._goat_ctl.filter.Ginv_uh.disable()
        self._goat_ctl.filter.Ginv_d.disable()
        # Configure Filters
        self._goat_ctl.filter.Ginv_ur.num_coef = Ginv[0]
        self._goat_ctl.filter.Ginv_uh.num_coef = Ginv[0]
        self._goat_ctl.filter.Ginv_d.num_coef  = Ginv[0]
        self._goat_ctl.filter.Ginv_ur.den_coef = Ginv[1]
        self._goat_ctl.filter.Ginv_uh.den_coef = Ginv[1]
        self._goat_ctl.filter.Ginv_d.den_coef  = Ginv[1]
        # Enable filters
        self._goat_ctl.filter.Ginv_ur.enable()
        self._goat_ctl.filter.Ginv_uh.enable()
        self._goat_ctl.filter.Ginv_d.enable()

        # Enable regul if is was previously wanted
        if wanted:
            self.on()

    def _identify_plant(self, fast_jack="ur", duration=10, file_name="plant_id"):
        """ Computes the transfer function from "fast_jack"
        to the defined counters. """

        # Make sure regulator is Off
        self.off()

        # Configure Test Signal
        self._goat_ctl.generator.id_plant.type.NormalWhiteNoise
        self._goat_ctl.generator.id_plant.offset = 0.
        self._goat_ctl.generator.id_plant.amplitude = 500.
        self._goat_ctl.generator.id_plant.duration = duration

        # Configure Filter for test signal
        self._goat_ctl.filter.id_plant.wn = 2*numpy.pi*80
    
        # Configure where to inject test signal
        if (fast_jack == "ur"):
            self._goat_ctl.parameter.id_fj_index = 1
        elif (fast_jack == "uh"):
            self._goat_ctl.parameter.id_fj_index = 2
        elif (fast_jack == "d"):
            self._goat_ctl.parameter.id_fj_index = 3

        # Configure Counters
        counters_out = [self._mono._goat.counters.y_fj_ur, self._mono._goat.counters.y_fj_uh, self._mono._goat.counters.y_fj_d]
        counter_in = self._mono._goat.counters.id_exc

        # Configure FastDAQ
        self._fastdaq.prepare_time(self._goat_ctl.generator.id_plant.duration, [counter_in] + counters_out)

        # Start the FastDAQ
        self._fastdaq.start(silent=True, wait=False)

        # Start the test signal
        self._goat_ctl.generator.id_plant.start()

        # Get FastDAQ Data
        self._fastdaq.wait_finished()
        data = self._fastdaq.get_data()

        # Plot the identified transfer function
        win = numpy.hanning(self._goat_ctl._Fs)
        fig, axs = plt.subplots(2, 1, dpi=150, sharex=True)

        for counter_out in counters_out:
            self._goat_ctl.utils._tfestimate(data[counter_in.name], data[counter_out.name], win=win, Fs=int(self._goat_ctl._Fs), plot=True, axs=axs, legend=f"fjp{fast_jack} to {counter_out.name}")

        axs[0].set_xlim([1e0, 1e3])
        axs[0].set_ylim([1e-3, 1e3])
        axs[0].legend()
        axs[1].set_ylim(-180, 180)

        # Save the identified plant
        now = datetime.now().strftime("%d-%m-%Y_%H-%M")
        if file_name is not None:
            savemat("%s/%s_%s_%s.mat" % (self._results_dir, now, file_name, fast_jack), data)

            fig.savefig("%s/%s_%s_%s.pdf" % (self._results_dir, now, file_name, fast_jack))
            fig.savefig("%s/%s_%s_%s.png" % (self._results_dir, now, file_name, fast_jack))
            plt.close(fig)

    def _identify_cl_tf(self, fast_jack="ur", duration=20, file_name="cl_id"):
        """ Computes the closed-loop transfer functions (S and T) """

        # Configure Test Signal
        self._goat_ctl.generator.id_plant.type.NormalWhiteNoise
        self._goat_ctl.generator.id_plant.offset = 0.
        self._goat_ctl.generator.id_plant.amplitude = 500.
        self._goat_ctl.generator.id_plant.duration = duration

        # Configure Filter for test signal
        self._goat_ctl.filter.id_plant.wn = 2*numpy.pi*80
    
        # Configure where to inject test signal
        if (fast_jack == "ur"):
            self._goat_ctl.parameter.id_fj_index = 1
        elif (fast_jack == "uh"):
            self._goat_ctl.parameter.id_fj_index = 2
        elif (fast_jack == "d"):
            self._goat_ctl.parameter.id_fj_index = 3

        # Configure FastDAQ
        self._fastdaq.prepare_time(self._goat_ctl.generator.id_plant.duration, [self._mono._goat.counters.id_exc, self._mono._goat.counters[f"fjp{fast_jack}"], self._mono._goat.counters[f"u_fj_{fast_jack}"]])

        # Start the FastDAQ
        self._fastdaq.start(silent=True, wait=False)

        # Start the test signal
        self._goat_ctl.generator.id_plant.start()

        # Get FastDAQ Data
        self._fastdaq.wait_finished()
        data = self._fastdaq.get_data()

        # Plot the identified transfer function
        win = numpy.hanning(5*self._goat_ctl._Fs)
        fig = plt.figure(dpi=150)
        ax = fig.add_subplot(1, 1, 1)

        nfft = len(win)

        [Pyx, f] = csd(data["id_exc"], data[f"fjp{fast_jack}"], window=win, NFFT=nfft, Fs=int(self._goat_ctl._Fs), noverlap=int(nfft/2), detrend="mean")
        Pxx      = psd(data["id_exc"],                          window=win, NFFT=nfft, Fs=int(self._goat_ctl._Fs), noverlap=int(nfft/2), detrend="mean")[0]
        S = Pyx/Pxx

        [Pyx, f] = csd(data["id_exc"], data[f"u_fj_{fast_jack}"], window=win, NFFT=nfft, Fs=int(self._goat_ctl._Fs), noverlap=int(nfft/2), detrend="mean")
        Pxx      = psd(data["id_exc"],                            window=win, NFFT=nfft, Fs=int(self._goat_ctl._Fs), noverlap=int(nfft/2), detrend="mean")[0]
        T = Pyx/Pxx

        ax.plot(f, numpy.abs(S), '-', label=f"S {fast_jack}")
        ax.plot(f, numpy.abs(T), '-', label=f"T {fast_jack}")
        ax.set_xscale('log')
        ax.set_yscale('log')
        ax.grid(True, which="both", axis="both")
        ax.set_xlabel("Frequency [Hz]")
        ax.set_ylabel("Amplitude")
        ax.set_xlim([0.5, 1e3])
        ax.set_ylim([1e-4, 1e1])
        ax.legend()

        # Save the identified plant
        now = datetime.now().strftime("%d-%m-%Y_%H-%M")
        if file_name is not None:
            savemat("%s/%s_%s_%s.mat" % (self._results_dir, now, file_name, fast_jack), data)

            fig.savefig("%s/%s_%s_%s.pdf" % (self._results_dir, now, file_name, fast_jack))
            fig.savefig("%s/%s_%s_%s.png" % (self._results_dir, now, file_name, fast_jack))
            plt.close(fig)


    @property
    def radix(self):
        return self._radix_setting.get()
        
    @radix.setter
    def radix(self, value):
        self._radix_setting.set(value)

