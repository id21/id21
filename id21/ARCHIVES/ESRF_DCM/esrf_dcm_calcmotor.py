import numpy
import xcalibu

from bliss.config import settings
from bliss.controllers.motor import CalcController
from bliss.physics.units import ur, units
from bliss.common.utils import add_object_method

from bliss.common.logtools import log_debug
from bliss import global_map
from bliss.controllers.monochromator.calcmotor import MonochromatorCalcMotorBase
from bliss.controllers.motors.tripod import tripod
from bliss.common.utils import ColorTags, BOLD, RED, GREEN

"""
controller:
    class: BraggRotationCalcMotor
    approximation: 0.01
    axes:
        - name: $mbrag
          tags: real bragg
        - name: $msafe
          tags: real safety
        - name: $mcoil
          tags: real coil

        - name: bragg_rot
          tags: bragg_rotation
"""

class BraggRotationCalcMotor(CalcController):

    def __init__(self, *args, **kwargs):
        CalcController.__init__(self, *args, **kwargs)

        self.approx = float(self.config.get("approximation"))

        global_map.register(self)

    def pseudos_are_moving(self):
        for axis in self.pseudos:
            if axis.is_moving:
                return True
        return False

    def calc_from_real(self, reals_dict):

        pseudos_dict = {}

        if (numpy.isclose(reals_dict["bragg"] , reals_dict["safety"], atol=self.approx) and \
           numpy.isclose(reals_dict["bragg"], reals_dict["coil"], atol=self.approx)) or \
           self.pseudos_are_moving():
            pseudos_dict["bragg_rotation"] = reals_dict["bragg"]
        else:
            pseudos_dict["bragg_rotation"] = numpy.nan

        _b = reals_dict["bragg"]
        _s = reals_dict["safety"]
        _c = reals_dict["coil"]
        _br = pseudos_dict["bragg_rotation"]
        log_debug(self, "calc_from_real(): %g %g %g -> %g", _b, _s, _c, _br)

        return pseudos_dict

    def calc_to_real(self, pseudos_dict):

        reals_dict = {}

        if not numpy.isnan(pseudos_dict["bragg_rotation"]).any():
            reals_dict["bragg"]  = pseudos_dict["bragg_rotation"]
            reals_dict["safety"] = pseudos_dict["bragg_rotation"]
            reals_dict["coil"]   = pseudos_dict["bragg_rotation"]

        _br = pseudos_dict["bragg_rotation"]
        _b = reals_dict["bragg"]
        _s = reals_dict["safety"]
        _c = reals_dict["coil"]
        log_debug(self, "calc_to_real(): %g -> %g %g %g", _br, _b, _s, _c)

        return reals_dict

class SafetyCoilCalcMotor(CalcController):

    def __init__(self, *args, **kwargs):
        CalcController.__init__(self, *args, **kwargs)

        self.approx = float(self.config.get("approximation"))

    def pseudos_are_moving(self):
        for axis in self.pseudos:
            if axis.is_moving:
                return True
        return False

    def calc_from_real(self, reals_dict):

        pseudos_dict = {}

        # Return nan if real motors are not close enough.
        if numpy.isclose(reals_dict["coil"], reals_dict["safety"], atol=self.approx) or \
           self.pseudos_are_moving():
            pseudos_dict["safe_coil"] = reals_dict["safety"]
        else:
            pseudos_dict["safe_coil"] = numpy.nan

        return pseudos_dict

    def calc_to_real(self, pseudos_dict):

        reals_dict = {}

        if not numpy.isnan(pseudos_dict["safe_coil"]).any():
            reals_dict["safety"] = pseudos_dict["safe_coil"]
            reals_dict["coil"]   = pseudos_dict["safe_coil"]

        return reals_dict

class DCMtripod(tripod):

    def _set_mono(self, mono):
        """Define mono property"""
        self._mono = mono
        
    def calc_from_real(self, real_dict):
        
        for key in real_dict.keys():
            real_dict[key] = numpy.atleast_1d(real_dict[key])
        
        corrected_real_dict = real_dict
        
        if self._mono.calib.fjs.lut_use:
            for i in range(numpy.size(real_dict["lega"])):
                for calib_name in self._mono.calib.fjs.calib_name:
                    # add 3 LUT correction to 3 FJS trajectories.
                    calib = self._mono.calib.fjs._fjs_calib[calib_name]
                    real_tag = self._mono.calib.fjs.real_tag[calib_name]
                    corrected_real_dict[real_tag][i] = calib.get_x(real_dict[real_tag][i])
        
        pseudo_dict = super().calc_from_real(corrected_real_dict)

        return pseudo_dict
        
    def calc_to_real(self, pseudo_dict):
        
        real_dict = super().calc_to_real(pseudo_dict)
        
        for key in real_dict.keys():
            real_dict[key] = numpy.atleast_1d(real_dict[key])
        
        if self._mono.calib.fjs.lut_use:
            for i in range(numpy.size(real_dict["lega"])):
                for calib_name in self._mono.calib.fjs.calib_name:
                    # add 3 LUT correction to 3 FJS trajectories.
                    calib = self._mono.calib.fjs._fjs_calib[calib_name]
                    real_tag = self._mono.calib.fjs.real_tag[calib_name]
                    real_dict[real_tag][i] = calib.get_y(real_dict[real_tag][i])
                    
        return real_dict
        
            
