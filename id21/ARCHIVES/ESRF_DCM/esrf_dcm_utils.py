import inspect
import time
import tabulate

from bliss import setup_globals

from bliss.controllers import pepu
from bliss.config.static import get_config
from bliss.shell.standard import umv, mv, wm
from bliss.common.scans import ascan
from bliss.common.utils import ColorTags, BOLD, GREEN, YELLOW, BLUE, RED, ORANGE
from bliss.controllers.motor import CalcController


##############################################################
#####
##### DCM Helpers
#####
##############################################################
def motor_info(mot_list):
    """
    Print position  dial  offset  lim-  lim+  state of a list of motors.
    A compact variation on wm().

    Example:
    TEST_SESSION [12]: print(motor_info([bad, roby, custom_axis]))
                 position  dial  offset  lim-  lim+  state
    bad          0         0.0   0.0     -inf  inf   ['READY']
    roby         3.3       3.3   0.0     -inf  inf   ['READY']
    custom_axis  0         0.0   0.0     -inf  inf   ['READY']
    """
    headers_list = ["", "position", "dial", "offset", "lim-", "lim+", "state"]
    table = []

    for mot in mot_list:
        mot_state = mot.state.current_states_names
        mot_info = (mot.name,
                    f"{mot.position:g}",
                    f"{mot.dial:g}",
                    f"{mot.offset:g}",
                    f"{mot.low_limit:g}",
                    f"{mot.high_limit:g}",
                    mot_state)
        table.append(mot_info)

    info_str = tabulate(tuple(table),  tablefmt="plain", headers=headers_list)

    return(info_str)

##############################################################
#####
##### Where All Motor Groups
#####

def dcmwm(*motors):
    print("")
    lines = []
    line1 = [BOLD("    Name")]
    line2 = [BOLD("    Pos.")]
    for mot in motors:
        if isinstance(mot.controller, CalcController):
            line1.append(f"{ORANGE(mot.name)}({mot.unit})")
        else:
            line1.append(f"{BLUE(mot.name)}({mot.unit})")
        line2.append(f"{mot.position:.3f}")
    lines.append(line1)
    lines.append(line2)
    mystr = tabulate.tabulate(lines, tablefmt="plain")
    print(mystr)

def wty():
    dcmwm(get_config().get("tyy"), get_config().get("tyrz"))
    dcmwm(get_config().get("mtyu"), get_config().get("mtyd"))
    dcmwm(get_config().get("mtyu_brake"), get_config().get("mtyd_brake"))
    
def wtz():
    dcmwm(get_config().get("tzz"), get_config().get("tzrx"))
    dcmwm(get_config().get("tzh"), get_config().get("tzr"))
    dcmwm(get_config().get("mtzur"), get_config().get("mtzdr"))

def wbragg():
    dcmwm(get_config().get("braggrot"), get_config().get("safecoil"))
    dcmwm(get_config().get("mbragg"), get_config().get("msafe"), get_config().get("mcoil"))

def wfjs():
    dcmwm(get_config().get("fjsz"), get_config().get("fjsrx"), get_config().get("fjsry"))
    dcmwm(get_config().get("mfjsur"), get_config().get("mfjsuh"), get_config().get("mfjsd"))

def wfjp():
    dcmwm(get_config().get("fjpz"), get_config().get("fjpry"), get_config().get("fjprx"))
    dcmwm(get_config().get("fjpur"), get_config().get("fjpuh"), get_config().get("fjpd"))

def wdcm():
    #dcmwm(get_config().get("Emonotrajund"), get_config().get("Emonotraj"), get_config().get("thtraj"), get_config().get("mtraj"))
    dcmwm(get_config().get("Edcmund"))
    dcmwm(get_config().get("Edcm"), get_config().get("braggfe"), get_config().get("braggrot"))
    dcmwm(get_config().get("mbrag"), get_config().get("msafe"), get_config().get("mcoil"))
    wfjs()


def wtraj():
    dcmwm(get_config().get("enetraj"), get_config().get("thtraj"), get_config().get("mtraj"))


##############################################################
#####
##### DCM Motors Synchronization
#####
def motor_esync(axis):
    axis.hw_state
    ch = axis.address
    axis.controller.raw_write(f"{ch}:esync")
    axis.controller.raw_write(f"{ch}:power on")
    axis.sync_hard()

def fjs_sync():
    get_config().get("mfjsuh").sync_hard()
    get_config().get("mfjsur").sync_hard()
    get_config().get("mfjsd").sync_hard()

def fjp_sync():
    get_config().get("fjpur").sync_hard()
    get_config().get("fjpuh").sync_hard()
    get_config().get("fjpd").sync_hard()
    get_config().get("fjpz").sync_hard()
    get_config().get("fjprx").sync_hard()
    get_config().get("fjpry").sync_hard()

def thesync():
    print("Bragg  esync")
    motor_esync(get_config().get("mbrag"))
    print("safety esync")
    motor_esync(get_config().get("msafe"))
    print("Coil   esync")
    motor_esync(get_config().get("mcoil"))
    print("Fjs    esync")
    fjs_sync()
    #print("Fjp    esync")
    #fjp_sync()
