# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2023 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

"""
Convention for bipod calculations:

    1 - We are using a direct coordiante system (x,y,z) as follow

                    ^
                    | Z axis
                    |
        X axis      |
          <----------
                   /
                  /
                 / Y axis
                /
                v

    2 - The rotation are name rotx, roty, rotz around respectively the
        X, Y and Z axis with counter-clockwize positive value

    3 - all parameters relative to legs are following the same convention:
            - coordinates of "lega" are named: "ax", "ay"
            - coordinates of "legb" are named: "bx", "by"
            - ...

    4 - all parameters are given in meters

    5 - The calculation takes into account the "unit" defined for each
        real or calculated motors.
        if the "unit" field is missing, the universal units are used:
        meters (m) for distance, radians (rad) for angles
"""

import numpy as np
import tabulate
import io

from rich.table import Table
from rich.console import Console

from bliss.common.logtools import log_debug, log_info
from bliss.controllers.motor import CalcController
from bliss.physics.units import ur
from bliss.common.utils import ColorTags, BOLD
from bliss.shell.standard import wm

from bliss.config import static
cfg = static.get_config()


"""
Table with 2 legs:

    - Definition:

        + the 2 legs are tagged "lega" and "legb"
        + the height of the table is tagged "trans"
        + X-axis is the axis passing thrue lega and legb, positive
          from a to b
        + The rotation is around Y-axis, perpendicular to
          the [lega-legb] axis (X-axis)
        + the rotation is tagged "ry"
        + C is the center of rotation and its height is represented by the
          position of the "dz" axis
        + C is the origin of the X/Y/Z coordinates system
        + "ax" is the coordinate of the leg "lega" in the X/Y/Z coordinates system
        + "bx" is the coordinate of the leg "legb" in the X/Y/Z coordinates system

    - Example if C is inside the 2 legs
        + "ax" is a negative value in meter
        + "bx" is a positive value in meter

                   Z-axis
    		            ^
    		            |
                  ______|______________
                 /      |             /
                /       |...ax..>|   /
     X axis    /        |        |  /     X axis
     <--------/-.-------C--------.-/----<------
             /  |<.bx../         |/
            /   |     /          |
           /____|____/__________/|
                |   /            |
                |  v Y-axis      |
                |                |
              legb             lega

        + YML file:
            - plugin: emotion
              module: bipod
              class: bipod
              name: btable
              ax:  1.250
              bx: -1.250
              axes:
                - name: $sim_btz1
                  tags: real lega
                - name: $sim_btz2
                  tags: real legb
                - name: sim_btz
                  tags: trans
                  unit: mm
                - name: sim_btr
                  tags: rot
                  unit: deg

- Example if C is before the 2 legs
    - "ax" is a positive value
    - "ay" is a positive value

                                        Z-axis
    		                              ^
    		                              |
                  _____________________   |
                 /                    /   |
                /<.......bx...............|
     X axis    /|                   /     |    X axis
     <--------/-.----------------.-/------C----------
             /  |                |/      /
            /   |                |<.ax../
           /____|_______________/|     /
                |                |    /
                |                |   v Y axis
                |                |
              legb             lega

        + YML file:
            - plugin: emotion
              module: bipod
              class: bipod
              name: btable
              ax:  1.250
              bx: -1.250
              axes:
                - name: $sim_btz1
                  tags: real lega
                - name: $sim_btz2
                  tags: real legb
                - name: sim_btz
                  tags: tz
                  unit: mm
                - name: sim_btr
                  tags: ry
                  unit: deg
"""


class bipod(CalcController):
    def __init__(self, *args, **kwargs):
        CalcController.__init__(self, *args, **kwargs)

        self.ax = (self.config.config_dict.get("ax", None) * ur.m).magnitude
        self.bx = (self.config.config_dict.get("bx", None) * ur.m).magnitude

        log_debug(self, "ax = %f" % self.ax)
        log_debug(self, "bx = %f" % self.bx)

    def initialize(self):
        CalcController.initialize(self)

        # get all motor units
        self.lega_unit = self._tagged["lega"][0].unit
        if self.lega_unit is None:
            self.lega_unit = "m"

        self.legb_unit = self._tagged["legb"][0].unit
        if self.legb_unit is None:
            self.legb_unit = "m"

        self.tz_unit = self._tagged["tz"][0].unit
        if self.tz_unit is None:
            self.tz_unit = "m"

        self.ry_unit = self._tagged["ry"][0].unit
        if self.ry_unit is None:
            self.ry_unit = "rad"

# Non-rich version
#    def __info__(self):
#        mystr = f"Type: bipod\n\n"
#        mystr += f"ax[{self.ax}]\n"
#        mystr += f"bx[{self.bx}]\n\n"
#        title = []
#        user = []
#        for axis in self.pseudos:
#            title.append(f"{axis.name}[{axis.unit}]")
#            user.append(f"{axis.position:.4f}")
#        mystr += tabulate.tabulate([title, user], tablefmt="plain")
#        mystr += "\n\n"
#        title = []
#        user = []
#        for axis in self.reals:
#            title.append(f"{axis.name}[{axis.unit}]")
#            user.append(f"{axis.position:.4f}")
#        mystr += tabulate.tabulate([title, user], tablefmt="plain")
#        return mystr

    def __info__(self):
        console = Console(file=io.StringIO(), width=120)
        output = f"        {self.name} bipod\n"

        # PARAMs
        output += f"  ax[{self.ax}]\n"
        output += f"  bx[{self.bx}]\n\n"

        # REALs
        table = Table(title="Reals")
        _reals_names = []
        _reals_pos = []
        for axis in self.reals:
            _reals_names.append(f"{axis.name}")
            _reals_pos.append(f"{axis.position:.4f} {axis.unit}")
        table.add_column(_reals_names[0])
        table.add_column(_reals_names[1])
        table.add_row(_reals_pos[0], _reals_pos[1])

        console.print(table)

        # CALCs
        table = Table(title="Calc")
        _pseudos_names = []
        _pseudos_pos = []
        for axis in self.pseudos:
            _pseudos_names.append(f"{axis.name}")
            _pseudos_pos.append(f"{axis.position:.4f} {axis.unit}")

        table.add_column(_pseudos_names[0])
        table.add_column(_pseudos_names[1])
        table.add_row(_pseudos_pos[0], _pseudos_pos[1])

        console.print(table)

        # ENCs
        table = Table(title="Encs")
        encoders_names = ["enc_tyu", "enc_tyd", "enc_ty", "enc_rz"]
        encoders_values = []

        for enc_name in encoders_names:
            _enc = cfg.get(enc_name)
            table.add_column(enc_name)
            encoders_values.append(str(_enc.read()))

        table.add_row(*encoders_values)

        console.print(table)

        output += console.file.getvalue()

        return output

    def wa(self):
        mot_list = self.pseudos + self.reals
        wm(*mot_list)

    def calc_from_real(self, real_dict):

        log_info(self, "calc_from_real()")

        lega = (real_dict["lega"] * ur.parse_units(self.lega_unit)).to("m").magnitude
        legb = (real_dict["legb"] * ur.parse_units(self.legb_unit)).to("m").magnitude

        if not isinstance(lega, np.ndarray):
            lega = np.array([lega], dtype=float)
            legb = np.array([legb], dtype=float)

        ry = np.arctan((lega - legb) / (self.bx - self.ax))
        tz = lega + self.ax * np.tan(ry)

        ry = (ry * ur.rad).to(self.ry_unit).magnitude
        tz = (tz * ur.m).to(self.tz_unit).magnitude

        if len(lega) == 1:
            return {"tz": tz[0], "ry": ry[0]}
        return {"tz": tz, "ry": ry}

    def calc_to_real(self, calc_dict):

        log_info(self, "calc_to_real()")

        tz = (calc_dict["tz"] * ur.parse_units(self.tz_unit)).to("m").magnitude
        ry = (calc_dict["ry"] * ur.parse_units(self.ry_unit)).to("rad").magnitude

        if not isinstance(tz, np.ndarray):
            tz = np.array([tz], dtype=float)
            ry = np.array([ry], dtype=float)

        lega = tz - self.ax * np.tan(ry)
        legb = tz - self.bx * np.tan(ry)

        lega = (lega * ur.m).to(self.lega_unit).magnitude
        legb = (legb * ur.m).to(self.legb_unit).magnitude

        if len(tz) == 1:
            return {"lega": lega[0], "legb": legb[0]}
        return {"lega": lega, "legb": legb}
