# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2022 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

"""
ID21 TRIPOD FRAME

https://confluence.esrf.fr/display/ID21REFURBWK/Tripod+frame+and+chamber

REAL:
                fleg1
                  ^
                  |
                  |
                  |
                  |
                  + Sample
                  |                              ^
                  height                         | beam
                  |                              X
                  |                              |
                  V                      <--Y--- .
 fleg2 <---------base------> fleg3               Z

height = 500 mm
base = 340 mm


CALC:
  - TZ mm
  - RX mrad
  - RY mrad

Calculations:
    TZ = ( sz1 + sz2 + sz3 ) / 3.0
    RX = ( sz2 - sz3 )  / base
    Ry = ( 1 / height ) * ( ( sz2 + sz3 ) / 2 - sz1 )


Matrix for Frame tripod:

[Tz]         [ 1/3 ,  1/3,       1/3   ]     [nair1]

[Rx]    =    [ 0,     1/b,      -1/b   ]  .  [nair2]

[Ry]         [ -1/h,  1/(2*h), 1/(2*h) ]     [nair3]
"""


import tabulate
from bliss.controllers.motor import CalcController
import numpy as np

class tripod_frame(CalcController):
    """
    ID21 TRIPOD FRAME Calc motor controller.
    """
    def __init__(self, *args, **kwargs):
        CalcController.__init__(self, *args, **kwargs)

        self.base = self.config.get("base", float)
        self.height = self.config.get("height", float)

        if not self.base or not self.height :
            raise RuntimeError("Parameters `base` and `height` are not configured")

        self._axes_names = {}
        for axis in self.config.config_dict["axes"]:
            if "real fleg" in axis['tags']:
                self._axes_names[axis["tags"]] = axis["name"].name
            else:
                self._axes_names[axis["tags"]] = axis["name"]

        # legs to virtual and virtual to legs matrices:
        b = self.base
        h = self.height
        self.l2v_matrix = np.array([[1/3 ,1/3, 1/3], [0, 1000/b , -1000/b], [-1000/h, 1000/(2*h), 1000/(2*h)] ])
        self.v2l_matrix = np.linalg.inv(self.l2v_matrix)

        # ???
        self.no_offset = self.config.get("no_offset", bool, True)

    def __info__(self):
        """
        info about tripod frame
        """
        info_str = "ID21 TRIPOD FRAME \n"
        info_str += f"Base   = {self.base} mm\n"
        info_str += f"Height = {self.height} mm \n"

        l1 = self.axes[self._axes_names['real fleg1']]
        l2 = self.axes[self._axes_names['real fleg2']]
        l3 = self.axes[self._axes_names['real fleg3']]

        # labels of instanciated axes
        ll1 = self._axes_names['real fleg1']
        ll2 = self._axes_names['real fleg2']
        ll3 = self._axes_names['real fleg3']

        tz = self.axes[self._axes_names['tz']]
        rx = self.axes[self._axes_names['rx']]
        ry = self.axes[self._axes_names['ry']]

        # labels of instanciated calc axes
        ltz = self._axes_names['tz']
        lrx = self._axes_names['rx']
        lry = self._axes_names['ry']


        tab_roles = ["(fleg1)", "(fleg2)", "(fleg3)", "(TZ)", "(RX)" ,"(RY)"]
        tab_sep = ["-------", "-------", "-------", "----", "----" ,"----"]
        tab_axes = [ll1, ll2, ll3, ltz, lrx ,lry]
        tab_pos = [f"{xx.position:g}" for xx in [l1, l2, l3, tz, rx, ry]]

        # tabulate.PRESERVE_WHITESPACE = True
        # pos_table = tabulate.tabulate([tab_axes, tab_pos], tablefmt="plain") + "\n"
        pos_table = tabulate.tabulate([tab_roles, tab_sep, tab_axes, tab_pos])

        info_str += pos_table

#        info_str += f"   leg2 = {l2.position} \n "
#        info_str += f"   leg3 = {l3.position} \n "
#        info_str += f"   TZ = {tz.position} \n "
#        info_str += f"   RX = {rx.position} \n "
#        info_str += f"   RY = {ry.position} \n "


# TODO: Add info about hook ?


        return info_str

    def initialize_axis(self, axis):
        CalcController.initialize_axis(self, axis)

        # no_offset feature (True by default) to avoid offset accumulation.
        axis.no_offset = self.no_offset

    def calc_from_real(self, positions_dict):
        """
        Real legs lengths (l1, l2, l3 ) to Virtual Axes (Tz Rx Ry) Calculation.
        """
        l1 = positions_dict["fleg1"]
        l2 = positions_dict["fleg2"]
        l3 = positions_dict["fleg3"]

        v_legs = np.array([l1, l2, l3])

        # 'Legs to Virtual' matrix
        v_virt = np.dot(self.l2v_matrix, v_legs)

        tz = v_virt[0]
        rx = v_virt[1]
        ry = v_virt[2]

        return {"tz": tz, "rx": rx, "ry": ry}

    def calc_to_real(self, positions_dict):
        """
        Inverse calculation: Virtual Axes (Tz Rx Ry) to Legs lengths (l1, l2, l3 ).
        """
        rx = positions_dict["rx"]
        ry = positions_dict["ry"]
        tz = positions_dict["tz"]

        v_virt = np.array([tz, rx, ry])

        # Virtual axes to legs matrix
        v_legs = np.dot(self.v2l_matrix, v_virt)

        l1 = v_legs[0]
        l2 = v_legs[1]
        l3 = v_legs[2]

        return {"fleg1": l1, "fleg2": l2, "fleg3": l3}



