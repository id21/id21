# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2020 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.



"""TangoShutter BLISS class is designed for standard shutter.

On ID21, bsh1 and bsh2 absorbers are interlocked with downstream
valves (respectively rv4 and rv9).

This cause absorbers to be in state FAULT after closing and not CLOSED.

This class is then designed to deal with this state and to perfrom a
reset() before opening the absorber.

"""
from gevent import Timeout, sleep

from bliss.controllers.tango_shutter import TangoShutter
from bliss.controllers.tango_shutter import TangoShutterState
from bliss.common.logtools import log_warning, user_print

class Absorber(TangoShutter):
    """
    Class to deal with ID21 absorbers.
    """
    def __init__(self, name, config):
        super().__init__(name, config, "SafetyShutter")

    def _wait_shutter_to_be_closed(self, timeout=5):
        """Wait shutter to be in state "CLOSE" or "FAULT + interlock"
        Args:
            (float): timeout [s].
        Raises:
            RuntimeError: Execution timeout.
        """
        with Timeout(timeout, RuntimeError("Execution timeout in _wait_shutter_to_be_closed")):
            while self.state.name not in ["CLOSED", "FAULT"]:
                print(self.state.name)
                sleep(0.5)

            print(self.state.name)

            # check that in case of FAULT, the message is ok.
            if self.state.name == "FAULT":
                if not self.state_string[1].startswith('Pneumatic Beam Shutter is closed'):
                    raise RuntimeError(f"ERROR : shutter {self.name} is in fault state but seems not closed !!!!")
                else:
                    print(f"OK: shutter {self.name} is in fault state but seems well closed.")
            self._state_changed("CLOSED")

    def close(self, timeout=60):
        """Close
        Args:
            (float): Timeout [s] to wait until execution finished
        Raises:
            RuntimeError: Cannot execute if device in wrong state
        """
        state = self.state
        if state == TangoShutterState.CLOSED:
            log_warning(self, f"{self.name} already closed, command ignored")
        elif state.name in ("OPEN", "RUNNING"):
            try:
                self.proxy.close()

                # Here the _wait is replaced for ID21 absorbers:
                # self._wait(TangoShutterState.CLOSED, timeout)
                self._wait_shutter_to_be_closed()

                user_print(f"{self.name} was {state.name} and is now {self.state.name}")
                # user_print(f"{self.name} was {state.name} and is now {self.state.name}")
            except RuntimeError as err:
                print(err)
                raise
        else:
            raise RuntimeError(
                f"Cannot close {self.name}, current state is: {state.value}"
            )
