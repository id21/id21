# -*- coding: utf-8 -*-

import bliss
import gevent
import time
from bliss.config import static
cfg = static.get_config()

from bliss.common.logtools import user_print
from bliss.comm import rpc

from id21.absorbers_open_close import abs1_close, abs1_open, abs1_status

"""
RPC server for ID21 DCM vacuum monitoring.
"""


class ValveMonitoring(object):
    def __init__(self, verbose=None):
        self.verbose = verbose
        self.task = None
        self._stop_event = gevent.event.Event()

        self.get_bliss_objects()

        self.log = ""

        print("List of Gauges:")
        for g in self.gauges_list:
            print(g.name)

        self.start()

    def set_verbose_level(self, level):
        self.verbose = level

    def start(self):
        """
        initialize stop event
        start loop in a greenlet
        """
        timestamp = time.strftime("%Y-%m-%d %H:%M:%S")
        print(f"{timestamp} START MONITORING", flush=True)
        user_print(f"{timestamp} START MONITORING")
        self._stop_event.clear()
        self.task = gevent.spawn(self.monitoring_loop)

    def stop(self):
        """
        require task to stop
        wait end of loop
        """
        timestamp = time.strftime("%Y-%m-%d %H:%M:%S")
        print(f"{timestamp} STOP MONITORING", flush=True)
        user_print(f"{timestamp} STOP MONITORING")
        if self.task is not None:
            self._stop_event.set()
            with gevent.Timeout(12.0):
                self.task.join()
                print("joined", self.task)

    def get_log(self):
        return self.log

    def clear_log(self):
        self.log = ""

    """
    ######### Frontend states
    # automatic mode
     CYRIL [5]: fe.state
       Out [5]: <TangoShutterState.RUNNING: 'Automatic opening'>
     CYRIL [6]: fe.state.value
       Out [6]: 'Automatic opening'
    # forced mode
    ???
    """
    def monitoring_loop(self):
        ii = 0
        self._last_print_FE = time.time()
        self._last_print_RV = time.time()

        while not self._stop_event.is_set():
            timestamp = time.strftime("%Y-%m-%d %H:%M:%S")
            if self.frontend.state.value in ["Open", "Automatic opening"]:
                if (ii % 5) == 0:
                    # Call monitoring only once on 5 times.
                    self.monitor_valves(timestamp)
                else:
                    ii += 1
            else:
                # Print 'FE is not open' only once per 10 mn to avoid flooding of stdout.
                if ((time.time() - self._last_print_FE) > 600) :
                    self._last_print_FE = time.time()
                    print(f"{timestamp} Frontend is not Open", flush=True)
                    time.sleep(1)

            # Keep sleep small to be reactive on stop().
            time.sleep(1)

    def get_bliss_objects(self):
        self.rv4   = cfg.get("rv4")
        self.rv6   = cfg.get("rv6")
        self.rv7   = cfg.get("rv7")
        self.abs1  = cfg.get("bsh1")
        self.pir71 = cfg.get("pir71")
        self.pen71 = cfg.get("pen71")
        self.pir61 = cfg.get("pir61")
        self.pen61 = cfg.get("pen61")
        self.pir81 = cfg.get("pir81")
        self.pen81 = cfg.get("pen81")

        self.frontend = cfg.get("fe")

        self.gauges_list =[self.pir61, self.pen61,
                           self.pir71, self.pen71,
                           self.pir81, self.pen81]

    def all_gauges_on(self):
        gauges_states = [g._tango_state == "ON" for g in self.gauges_list]
        print("States of gauges:", gauges_states)
        return all(gauges_states)

    def state(self):
        if self._stop_event.is_set():
            _state = "OFF"
        else:
            _state = "RUNNING"

        return _state

    def status(self):
        if not self._stop_event.is_set():
            # _stop event is not set => loop is running.
            # I stop it before querying status.
            _was_on = True
            self.stop()
        else:
            _was_on = False

        timestamp = time.strftime("%Y-%m-%d %H:%M:%S")

        _status = (f"{timestamp}\n"
                   f"RV-6: {self.rv6._tango_state}\n"
                   f"RV-7: {self.rv7._tango_state}\n"
                   f"ABS1: {abs1_status()}\n"
                   f"PIR-61: {self.pir61._tango_state} {self.pir61.pressure:1.2e}\n"
                   f"PEN-61: {self.pen61._tango_state} {self.pen61.pressure:1.2e}\n"
                   f"PIR-71: {self.pir71._tango_state} {self.pir71.pressure:1.2e}\n"
                   f"PEN-71: {self.pen71._tango_state} {self.pen71.pressure:1.2e}\n"
                   f"PIR-81: {self.pir81._tango_state} {self.pir81.pressure:1.2e}\n"
                   f"PEN-81: {self.pen81._tango_state} {self.pen81.pressure:1.2e}\n"
                   f"--------------------------------------------------\n")

        # restart loop if it was active.
        if _was_on:
            self.start()

        return _status

    def monitor_valves(self, timestamp):
        """
        A single execution of monitoring process.
        """
        rv6_state = self.rv6._tango_state
        rv7_state = self.rv7._tango_state

        _status= (f"--------------------------------------------------\n"
                  f"{timestamp}\n"
                  f"RV-6: {rv6_state}\n"
                  f"RV-7: {rv7_state}\n"
                  f"ABS1: {abs1_status()}\n"
                  f"PIR-61: {self.pir61._tango_state} {self.pir61.pressure:1.2e}\n"
                  f"PEN-61: {self.pen61._tango_state} {self.pen61.pressure:1.2e}\n"
                  f"PIR-71: {self.pir71._tango_state} {self.pir71.pressure:1.2e}\n"
                  f"PEN-71: {self.pen71._tango_state} {self.pen71.pressure:1.2e}\n"
                  f"PIR-81: {self.pir81._tango_state} {self.pir81.pressure:1.2e}\n"
                  f"PEN-81: {self.pen81._tango_state} {self.pen81.pressure:1.2e}\n"
                  f"--------------------------------------------------\n")

        if self.verbose > 0:
            print(_status, flush=True)

        if rv6_state == "FAULT":
            print("resetting rv6", flush=True)
            self.rv6.reset()
            time.sleep(3)

        if rv7_state == "FAULT":
            print("resetting rv7", flush=True)
            self.rv7.reset()
            time.sleep(3)


        if rv6_state == "OPEN" and rv7_state == "OPEN":
            # Print message only every 10 minutes to avoid of overflow stdout.
            if ((time.time() - self._last_print_RV) > 600) :
                self._last_print_RV = time.time()
                print(f"{timestamp} ok rv6 rv7 are open", flush=True)
        else:
            _msg =  f"{timestamp } PROBLEM: at least one of the valves is closed"
            print(_msg, flush=True)
            self.log += (_msg + "\n")
            print(f"  rv6 is {rv6_state}", flush=True)
            print(f"  rv7 is {rv7_state}", flush=True)

#           print("resetting all gauges") ??? why ?
#           for g in gauges_list:
#               g.reset()

            if self.all_gauges_on():
                print("Ok all gauges (pir/pen 61 71 81) are ON")

                if rv6_state in ["CLOSE", "FAULT"]:
                    print("resetting rv6", flush=True)
                    self.rv6.reset()
                    time.sleep(3)
                    if self.rv6._tango_state in ["CLOSE", "OPEN"]:
                        print("RV6 reset successfull  -> now re-opening", flush=True)
                        self.rv6.open()
                        user_print(f"{timestamp} RV6 re-opened")

                if rv7_state in ["CLOSE", "FAULT"]:
                    print("resetting rv7", flush=True)
                    self.rv7.reset()
                    time.sleep(3)
                    if self.rv7._tango_state in ["CLOSE", "OPEN"]:
                        print("RV7 reset successfull  -> now re-opening", flush=True)
                        self.rv7.open()
                        user_print(f"{timestamp} RV7 re-opened", flush=True)

                user_print("Re-openning ABS1", flush=True)
                print("Re-openning ABS1", flush=True)

                abs1_open()

                # no need to re-open abs2 as it is not interlocked

            else:
                print("ERROR: not all gauges are ON")
                _status = "--------------------  ERROR  -------------------\n"

            _status += (f"{timestamp}\n"
                        f"RV-6: {self.rv6._tango_state}\n"
                        f"RV-7: {self.rv7._tango_state}\n"
                        f"ABS1: {abs1_status()}\n"
                        f"PIR-61: {self.pir61._tango_state} {self.pir61.pressure:1.2e}\n"
                        f"PEN-61: {self.pen61._tango_state} {self.pen61.pressure:1.2e}\n"
                        f"PIR-71: {self.pir71._tango_state} {self.pir71.pressure:1.2e}\n"
                        f"PEN-71: {self.pen71._tango_state} {self.pen71.pressure:1.2e}\n"
                        f"PIR-81: {self.pir81._tango_state} {self.pir81.pressure:1.2e}\n"
                        f"PEN-81: {self.pen81._tango_state} {self.pen81.pressure:1.2e}\n"
                        f"--------------------------------------------------\n")


def run(bind="0.0.0.0", port=8899, verbose=0):

    if verbose:
        print("in run()")

    access = "tcp://{}:{}".format(bind, port)
    vm = ValveMonitoring(verbose=verbose)
    server = rpc.Server(vm, stream=True)
    server.bind(access)
    print(f"Serving on {access}")

    try:
        server.run()
    except KeyboardInterrupt:
        print("Interrupted.")
    finally:
        server.close()


def parse_args(args=None):
    parser = argparse.ArgumentParser(
        prog="vacuum21-monitor",
        description="Serve a monitoring class using bliss rpc",
    )
    parser.add_argument(
        "--bind",
        "-b",
        default="0.0.0.0",
        metavar="address",
        help="Specify alternate bind address [default: all interfaces]",
    )
    parser.add_argument(
        "--verbose",
        "-v",
        default="0",
        metavar="verbosity",
        help="Specify level of verbosity [default: 0]",
    )
    parser.add_argument(
        "port",
        action="store",
        default=8899,
        type=int,
        nargs="?",
        help="Specify alternate port [default: 8899]",
    )
    return parser.parse_args(args)


def main(args=None):
    namespace = parse_args(args)

    run(namespace.bind, namespace.port, int(namespace.verbose))


if __name__ == "__main__":
    run()
