import math

from bliss.config import settings
from bliss.physics.units import ur
from bliss.physics.diffraction import CrystalPlane

"""
- class: DCMenergy
  # Fix exit geometry
  dist_fix_exit: 0.01
  dist_xtal_fjs0: 25
  # Crystals parameters
  xtal1: Si111
  xtal2: Si311
"""

class MonochromatorBase(object):
    
    
    def __init__(self, name, config):
        
        self.name = name
        
        # keep config node
        self.config_node = config
        
        # Fix exit Geometry
        self.fix_exit_offset = self.config_node.get("fix_exit_offset")
        
        # Crystals parameters
        xtals = self.config_node.get("xtals")
        self.xtal_names = []
        self.xtal = {}
        for xtali in range(len(xtals)):
            xtal_name = xtals[xtali]["xtal_name"]
            self.xtal_names.append(xtal_name)
            self.xtal[xtal_name] = CrystalPlane.fromstring(xtal_name)
        
        mono_def_val = {"xtal_sel": None}
        self.monochromator_setting_name = f"{self.name}_monochromator"
        self.monochromator_setting = settings.HashSetting(self.monochromator_setting_name, default_values=mono_def_val)

        self.xtal_sel = self.monochromator_setting["xtal_sel"]


    def __info__(self):
        ret_str = ""
        if self.xtal_sel is not None:
            ret_str = f"Crystal: {self.xtal_sel}\n"
        ret_str = f"{ret_str}Fix exit offset: {self.fix_exit_offset}"
        
        return ret_str
        
    ########################
    #
    # Fix Exit Monochromator
    #
    @property
    def fix_exit(self):
        return self.fix_exit_offset
        
    @fix_exit.setter
    def fix_exit(self, value):
        self.fix_exit_offset = value
        self.config_node["fix_exit_offset"] = value
        self.config_node.save()
        
    def set_crystal(self, crystal):
        if crystal in self.xtal_names:
            self.xtal_sel = crystal
            self.monochromator_setting["xtal_sel"] = crystal
        else:
            raise RuntimeError("Monochromator Base: Crystal (%s) not configured"%crystal)

    #####################
    #
    # Calculation methods
    #
    def energy2bragg(self, ene):
        xtal = self.xtal[self.xtal_sel]
        bragg = xtal.bragg_angle(ene*ur.keV).to(ur.deg).magnitude
        return(bragg)
        
    def bragg2energy(self, bragg):
        xtal = self.xtal[self.xtal_sel]
        energy = xtal.bragg_energy(bragg*ur.deg).to(ur.keV).magnitude
        return energy
        
    def bragg2dxtal(self, bragg):
        dxtal = self.fix_exit_offset / (2.0 * math.cos(math.radians(bragg)))
        return dxtal
        
    def dxtal2bragg(self, dxtal):
        bragg = math.degrees(math.acos(self.fix_exit_offset / (2.0 * dxtal)))
        return bragg
        
    def energy2dxtal(self, ene):
        xtal = self.xtal[self.xtal_sel]
        bragg = xtal.bragg_angle(ene*ur.keV).to('deg').magnitude
        dxtal = self.fix_exit_offset / (2.0 * math.cos(math.radians(bragg)))
        return dxtal
