#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `id21` package."""

import pytest

import id21


#
# !!! test with operational beacon.
#
def test_SXM_motors(id21beacon):
    """test SXM motors"""
    vscan_x = id21beacon.get("sampy")
    vscan_y = id21beacon.get("sampz")

    print(f"sampy={vscan_x.position}")
    print(f"sampz={vscan_y.position}")
    
