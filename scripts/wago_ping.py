#!/usr/bin/env python
# -*- coding: utf-8 -*-
# wago_ping.py
'''
Test if devices in a list are responding
'''
import gevent
import subprocess

devices_list = ["wcid21bs", "wcid21bv1", "wcid21bv2", "wcid21bv3", "wcid21bv4",
                "wcid21bv5", "wcid21c", "wcid21cal", "wcid21d", "wcid21dcm",
                "wcid21e", "wcid21hpps", "wcid21k", "wcid21m0"]

def ping_ta_wago(device):
    # c: number of packets
    # W: timeout
    p = subprocess.call(["ping", "-c", "1", "-W", "1", device],
                        stdout=subprocess.PIPE)
    status = "off" if p else "active"
    print (f"{device:12} {status}")

_threads = list()

for dev in devices_list:
    _threads.append(gevent.spawn(ping_ta_wago, dev))

gevent.joinall(_threads)
